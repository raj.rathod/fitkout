//
//  ComunitySeachVC.swift
//  FitKout
//
//  Created by HT-Admin on 28/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit
protocol ComunitySeachVCDelegate
{
    func selectedCommunity( selectedCmty : String)
}
class ComunitySeachVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {
    
    var dropDownFlag = ""
    
    var delegate : ComunitySeachVCDelegate?

    var searchStr = ""
    var communityArray = [CommunitySearch]()

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableUsers: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        searchBar.delegate = self
        tableUsers.register(UINib(nibName: "ComunityUserCell", bundle: nil), forCellReuseIdentifier: "ComunityUserCell")
        tableUsers.tableFooterView = UIView()
        tableUsers.separatorStyle = .none
        tableUsers.reloadData()
        
        self.view.activityStartAnimating(activityColor: #colorLiteral(red: 0.003921568627, green: 0.7764705882, blue: 0.8470588235, alpha: 1), backgroundColor: .clear)
        searchUserThroughApiData(search : searchStr)
    }


   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return communityArray.count
        }
         
         func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
             let cell = tableView.dequeueReusableCell(withIdentifier: "ComunityUserCell", for: indexPath) as! ComunityUserCell
          
             let csModel: CommunitySearch
             csModel = communityArray[indexPath.row]
             cell.lblUName.text = csModel.CommunityName
             cell.imgUser.loadImageUsingCache(withUrl : csModel.ProfileUrl)

             return cell
             
         }
         
         
      func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
             return 100
         }
         
      func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           self.searchBar.resignFirstResponder()
        let csModel: CommunitySearch
        csModel = communityArray[indexPath.row]
        
        if (dropDownFlag == "1")
        {
            delegate?.selectedCommunity(selectedCmty: csModel.CommunityName)
            self.dismiss(animated: true, completion: nil)
        }else
        {
            let comunityDetailsVC = ComunityDetailsVC.init(nibName: "ComunityDetailsVC", bundle: nil)
            comunityDetailsVC.communityId = csModel.CommunityId
            comunityDetailsVC.modalPresentationStyle = .fullScreen
            self.present(comunityDetailsVC, animated: true, completion: nil)
        }

         }
      
 
   func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
   let searchString = searchText.replacingOccurrences(of: " ", with: "")
    print(searchString)
    searchUserThroughApiData(search : searchString)
  
   }

    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnCreateCmntClicked(_ sender: UIButton) {
        let createComunityVC = CreateComunityVC.init(nibName: "CreateComunityVC", bundle: nil)
        createComunityVC.modalPresentationStyle = .fullScreen
        self.present(createComunityVC, animated: true, completion: nil)
    }
    
    
    
    func searchUserThroughApiData(search : String) -> Void {
        
           getDataFromUrl(url: "https://fitkout.com.cs-mum-27.bluehostwebservers.com/Fitkout/index.php/api/Community/search?LoginId=1083&Keyword=\(search)&MaxCount=20&Offset=0") { (response) in
               print(response)
            
            if response.count > 0
            {
                var cArray = [CommunitySearch]()
                for anItem in response as! [Dictionary<String, Any>] {
                    
                if let netWorkObj = CommunitySearch(dictionary: anItem as [String : Any])
                {
                cArray.append(netWorkObj)
                 }
                
                  self.communityArray = cArray
                 DispatchQueue.main.async {
                  self.tableUsers.reloadData()
                 self.view.activityStopAnimating()
                    }
                   }
            }
            else
            {
                self.communityArray.removeAll()
                DispatchQueue.main.async {
                self.tableUsers.reloadData()
             self.view.activityStopAnimating()

                }
            }
           }
       }

}


class CommunitySearch {
    var CommunityId:String = ""
    var CommunityName: String = ""
    var ProfileUrl: String = ""
    
    init?(dictionary:[String:Any]) {
        guard let CommunityId = dictionary["CommunityId"],
            let CommunityName = dictionary["CommunityName"],
            let ProfileUrl = dictionary["ProfileUrl"]
           
         else{ return}
        
        self.CommunityId = CommunityId as! String
        self.CommunityName = CommunityName as! String
        self.ProfileUrl = ProfileUrl as! String
        
    }
}
