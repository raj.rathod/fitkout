//
//  UserDetail.swift
//  FitKout
//
//  Created by HT-Admin on 29/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//


import Foundation


class UserDetail : NSObject, NSCoding{

    var uniqueUsername : String!
    var userFullname : String!
    var userProfilePicture : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        uniqueUsername = dictionary["UniqueUsername"] as? String
        userFullname = dictionary["UserFullname"] as? String
        userProfilePicture = dictionary["UserProfilePicture"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if uniqueUsername != nil{
            dictionary["UniqueUsername"] = uniqueUsername
        }
        if userFullname != nil{
            dictionary["UserFullname"] = userFullname
        }
        if userProfilePicture != nil{
            dictionary["UserProfilePicture"] = userProfilePicture
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        uniqueUsername = aDecoder.decodeObject(forKey: "UniqueUsername") as? String
        userFullname = aDecoder.decodeObject(forKey: "UserFullname") as? String
        userProfilePicture = aDecoder.decodeObject(forKey: "UserProfilePicture") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if uniqueUsername != nil{
            aCoder.encode(uniqueUsername, forKey: "UniqueUsername")
        }
        if userFullname != nil{
            aCoder.encode(userFullname, forKey: "UserFullname")
        }
        if userProfilePicture != nil{
            aCoder.encode(userProfilePicture, forKey: "UserProfilePicture")
        }
    }
}
