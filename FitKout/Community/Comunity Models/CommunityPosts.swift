//
//  CommunityPosts.swift
//  FitKout
//
//  Created by HT-Admin on 29/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//


import Foundation


class CommunityPosts : NSObject, NSCoding{

    var commentsCount : String!
    var community : Community!
    var isLiked : String!
    var likesCount : String!
    var loginId : String!
    var postCaption : String!
    var postHeaderCreatedAt : String!
    var postHeaderId : String!
    var posts : [Post]!
    var postType : String!
    var userDetails : UserDetail!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        commentsCount = dictionary["CommentsCount"] as? String
        isLiked = dictionary["IsLiked"] as? String
        likesCount = dictionary["LikesCount"] as? String
        loginId = dictionary["LoginId"] as? String
        postCaption = dictionary["PostCaption"] as? String
        postHeaderCreatedAt = dictionary["PostHeaderCreatedAt"] as? String
        postHeaderId = dictionary["PostHeaderId"] as? String
        postType = dictionary["PostType"] as? String
        if let communityData = dictionary["Community"] as? [String:Any]{
            community = Community(fromDictionary: communityData)
        }
        if let userDetailsData = dictionary["UserDetails"] as? [String:Any]{
            userDetails = UserDetail(fromDictionary: userDetailsData)
        }
        posts = [Post]()
        if let postsArray = dictionary["Posts"] as? [[String:Any]]{
            for dic in postsArray{
                let value = Post(fromDictionary: dic)
                posts.append(value)
            }
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if commentsCount != nil{
            dictionary["CommentsCount"] = commentsCount
        }
        if isLiked != nil{
            dictionary["IsLiked"] = isLiked
        }
        if likesCount != nil{
            dictionary["LikesCount"] = likesCount
        }
        if loginId != nil{
            dictionary["LoginId"] = loginId
        }
        if postCaption != nil{
            dictionary["PostCaption"] = postCaption
        }
        if postHeaderCreatedAt != nil{
            dictionary["PostHeaderCreatedAt"] = postHeaderCreatedAt
        }
        if postHeaderId != nil{
            dictionary["PostHeaderId"] = postHeaderId
        }
        if postType != nil{
            dictionary["PostType"] = postType
        }
        if community != nil{
            dictionary["community"] = community.toDictionary()
        }
        if userDetails != nil{
            dictionary["userDetails"] = userDetails.toDictionary()
        }
        if posts != nil{
            var dictionaryElements = [[String:Any]]()
            for postsElement in posts {
                dictionaryElements.append(postsElement.toDictionary())
            }
            dictionary["posts"] = dictionaryElements
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        commentsCount = aDecoder.decodeObject(forKey: "CommentsCount") as? String
        community = aDecoder.decodeObject(forKey: "Community") as? Community
        isLiked = aDecoder.decodeObject(forKey: "IsLiked") as? String
        likesCount = aDecoder.decodeObject(forKey: "LikesCount") as? String
        loginId = aDecoder.decodeObject(forKey: "LoginId") as? String
        postCaption = aDecoder.decodeObject(forKey: "PostCaption") as? String
        postHeaderCreatedAt = aDecoder.decodeObject(forKey: "PostHeaderCreatedAt") as? String
        postHeaderId = aDecoder.decodeObject(forKey: "PostHeaderId") as? String
        posts = aDecoder.decodeObject(forKey: "Posts") as? [Post]
        postType = aDecoder.decodeObject(forKey: "PostType") as? String
        userDetails = aDecoder.decodeObject(forKey: "UserDetails") as? UserDetail
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if commentsCount != nil{
            aCoder.encode(commentsCount, forKey: "CommentsCount")
        }
        if community != nil{
            aCoder.encode(community, forKey: "Community")
        }
        if isLiked != nil{
            aCoder.encode(isLiked, forKey: "IsLiked")
        }
        if likesCount != nil{
            aCoder.encode(likesCount, forKey: "LikesCount")
        }
        if loginId != nil{
            aCoder.encode(loginId, forKey: "LoginId")
        }
        if postCaption != nil{
            aCoder.encode(postCaption, forKey: "PostCaption")
        }
        if postHeaderCreatedAt != nil{
            aCoder.encode(postHeaderCreatedAt, forKey: "PostHeaderCreatedAt")
        }
        if postHeaderId != nil{
            aCoder.encode(postHeaderId, forKey: "PostHeaderId")
        }
        if posts != nil{
            aCoder.encode(posts, forKey: "Posts")
        }
        if postType != nil{
            aCoder.encode(postType, forKey: "PostType")
        }
        if userDetails != nil{
            aCoder.encode(userDetails, forKey: "UserDetails")
        }
    }
}
