//
//  Community.swift
//  FitKout
//
//  Created by HT-Admin on 29/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//



import Foundation


class Community : NSObject, NSCoding{

    var communityDescription : String!
    var communityId : String!
    var communityName : String!
    var profileUrl : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        communityDescription = dictionary["CommunityDescription"] as? String
        communityId = dictionary["CommunityId"] as? String
        communityName = dictionary["CommunityName"] as? String
        profileUrl = dictionary["ProfileUrl"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if communityDescription != nil{
            dictionary["CommunityDescription"] = communityDescription
        }
        if communityId != nil{
            dictionary["CommunityId"] = communityId
        }
        if communityName != nil{
            dictionary["CommunityName"] = communityName
        }
        if profileUrl != nil{
            dictionary["ProfileUrl"] = profileUrl
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        communityDescription = aDecoder.decodeObject(forKey: "CommunityDescription") as? String
        communityId = aDecoder.decodeObject(forKey: "CommunityId") as? String
        communityName = aDecoder.decodeObject(forKey: "CommunityName") as? String
        profileUrl = aDecoder.decodeObject(forKey: "ProfileUrl") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if communityDescription != nil{
            aCoder.encode(communityDescription, forKey: "CommunityDescription")
        }
        if communityId != nil{
            aCoder.encode(communityId, forKey: "CommunityId")
        }
        if communityName != nil{
            aCoder.encode(communityName, forKey: "CommunityName")
        }
        if profileUrl != nil{
            aCoder.encode(profileUrl, forKey: "ProfileUrl")
        }
    }
}
