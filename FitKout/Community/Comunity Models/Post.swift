//
//  Post.swift
//  FitKout
//
//  Created by HT-Admin on 29/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import Foundation


class Post : NSObject, NSCoding{

    var postAspect : String!
    var postCaption : String!
    var postCreatedAt : String!
    var postHeaderId : String!
    var postId : String!
    var postMediaThumbnail : String!
    var postMediaType : String!
    var postMediaUrl : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        postAspect = dictionary["PostAspect"] as? String
        postCaption = dictionary["PostCaption"] as? String
        postCreatedAt = dictionary["PostCreatedAt"] as? String
        postHeaderId = dictionary["PostHeaderId"] as? String
        postId = dictionary["PostId"] as? String
        postMediaThumbnail = dictionary["PostMediaThumbnail"] as? String
        postMediaType = dictionary["PostMediaType"] as? String
        postMediaUrl = dictionary["PostMediaUrl"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if postAspect != nil{
            dictionary["PostAspect"] = postAspect
        }
        if postCaption != nil{
            dictionary["PostCaption"] = postCaption
        }
        if postCreatedAt != nil{
            dictionary["PostCreatedAt"] = postCreatedAt
        }
        if postHeaderId != nil{
            dictionary["PostHeaderId"] = postHeaderId
        }
        if postId != nil{
            dictionary["PostId"] = postId
        }
        if postMediaThumbnail != nil{
            dictionary["PostMediaThumbnail"] = postMediaThumbnail
        }
        if postMediaType != nil{
            dictionary["PostMediaType"] = postMediaType
        }
        if postMediaUrl != nil{
            dictionary["PostMediaUrl"] = postMediaUrl
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        postAspect = aDecoder.decodeObject(forKey: "PostAspect") as? String
        postCaption = aDecoder.decodeObject(forKey: "PostCaption") as? String
        postCreatedAt = aDecoder.decodeObject(forKey: "PostCreatedAt") as? String
        postHeaderId = aDecoder.decodeObject(forKey: "PostHeaderId") as? String
        postId = aDecoder.decodeObject(forKey: "PostId") as? String
        postMediaThumbnail = aDecoder.decodeObject(forKey: "PostMediaThumbnail") as? String
        postMediaType = aDecoder.decodeObject(forKey: "PostMediaType") as? String
        postMediaUrl = aDecoder.decodeObject(forKey: "PostMediaUrl") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if postAspect != nil{
            aCoder.encode(postAspect, forKey: "PostAspect")
        }
        if postCaption != nil{
            aCoder.encode(postCaption, forKey: "PostCaption")
        }
        if postCreatedAt != nil{
            aCoder.encode(postCreatedAt, forKey: "PostCreatedAt")
        }
        if postHeaderId != nil{
            aCoder.encode(postHeaderId, forKey: "PostHeaderId")
        }
        if postId != nil{
            aCoder.encode(postId, forKey: "PostId")
        }
        if postMediaThumbnail != nil{
            aCoder.encode(postMediaThumbnail, forKey: "PostMediaThumbnail")
        }
        if postMediaType != nil{
            aCoder.encode(postMediaType, forKey: "PostMediaType")
        }
        if postMediaUrl != nil{
            aCoder.encode(postMediaUrl, forKey: "PostMediaUrl")
        }
    }
}
