//
//  CmntyAddPostVC.swift
//  FitKout
//
//  Created by HT-Admin on 28/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit
import MobileCoreServices


class CmntyAddPostVC: UIViewController , UITextViewDelegate ,UIImagePickerControllerDelegate,UINavigationControllerDelegate , ComunitySeachVCDelegate {

    
var imagePickerController = UIImagePickerController()
    
    @IBOutlet weak var lblCmnty: UILabel!
    @IBOutlet weak var tfPostDesc: UITextView!
    
    @IBOutlet weak var stackChoose: UIStackView!
    
    @IBOutlet weak var imageCapture: UIView!
    @IBOutlet weak var imageGallery: UIView!
    @IBOutlet weak var videoGallery: UIView!
    @IBOutlet weak var videoCapture: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.tfPostDesc.layer.borderColor = #colorLiteral(red: 0.003921568627, green: 0.7764705882, blue: 0.8470588235, alpha: 1)
        self.tfPostDesc.layer.borderWidth = 1
        self.tfPostDesc.layer.cornerRadius = 15
        
        tfPostDesc.delegate = self
        tfPostDesc.text = "Write Something..."
        tfPostDesc.textColor = UIColor.lightGray
        
        hideKeyboardWhenTappedAround()
        
        let fGuesture = UITapGestureRecognizer(target: self, action: #selector(self.showDropDown(sender:)))
        stackChoose.addGestureRecognizer(fGuesture)
        stackChoose.isUserInteractionEnabled = true
        
        
        let gesturePlans1 = UITapGestureRecognizer(target: self, action:  #selector(self.imageCaptureAction))
        self.imageCapture.addGestureRecognizer(gesturePlans1)
        
        let gesturePlans2 = UITapGestureRecognizer(target: self, action:  #selector(self.imageGalleryAction))
        self.imageGallery.addGestureRecognizer(gesturePlans2)
        
        let gesturePlans3 = UITapGestureRecognizer(target: self, action:  #selector(self.videoCaptureAction))
        self.videoCapture.addGestureRecognizer(gesturePlans3)
        
        let gesturePlans4 = UITapGestureRecognizer(target: self, action:  #selector(self.videoGalleryAction))
        self.videoGallery.addGestureRecognizer(gesturePlans4)

        imagePickerController.delegate = self
        self.imagePickerController.allowsEditing = true
    }
    
    
    @objc func showDropDown(sender: AnyObject){
        let comunitySeachVC = ComunitySeachVC.init(nibName: "ComunitySeachVC", bundle: nil)
        comunitySeachVC.dropDownFlag = "1"
        comunitySeachVC.delegate = self
        comunitySeachVC.modalPresentationStyle = .fullScreen
        self.present(comunitySeachVC, animated: true, completion: nil)
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            
            imagePickerController.sourceType = UIImagePickerController.SourceType.camera
            self.present(imagePickerController, animated: true, completion: nil)
            
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(imagePickerController, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    // adding image to imageview
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
     var image : UIImage!

     if let img = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
            {
                image = img

            }
     else if let img = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            {
                image = img
            }
        
        let videoURL = info[UIImagePickerController.InfoKey.mediaURL] as? NSURL
          print(videoURL!)
//        img.image = image
        self.dismiss(animated: true, completion: nil)
    }
    // MARK: IMAGE PICKER METHODS END
    
  //MARK:- Call this function only
 
    
    @objc func imageCaptureAction(sender : UITapGestureRecognizer) {
        self.openCamera()
          }
        
    @objc func imageGalleryAction(sender : UITapGestureRecognizer) {
        self.openGallary()
              }
        
    @objc func videoCaptureAction(sender : UITapGestureRecognizer) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
             print("Camera Available")

             let imagePicker = UIImagePickerController()
             imagePicker.delegate = self
             imagePicker.sourceType = .camera
             imagePicker.mediaTypes = [kUTTypeMovie as String]
             imagePicker.allowsEditing = false

             self.present(imagePicker, animated: true, completion: nil)
         } else {
             print("Camera UnAvaialable")
         }
              }
    
    @objc func videoGalleryAction(sender : UITapGestureRecognizer) {
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.delegate = self
        imagePickerController.mediaTypes = ["public.movie"]
        present(imagePickerController, animated: true, completion: nil)
        
    }

    
    func textViewDidBeginEditing(_ textView: UITextView) {

        if tfPostDesc.textColor == UIColor.lightGray {
            tfPostDesc.text = ""
            tfPostDesc.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {

        if tfPostDesc.text == "" {
           tfPostDesc.text = "Write Something..."
           tfPostDesc.textColor = UIColor.lightGray
        }
    }
    
    func selectedCommunity(selectedCmty: String) {
        self.lblCmnty.text = selectedCmty
    }

    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    

}
