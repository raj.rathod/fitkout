//
//  CommunityHomeVC.swift
//  FitKout
//
//  Created by HT-Admin on 28/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class CommunityHomeVC: UIViewController ,UICollectionViewDataSource,UICollectionViewDelegate ,UITableViewDelegate,UITableViewDataSource {
    
    var communityArray = [CommunitySearch]()

    
    @IBOutlet weak var collectionComunities: UICollectionView!
    @IBOutlet weak var tablePosts: UITableView!
    
    var imgUArr = [#imageLiteral(resourceName: "dummy2"),#imageLiteral(resourceName: "dummy"),#imageLiteral(resourceName: "workout_image"),#imageLiteral(resourceName: "dummy2"),#imageLiteral(resourceName: "dummy"),#imageLiteral(resourceName: "workout_image"),#imageLiteral(resourceName: "dummy2"),#imageLiteral(resourceName: "dummy"),#imageLiteral(resourceName: "workout_image"),#imageLiteral(resourceName: "dummy2")]

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        searchUserThroughApiData(search: "")
        
        collectionComunities.register(UINib(nibName: "CommunitiesCell", bundle: nil), forCellWithReuseIdentifier: "CommunitiesCell")
        
        tablePosts.register(UINib(nibName: "PostsTVCell", bundle: nil), forCellReuseIdentifier: "PostsTVCell")
        tablePosts.tableFooterView = UIView()
        tablePosts.separatorStyle = .none
        tablePosts.reloadData()
    }

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         
             return 10
     }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
             let cell = tableView.dequeueReusableCell(withIdentifier: "PostsTVCell", for: indexPath) as! PostsTVCell
              
              
         cell.btnLike.tag = indexPath.row
         cell.btnLike.addTarget(self, action:  #selector(likeBtnPressed(sender:)), for: .touchUpInside)
         cell.btnComment.tag = indexPath.row
         cell.btnComment.addTarget(self, action:  #selector(commentBtnPressed(sender:)), for: .touchUpInside)
         cell.btnShare.tag = indexPath.row
         cell.btnShare.addTarget(self, action:  #selector(shareBtnPressed(sender:)), for: .touchUpInside)
             
             return cell
        
          }
          
          
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
             
               return 450
          }
          
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         
          }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
                return 1
            }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return communityArray.count
            }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CommunitiesCell", for: indexPath) as! CommunitiesCell
      let csModel: CommunitySearch
      csModel = communityArray[indexPath.row]
      cell.imgCmnty.loadImageUsingCache(withUrl : csModel.ProfileUrl)
      cell.lblCmnty.text = csModel.CommunityName
      return cell;
            }
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let csModel: CommunitySearch
        csModel = communityArray[indexPath.row]
        let comunityDetailsVC = ComunityDetailsVC.init(nibName: "ComunityDetailsVC", bundle: nil)
        comunityDetailsVC.communityId = csModel.CommunityId
        comunityDetailsVC.modalPresentationStyle = .fullScreen
        self.present(comunityDetailsVC, animated: true, completion: nil)
     }
     
     
     @objc func likeBtnPressed(sender: UIButton) {
           print(sender.tag)
         let likeVC = LikesVC.init(nibName: "LikesVC", bundle: nil)
         likeVC.modalPresentationStyle = .fullScreen
        self.present(likeVC, animated: true, completion: nil)
          
       }
     
     @objc func commentBtnPressed(sender: UIButton) {
         print(sender.tag)
        let commentVC = CommentVC.init(nibName: "CommentVC", bundle: nil)
        commentVC.modalPresentationStyle = .fullScreen
        self.present(commentVC, animated: true, completion: nil)
         
     }
     @objc func shareBtnPressed(sender: UIButton) {
         print(sender.tag)
        let shareVC = ShareVC.init(nibName: "ShareVC", bundle: nil)
        shareVC.modalPresentationStyle = .fullScreen
        self.present(shareVC, animated: true, completion: nil)
         
     }
     
    
    
    

    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSearchClicked(_ sender: UIBarButtonItem) {
        let comunitySeachVC = ComunitySeachVC.init(nibName: "ComunitySeachVC", bundle: nil)
        comunitySeachVC.modalPresentationStyle = .fullScreen
        self.present(comunitySeachVC, animated: true, completion: nil)
    }
    
    @IBAction func btnShareClicked(_ sender: UIBarButtonItem) {
        let comunityShareVC = ComunityShareVC.init(nibName: "ComunityShareVC", bundle: nil)
        comunityShareVC.modalPresentationStyle = .fullScreen
      self.present(comunityShareVC, animated: true, completion: nil)
        
    }
    
    
    @IBAction func btnAddPostClicked(_ sender: UIButton) {
        let cmntyAddPostVC = CmntyAddPostVC.init(nibName: "CmntyAddPostVC", bundle: nil)
        cmntyAddPostVC.modalPresentationStyle = .fullScreen
      self.present(cmntyAddPostVC, animated: true, completion: nil)
    }
    
    
    @IBAction func btnFStepsClicked(_ sender: UIBarButtonItem) {
    let footStepsVC = FootStepsVC.init(nibName: "FootStepsVC", bundle: nil)
    footStepsVC.modalPresentationStyle = .fullScreen
    self.present(footStepsVC, animated: true, completion: nil)
    }
    
    
    func searchUserThroughApiData(search : String) -> Void {
        
           getDataFromUrl(url: "https://fitkout.com.cs-mum-27.bluehostwebservers.com/Fitkout/index.php/api/Community/search?LoginId=1083&Keyword=\(search)&MaxCount=20&Offset=0") { (response) in
               print(response)
            
                var cArray = [CommunitySearch]()
                for anItem in response as! [Dictionary<String, Any>] {
                    
                if let netWorkObj = CommunitySearch(dictionary: anItem as [String : Any])
                {
                cArray.append(netWorkObj)
                 }
                
                  self.communityArray = cArray
                 DispatchQueue.main.async {
                  self.collectionComunities.reloadData()
                    }
                   }
           }
       }
    
}
