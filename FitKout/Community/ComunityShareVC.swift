//
//  ComunityShareVC.swift
//  FitKout
//
//  Created by HT-Admin on 28/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class ComunityShareVC: UIViewController {
    
    @IBOutlet weak var tfDesc: UITextField!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUName: UILabel!
    @IBOutlet weak var viewExercise: UIView!
    @IBOutlet weak var viewBfrAndAfr: UIView!
    @IBOutlet weak var viewFood: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        hideKeyboardWhenTappedAround()
        

    }
    
    
    @IBAction func btnShareExerciseClicked(_ sender: UIButton) {
        let shareExerciseVC = ShareExerciseVC.init(nibName: "ShareExerciseVC", bundle: nil)
        shareExerciseVC.modalPresentationStyle = .fullScreen
        self.present(shareExerciseVC, animated: true, completion: nil)
    }
    
    
    @IBAction func btnBfrAftrClicked(_ sender: UIButton) {
        let beforeAfterShareVC = BeforeAfterShareVC.init(nibName: "BeforeAfterShareVC", bundle: nil)
        beforeAfterShareVC.modalPresentationStyle = .fullScreen
        self.present(beforeAfterShareVC, animated: true, completion: nil)
    }
    
    @IBAction func btnFoodClicked(_ sender: UIButton) {
        let foodShareVC = FoodShareVC.init(nibName: "FoodShareVC", bundle: nil)
        foodShareVC.modalPresentationStyle = .fullScreen
   self.present(foodShareVC, animated: true, completion: nil)
        
    }
    
    
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
