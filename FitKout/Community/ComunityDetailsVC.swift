//
//  ComunityDetailsVC.swift
//  FitKout
//
//  Created by HT-Admin on 28/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class ComunityDetailsVC: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    
    var communityId = ""
    
    var communityPostArray = [CommunityPosts]()

    
    @IBOutlet weak var lblCName: UILabel!
    @IBOutlet weak var lblCPosts: UILabel!
    @IBOutlet weak var viewImg: UIView!
    @IBOutlet weak var tableViewPosts: UITableView!
    @IBOutlet weak var viewUserData: UIView!
    @IBOutlet weak var viewPostData: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        fetchCommunityPosts(communityId: communityId)
        
        setBackGroundImageWithImage(image : #imageLiteral(resourceName: "temp_circuit") , targetView : viewUserData)
        setBackGroundImageWithImage(image : #imageLiteral(resourceName: "temp_circuit") , targetView : viewImg)
        viewImg.layer.borderWidth = 3
        viewImg.layer.borderColor = #colorLiteral(red: 0.003921568627, green: 0.7764705882, blue: 0.8470588235, alpha: 1).cgColor
        
        viewPostData.clipsToBounds = true
        viewPostData.layer.cornerRadius = 40
        viewPostData.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        tableViewPosts.register(UINib(nibName: "PostsTVCell", bundle: nil), forCellReuseIdentifier: "PostsTVCell")
        tableViewPosts.tableFooterView = UIView()
        tableViewPosts.separatorStyle = .none
        tableViewPosts.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return communityPostArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
            let cell = tableView.dequeueReusableCell(withIdentifier: "PostsTVCell", for: indexPath) as! PostsTVCell
             
             let csModel: CommunityPosts
             csModel = communityPostArray[indexPath.row]
        
        cell.btnLike.tag = indexPath.row
        cell.btnLike.addTarget(self, action:  #selector(likeBtnPressed(sender:)), for: .touchUpInside)
        cell.btnComment.tag = indexPath.row
        cell.btnComment.addTarget(self, action:  #selector(commentBtnPressed(sender:)), for: .touchUpInside)
        cell.btnShare.tag = indexPath.row
        cell.btnShare.addTarget(self, action:  #selector(shareBtnPressed(sender:)), for: .touchUpInside)
            
            return cell
       
         }
         
         
         func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            
              return 450
         }
         
         func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
         }
    
    @objc func likeBtnPressed(sender: UIButton) {
          print(sender.tag)
        let likeVC = LikesVC.init(nibName: "LikesVC", bundle: nil)
        likeVC.modalPresentationStyle = .fullScreen
       self.present(likeVC, animated: true, completion: nil)
         
      }
    
    @objc func commentBtnPressed(sender: UIButton) {
        print(sender.tag)
       let commentVC = CommentVC.init(nibName: "CommentVC", bundle: nil)
       commentVC.modalPresentationStyle = .fullScreen
       self.present(commentVC, animated: true, completion: nil)
        
    }
    @objc func shareBtnPressed(sender: UIButton) {
        print(sender.tag)
       let shareVC = ShareVC.init(nibName: "ShareVC", bundle: nil)
       shareVC.modalPresentationStyle = .fullScreen
       self.present(shareVC, animated: true, completion: nil)
        
    }
    
    
    func fetchCommunityPosts(communityId : String) -> Void {
        
           getDataFromUrl(url: "https://fitkout.com.cs-mum-27.bluehostwebservers.com/Fitkout/index.php/api/Community/feed?LoginId=1083&CommunityId=\(communityId)") { (response) in
               print(response)
            
          var cArray = [CommunityPosts]()
            for anItem in response as! [Dictionary<String, Any>] {
                
             let netWorkObj = CommunityPosts(fromDictionary: anItem)
            cArray.append(netWorkObj)
              self.communityPostArray = cArray
             DispatchQueue.main.async {
              self.tableViewPosts.reloadData()
                }
            }
            
      
           }
       }
    
    @IBAction func btnFollowClicked(_ sender: UIButton) {
    }
    
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
