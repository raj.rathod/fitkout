//
//  BfrAfterSharingVC.swift
//  FitKout
//
//  Created by HT-Admin on 02/12/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class BfrAfterSharingVC: UIViewController {
    
    var imageBfr = UIImage()
    var imageAfr = UIImage()

    
    @IBOutlet weak var imgBfr: UIImageView!
    @IBOutlet weak var imgAftr: UIImageView!
    @IBOutlet weak var tfBfrAfr: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tfBfrAfr.layer.borderWidth = 1
        tfBfrAfr.layer.borderColor = #colorLiteral(red: 0.003921568627, green: 0.7764705882, blue: 0.8470588235, alpha: 1)
        
        imgBfr.image = imageBfr
        imgAftr.image = imageAfr
    }


    @IBAction func btnCloseClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnPostClicked(_ sender: UIButton) {
    }
    
}
