//
//  BeforeAfterShareVC.swift
//  FitKout
//
//  Created by HT-Admin on 02/12/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class BeforeAfterShareVC: UIViewController,UIPopoverPresentationControllerDelegate ,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
var imagePickerController = UIImagePickerController()
    
    var imageFlag = ""
    
    @IBOutlet weak var imgAfr: UIImageView!
    @IBOutlet weak var imgBfr: UIImageView!
    @IBOutlet weak var viewCnt: UIView!
    
    @IBOutlet weak var viewAfr: UIView!
    @IBOutlet weak var viewBfr: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        viewCnt.clipsToBounds = true
        viewCnt.layer.cornerRadius = 40
        viewCnt.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        imagePickerController.delegate = self
        self.imagePickerController.allowsEditing = true
    }

    

    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            
            imagePickerController.sourceType = UIImagePickerController.SourceType.camera
            self.present(imagePickerController, animated: true, completion: nil)
            
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(imagePickerController, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    // adding image to imageview
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
     var image : UIImage!

     if let img = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
            {
                image = img

            }
     else if let img = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            {
                image = img
            }

        if (imageFlag == "1")
        {
            imgBfr.image = image

        }else
        {
            imgAfr.image = image
        }
      //  imgUser.image = image
        self.dismiss(animated: true, completion: nil)
    }
    // MARK: IMAGE PICKER METHODS END

    @IBAction func btnBeforeClicked(_ sender: UIButton) {
        
        imageFlag = "1"
        
              let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
              alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
                  self.openCamera()
              }))
              
              alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                  self.openGallary()
              }))
              
              alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
              
              /*If you want work actionsheet on ipad
               then you have to use popoverPresentationController to present the actionsheet,
               otherwise app will crash on iPad */
              switch UIDevice.current.userInterfaceIdiom {
              case .pad:
                  alert.popoverPresentationController?.sourceView = view
                  alert.popoverPresentationController?.sourceRect = view.bounds
                  alert.popoverPresentationController?.permittedArrowDirections = .up
              default:
                  break
              }
              
              self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func btnAfrClicked(_ sender: UIButton) {
        imageFlag = "2"
        
              let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
              alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
                  self.openCamera()
              }))
              
              alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                  self.openGallary()
              }))
              
              alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
              
              /*If you want work actionsheet on ipad
               then you have to use popoverPresentationController to present the actionsheet,
               otherwise app will crash on iPad */
              switch UIDevice.current.userInterfaceIdiom {
              case .pad:
                  alert.popoverPresentationController?.sourceView = view
                  alert.popoverPresentationController?.sourceRect = view.bounds
                  alert.popoverPresentationController?.permittedArrowDirections = .up
              default:
                  break
              }
              
              self.present(alert, animated: true, completion: nil)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
           return UIModalPresentationStyle.none
       }
    @IBAction func btnShareClicked(_ sender: UIButton) {
        let controller = BfrAfterSharingVC()
        controller.imageBfr = viewBfr.screenshot()
        controller.imageAfr = viewAfr.screenshot()
        controller.preferredContentSize = CGSize(width: view.bounds.size.width,height: view.bounds.size.height)
        controller.modalPresentationStyle = UIModalPresentationStyle.popover
        let popController = controller.popoverPresentationController
        popController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
        popController?.delegate = self
        popController?.sourceRect = CGRect(x: 0, y:0 , width: view.bounds.size.width, height: view.bounds.size.height )
        popController?.sourceView =  view
        self.present(controller, animated: true, completion: nil)
    }
    
    
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
