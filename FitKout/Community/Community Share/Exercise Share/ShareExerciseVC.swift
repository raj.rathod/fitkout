//
//  ShareExerciseVC.swift
//  FitKout
//
//  Created by HT-Admin on 02/12/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class ShareExerciseVC: UIViewController , UITableViewDelegate, UITableViewDataSource,UIPopoverPresentationControllerDelegate ,DatePickerVCDelegate {
    
    
    @IBOutlet weak var lblDateSlctr: UILabel!
    @IBOutlet weak var viewfrst: UIView!
    @IBOutlet weak var dateSlctrStack: UIStackView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblExercise: UILabel!
    @IBOutlet weak var tableExercise: UITableView!
    @IBOutlet weak var lblCalResults: UILabel!
    @IBOutlet weak var lblCalsCount: UILabel!
    
    
    
    @IBOutlet weak var viewSec: UIView!
    
    let animals = ["Exercise 1", "Exercise 2", "Exercise 3", "Exercise 4", "Exercise 5"]

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableExercise.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        
        setBackGroundViewToView(targetView : viewfrst)
        setBackGroundImageWithImage(image : #imageLiteral(resourceName: "exercise_background") , targetView : viewfrst)
        
        viewSec.clipsToBounds = true
        viewSec.layer.cornerRadius = 40
        viewSec.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        let fGuesture = UITapGestureRecognizer(target: self, action: #selector(self.showDropDownForAW(sender:)))
        dateSlctrStack.addGestureRecognizer(fGuesture)
        dateSlctrStack.isUserInteractionEnabled = true
        
       
    }
    
    // number of rows in table view
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return self.animals.count
       }

       // create a cell for each table view row
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

           // create a new cell if needed or reuse an old one
        let cell = tableExercise.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.backgroundColor = .clear
        cell.textLabel?.textColor = .white
        cell.textLabel?.textAlignment = .center
        cell.textLabel?.font = UIFont(name:"Roboto-Bold", size: 16.0)
        cell.textLabel?.text = self.animals[indexPath.row]
        return cell
       }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 21
    }
    

    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
 
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    @IBAction func btnShareClicked(_ sender: UIButton) {
        
        
        
        let controller = ExShareFinalVC()
        
        controller.imgScreenshot = viewfrst.screenshot()
        controller.preferredContentSize = CGSize(width: view.bounds.size.width,height: view.bounds.size.height)
        controller.modalPresentationStyle = UIModalPresentationStyle.popover
        let popController = controller.popoverPresentationController
        popController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
        popController?.delegate = self
        popController?.sourceRect = CGRect(x: 0, y:0 , width: view.bounds.size.width, height: view.bounds.size.height )
        popController?.sourceView =  view
        self.present(controller, animated: true, completion: nil)
    }
    
    

    
    @objc func showDropDownForAW(sender: AnyObject){
          
      let controller = DatePickerVC()
      controller.delegate = self
      controller.preferredContentSize = CGSize(width: view.bounds.size.width,height: 374)
      controller.modalPresentationStyle = UIModalPresentationStyle.popover
      let popController = controller.popoverPresentationController
      popController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
      popController?.delegate = self
      popController?.sourceRect = CGRect(x: self.view.bounds.midX , y:self.view.bounds.midY , width: 0, height: 0 )
      popController?.sourceView =  view
      self.present(controller, animated: true, completion: nil)
          
      }
    func selectedDate(selected: String) {
        lblDate.text = selected
        lblDateSlctr.text = selected
    }
    
  
    
}
