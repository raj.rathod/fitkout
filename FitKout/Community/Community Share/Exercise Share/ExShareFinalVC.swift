//
//  ExShareFinalVC.swift
//  FitKout
//
//  Created by HT-Admin on 02/12/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit



class ExShareFinalVC: UIViewController {

    @IBOutlet weak var imgCaptured: UIImageView!
    @IBOutlet weak var tfDesc: UITextField!
    @IBOutlet weak var cntView: UIView!
    
    var imgScreenshot = UIImage()
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tfDesc.layer.borderWidth = 1
        tfDesc.layer.borderColor = #colorLiteral(red: 0.003921568627, green: 0.7764705882, blue: 0.8470588235, alpha: 1)
        imgCaptured.image = imgScreenshot
    }

   
    
    @IBAction func btnBackPressed(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnPostClicked(_ sender: UIButton) {
    }
    
}
