//
//  FoodSharingVC.swift
//  FitKout
//
//  Created by HT-Admin on 03/12/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class FoodSharingVC: UIViewController {

    var imgCaptured = UIImage()
    
    @IBOutlet weak var imgSharing: UIImageView!
    @IBOutlet weak var tfFoodShare: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.imgSharing.image = imgCaptured
    }

    
    @IBAction func btnCloseClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnShareClicked(_ sender: UIButton) {
    }
    

}
