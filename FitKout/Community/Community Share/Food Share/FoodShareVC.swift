//
//  FoodShareVC.swift
//  FitKout
//
//  Created by HT-Admin on 02/12/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class FoodShareVC: UIViewController,UITableViewDelegate,UITableViewDataSource ,UICollectionViewDataSource,UICollectionViewDelegate , UICollectionViewDelegateFlowLayout , UIPopoverPresentationControllerDelegate , DatePickerVCDelegate{
    
    var arrayMeal = [String]()
    
    @IBOutlet weak var viewSec: UIView!
    
    @IBOutlet weak var viewFrst: UIView!
    
    @IBOutlet weak var lblProteins: UILabel!
    @IBOutlet weak var lblFats: UILabel!
    @IBOutlet weak var lblCarbs: UILabel!
    @IBOutlet weak var lblTotalsCals: UILabel!
    @IBOutlet weak var lblDateResults: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var dateSelectorStack: UIStackView!
    @IBOutlet weak var collectionMeal: UICollectionView!
    @IBOutlet weak var viewSecCnt: UIView!
    @IBOutlet weak var tableCals: UITableView!
    
    var imgArray = [ #imageLiteral(resourceName: "workout_image"),#imageLiteral(resourceName: "nurtion_icon"),#imageLiteral(resourceName: "nober_gym_icon"),#imageLiteral(resourceName: "background_image"),#imageLiteral(resourceName: "temp_categories"),#imageLiteral(resourceName: "personalt_trainer_icon"), #imageLiteral(resourceName: "temp_circuit"),#imageLiteral(resourceName: "temp_categories"),#imageLiteral(resourceName: "nober_gym_icon"),#imageLiteral(resourceName: "background_image"),#imageLiteral(resourceName: "temp_categories"),#imageLiteral(resourceName: "personalt_trainer_icon")]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setBackGroundViewToView(targetView : viewFrst)
        setBackGroundImageWithImage(image : #imageLiteral(resourceName: "food_background") , targetView : viewFrst)
        
        viewSec.clipsToBounds = true
        viewSec.layer.cornerRadius = 40
        viewSec.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]

        arrayMeal = ["Brown Rice","Wheat" ,"Brown Cread","Veg Juice","Raw Veg","Proteins","Eggs"]
        
        tableCals.register(UINib(nibName: "FoodShareCell", bundle: nil), forCellReuseIdentifier: "FoodShareCell")
        tableCals.tableFooterView = UIView()
        tableCals.separatorStyle = .none
        tableCals.reloadData()
        
        collectionMeal.register(UINib(nibName: "CommunitiesCell", bundle: nil), forCellWithReuseIdentifier: "CommunitiesCell")
        
        let fGuesture = UITapGestureRecognizer(target: self, action: #selector(self.showDropDownForAW(sender:)))
        dateSelectorStack.addGestureRecognizer(fGuesture)
        dateSelectorStack.isUserInteractionEnabled = true

    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return arrayMeal.count
          }
          
          func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
              let cell = tableView.dequeueReusableCell(withIdentifier: "FoodShareCell", for: indexPath) as! FoodShareCell
            cell.lblFood.text = arrayMeal[indexPath.row]
             cell.lblCals.text = "10 cals"
              return cell
              
          }
          
          
          func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
              return 21
          }
          
          func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
          }
    
    
    
    
    //this method is for the size of items
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           let width = collectionView.frame.width/3
        //   let height : CGFloat = 180.0
           return CGSize(width: width, height: width)
       }
       //these methods are to configure the spacing between items

       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
           return UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
       }

       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
           return 0
       }

       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
           return 0
       }
       

    func numberOfSections(in collectionView: UICollectionView) -> Int {
                  return 1
              }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
              
        return imgArray.count
              
              }

           
           
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CommunitiesCell", for: indexPath) as! CommunitiesCell
                
        cell.imgCmnty.image = imgArray[indexPath.item]
          return cell;
              
              }
     
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
         
     }
     
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
           return UIModalPresentationStyle.none
       }
    
    @objc func showDropDownForAW(sender: AnyObject){
          
                     let controller = DatePickerVC()
                     controller.delegate = self
                     controller.preferredContentSize = CGSize(width: view.bounds.size.width,height: 374)

                     controller.modalPresentationStyle = UIModalPresentationStyle.popover
                     let popController = controller.popoverPresentationController
                     popController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
                     popController?.delegate = self
                     popController?.sourceRect = CGRect(x: self.view.bounds.midX , y:self.view.bounds.midY , width: 0, height: 0 )
                     popController?.sourceView =  view
                     self.present(controller, animated: true, completion: nil)
          
      }
    func selectedDate(selected: String) {
        lblDate.text = selected
        lblDateResults.text = selected
    }
    
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnShareClicked(_ sender: UIButton) {
        
        let controller = FoodSharingVC()
        controller.imgCaptured = viewFrst.screenshot()
        controller.preferredContentSize = CGSize(width: view.bounds.size.width,height: view.bounds.size.height)
        controller.modalPresentationStyle = UIModalPresentationStyle.popover
        let popController = controller.popoverPresentationController
        popController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
        popController?.delegate = self
        popController?.sourceRect = CGRect(x: 0, y:0 , width: view.bounds.size.width, height: view.bounds.size.height )
        popController?.sourceView =  view
        self.present(controller, animated: true, completion: nil)
    }
    
    
}
