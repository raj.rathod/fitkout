//
//  NutritionCell.swift
//  FitKout
//
//  Created by HT-Admin on 28/10/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class NutritionCell: UITableViewCell , UIPopoverPresentationControllerDelegate{

    var number: Int!

    @IBOutlet weak var tableMeals: UITableView!
    @IBOutlet weak var nutritionView: UIView!
    @IBOutlet weak var lblMeal: UILabel!
    @IBOutlet weak var lblCalorie: UILabel!
    @IBOutlet weak var btnAdd: UIButton!
    
    
    
       @IBInspectable var cornerRadius: CGFloat = 10
       @IBInspectable var shadowOffsetWidth: Int = 0
       @IBInspectable var shadowOffsetHeight: Int = 0
       @IBInspectable var shadowColor: UIColor? = UIColor.darkGray
       @IBInspectable var shadowOpacity: Float = 0.2
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        tableMeals.delegate = self
        tableMeals.dataSource = self
        
        tableMeals.register(UINib(nibName: "NutMealSecCell", bundle: nil), forCellReuseIdentifier: "NutMealSecCell")
        tableMeals.tableFooterView = UIView()
        tableMeals.separatorStyle = .none
        tableMeals.reloadData()
        
     
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    override func layoutSubviews() {

        nutritionView.layer.cornerRadius = cornerRadius
        nutritionView.layer.masksToBounds = false
        nutritionView.layer.shadowColor = shadowColor?.cgColor
        nutritionView.layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        nutritionView.layer.shadowOpacity = shadowOpacity
        nutritionView.layer.borderWidth = 1.0
        nutritionView.layer.borderColor = UIColor(red:169, green:169, blue:169, alpha:1.0).cgColor

    }
}

extension NutritionCell: UITableViewDelegate {
    
}

extension NutritionCell: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if nil == number {
            number = 1 + Int(arc4random_uniform(UInt32(10 - 1 + 1)))
        }
        return number
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NutMealSecCell", for: indexPath) as! NutMealSecCell
       // cell.setupData(number: indexPath.row)
        
        cell.btnMore.tag = indexPath.row
        cell.btnMore.addTarget(self, action:  #selector(moreBtnPressed(sender:)), for: .touchUpInside)
        cell.sizeToFit()
        cell.layoutIfNeeded()
        return cell
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    @objc func moreBtnPressed(sender: UIButton) {
        print(sender.tag)
        let controller = MoreVC()
        controller.preferredContentSize = CGSize(width: 80,height: 80)
        controller.modalPresentationStyle = UIModalPresentationStyle.popover
        let popController = controller.popoverPresentationController
        popController?.permittedArrowDirections = UIPopoverArrowDirection.down
        popController?.delegate = self
        let viewForSource = sender as UIView
        popController?.sourceView = viewForSource
        // the position of the popover where it's showed
        popController?.sourceRect = viewForSource.bounds
        
       DispatchQueue.main.async {
        self.getTopMostViewController()?.present(controller, animated: true, completion: nil)
        }
       
    }
    
    
    func getTopMostViewController() -> UIViewController? {
        var topMostViewController = UIApplication.shared.keyWindow?.rootViewController

        while let presentedViewController = topMostViewController?.presentedViewController {
            topMostViewController = presentedViewController
        }

        return topMostViewController
    }
}
