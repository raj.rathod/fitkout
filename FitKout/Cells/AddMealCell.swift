//
//  AddMealCell.swift
//  FitKout
//
//  Created by HT-Admin on 29/10/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class AddMealCell: UITableViewCell {

    @IBOutlet weak var lblMeal: UILabel!
    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var btnAddMeal: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
