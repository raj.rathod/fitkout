//
//  GDTimelineCell.swift
//  FitKout
//
//  Created by HT-Admin on 31/10/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class GDTimelineCell: UITableViewCell {

    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    
    @IBOutlet weak var btnLevelStatus: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
