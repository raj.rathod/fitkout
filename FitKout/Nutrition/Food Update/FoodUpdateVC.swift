//
//  FoodUpdateVC.swift
//  FitKout
//
//  Created by HT-Admin on 29/10/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class FoodUpdateVC: UIViewController , UIPickerViewDelegate, UIPickerViewDataSource {
   
    @IBOutlet weak var pickerQty: UIPickerView!
    @IBOutlet weak var pickerQtyType: UIPickerView!
    @IBOutlet weak var imgLike: UIImageView!
    
    var qtyArray: [String] = [String]()
    var qtyTypeArray: [String] = [String]()
    
    var iconClick = true

    @IBOutlet weak var tfQtyEntered: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        hideKeyboardWhenTappedAround()
        qtyArray = ["1", "2", "3", "4", "5", "6","7","8","9","10"]
        qtyTypeArray = ["Bowl","Kg","Gm"]
        
        iconClick = true
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imgVwTapped(tapGestureRecognizer:)))
        imgLike.isUserInteractionEnabled = true
        imgLike.addGestureRecognizer(tapGestureRecognizer)
        
    }
    
    @objc func imgVwTapped(tapGestureRecognizer: UITapGestureRecognizer)
      {
          if(iconClick == true) {
              imgLike.image =  #imageLiteral(resourceName: "unlike_heart")
          } else {
              imgLike.image =  #imageLiteral(resourceName: "heart")
          }
          
          iconClick = !iconClick
      }

   func numberOfComponents(in pickerView: UIPickerView) -> Int {
         1
      }
      
      func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
         if pickerView == pickerQty
          {
            return qtyArray.count
          }else
          {
            return qtyTypeArray.count
          }
      }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == pickerQty
        {
          return qtyArray[row]
        }else
        {
          return qtyTypeArray[row]
        }
        
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent  component: Int) {
        
        if pickerView == pickerQty
                 {
                  let qtyValueSelected = qtyArray[row] as String
                         print(qtyValueSelected)
                    tfQtyEntered.text = qtyValueSelected
                    
                 }else
                 {
                   let unitsValueSelect = qtyTypeArray[row] as String
                          print(unitsValueSelect)
                 }
         }
   
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "updateMealNotif"), object: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
}
