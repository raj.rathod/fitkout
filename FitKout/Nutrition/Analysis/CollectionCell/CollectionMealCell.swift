//
//  CollectionMealCell.swift
//  FitKout
//
//  Created by HT-Admin on 30/10/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class CollectionMealCell: UICollectionViewCell {
    
    @IBOutlet weak var lblMealName: UILabel!
    
    @IBOutlet weak var lblQty: UILabel!
    
    @IBOutlet weak var indicatorView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
