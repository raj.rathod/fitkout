//
//  AnalysisVC.swift
//  FitKout
//
//  Created by HT-Admin on 30/10/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class AnalysisVC: UIViewController ,  UICollectionViewDataSource,UICollectionViewDelegate ,DatePickerVCDelegate , UIPopoverPresentationControllerDelegate{
    
    
    @IBOutlet weak var lblDate: UILabel!
    
    
    @IBOutlet weak var collectionViewMeal: UICollectionView!
    
    @IBOutlet weak var proteinProgress: CircularProgressView!
    @IBOutlet weak var fatProgress: CircularProgressView!
    @IBOutlet weak var carbsProgress: CircularProgressView!
    
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var cntView: UIView!
    @IBOutlet weak var datePickerStack: UIStackView!
    
    @IBOutlet weak var progressBig: CircularProgressBig!
    
    @IBOutlet weak var graphView: UIView!
    
    var arrMeal = [String]()
     var colors: [UIColor] = [
        UIColor(red: 0.121569, green: 0.466667, blue: 0.705882, alpha: 1),
        UIColor(red: 1, green: 0.498039, blue: 0.054902, alpha: 1),
        UIColor(red: 0.172549, green: 0.627451, blue: 0.172549, alpha: 1),
        UIColor(red: 0.839216, green: 0.152941, blue: 0.156863, alpha: 1),
        UIColor(red: 0.580392, green: 0.403922, blue: 0.741176, alpha: 1),
        UIColor(red: 0.54902, green: 0.337255, blue: 0.294118, alpha: 1),
        UIColor(red: 0.890196, green: 0.466667, blue: 0.760784, alpha: 1),
        UIColor(red: 0.498039, green: 0.498039, blue: 0.498039, alpha: 1),
        UIColor(red: 0.737255, green: 0.741176, blue: 0.133333, alpha: 1),
        UIColor(red: 0.0901961, green: 0.745098, blue: 0.811765, alpha: 1)
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setGradientBackgroundReverse(view:self.parentView)
      //  setGradientBackgroundReverse(view:self.view)

        cntView.clipsToBounds = true
        cntView.layer.cornerRadius = 40
        cntView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        proteinProgress.trackClr = #colorLiteral(red: 0.9215686275, green: 0.9215686275, blue: 0.9215686275, alpha: 1)
        proteinProgress.progressClr = #colorLiteral(red: 0.03137254902, green: 0.8, blue: 0.9215686275, alpha: 1)
        proteinProgress.setProgressWithAnimation(duration: 1.0, value: 0.65)
        fatProgress.trackClr = #colorLiteral(red: 0.9215686275, green: 0.9215686275, blue: 0.9215686275, alpha: 1)
        fatProgress.progressClr = #colorLiteral(red: 0.003921568627, green: 0.6156862745, blue: 0.8470588235, alpha: 1)
        fatProgress.setProgressWithAnimation(duration: 1.0, value: 0.15)
        carbsProgress.trackClr = #colorLiteral(red: 0.9215686275, green: 0.9215686275, blue: 0.9215686275, alpha: 1)
        carbsProgress.progressClr = #colorLiteral(red: 0, green: 0.1529411765, blue: 0.5058823529, alpha: 1)
        carbsProgress.setProgressWithAnimation(duration: 1.0, value: 0.25)
        
       
        progressBig.trackClr = #colorLiteral(red: 0.9215686275, green: 0.9215686275, blue: 0.9215686275, alpha: 1)
        progressBig.progressClr = #colorLiteral(red: 0, green: 0.8126131296, blue: 0.8772806525, alpha: 1)
        progressBig.setProgressWithAnimation(duration: 1.0, value: 0.45)
        
        
        let controller = LineGraphVC()
        controller.view.frame = CGRect(x: 0, y:0, width: graphView.bounds.size.width, height: graphView.bounds.size.height )
        graphView .addSubview(controller.view)
        self.addChild(controller)
        
        collectionViewMeal.register(UINib(nibName: "CollectionMealCell", bundle: nil), forCellWithReuseIdentifier: "CollectionMealCell")
        
        arrMeal = ["BREAK FAST","LUNCH" ]
        
        let fGuesture = UITapGestureRecognizer(target: self, action: #selector(self.showDropDown(sender:)))
        datePickerStack.addGestureRecognizer(fGuesture)
        datePickerStack.isUserInteractionEnabled = true
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
   @objc func showDropDown(sender: AnyObject){
       
                  let controller = DatePickerVC()
                //  controller.datePickerTitle = "Select Date"

                  controller.delegate = self
                  controller.preferredContentSize = CGSize(width: view.bounds.size.width,height: 374)

                  controller.modalPresentationStyle = UIModalPresentationStyle.popover
                  let popController = controller.popoverPresentationController
                  popController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
                  popController?.delegate = self
                  popController?.sourceRect = CGRect(x: self.view.bounds.midX , y:self.view.bounds.midY , width: 0, height: 0 )
                  popController?.sourceView =  view
                  self.present(controller, animated: true, completion: nil)
       
   }
    
    @IBAction func backBtnClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
           return 1
       }

       func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrMeal.count
       }

    
    
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionMealCell", for: indexPath) as! CollectionMealCell
        cell.lblMealName?.text = arrMeal[indexPath.row]
        cell.indicatorView.backgroundColor = colors[indexPath.row]
           return cell;
       }
    
    func selectedDate(selected: String) {
        lblDate.text = selected
    }

}


