//
//  LogsVC.swift
//  FitKout
//
//  Created by HT-Admin on 31/10/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class LogsVC: UIViewController {
    
    var i = 10

    @IBOutlet weak var lblWaterLevel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func btnRemoveClicked(_ sender: UIButton) {
       
        if i < 1 {
            
        }else{
            i = i - 1
        }
        
        lblWaterLevel.text = "\(i)"
    }
    
    @IBAction func btnAddClicked(_ sender: UIButton) {
         i = i + 1
        lblWaterLevel.text = "\(i)"
    }
    
}
