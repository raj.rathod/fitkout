//
//  AddWaterVC.swift
//  FitKout
//
//  Created by HT-Admin on 29/10/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class AddWaterVC: UIViewController ,DatePickerVCDelegate , UIPopoverPresentationControllerDelegate {

    @IBOutlet weak var viewStatus: UIView!
    
    @IBOutlet weak var cntntView: UIView!
    
    @IBOutlet weak var dateSelectorStack: UIStackView!
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var parentView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        viewStatus.layer.borderWidth = 5
        viewStatus.layer.borderColor = #colorLiteral(red: 0.4620226622, green: 0.8382837176, blue: 1, alpha: 1).cgColor
        
        cntntView.clipsToBounds = true
        cntntView.layer.cornerRadius = 40
        cntntView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        setGradientBackgroundReverse(view:self.parentView)
        
        
        let controller = SimpleViewControllerOne()
        controller.view.frame = CGRect(x: 0, y:30, width: self.cntntView.bounds.size.width, height: self.cntntView.bounds.size.height )
        self.cntntView.addSubview(controller.view)
        self.addChild(controller)
        
        let fGuesture = UITapGestureRecognizer(target: self, action: #selector(self.showDropDownForAW(sender:)))
        dateSelectorStack.addGestureRecognizer(fGuesture)
        dateSelectorStack.isUserInteractionEnabled = true
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
           return UIModalPresentationStyle.none
       }
    
    @objc func showDropDownForAW(sender: AnyObject){
          
                     let controller = DatePickerVC()
                   //  controller.datePickerTitle = "Select Date"

                     controller.delegate = self
                     controller.preferredContentSize = CGSize(width: view.bounds.size.width,height: 374)

                     controller.modalPresentationStyle = UIModalPresentationStyle.popover
                     let popController = controller.popoverPresentationController
                     popController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
                     popController?.delegate = self
                     popController?.sourceRect = CGRect(x: self.view.bounds.midX , y:self.view.bounds.midY , width: 0, height: 0 )
                     popController?.sourceView =  view
                     self.present(controller, animated: true, completion: nil)
          
      }
    func selectedDate(selected: String) {
        lblDate.text = selected
    }
    
    

    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
