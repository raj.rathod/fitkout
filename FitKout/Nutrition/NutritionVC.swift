//
//  NutritionVC.swift
//  FitKout
//
//  Created by HT-Admin on 28/10/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class NutritionVC: UIViewController ,UITableViewDelegate,UITableViewDataSource ,DatePickerVCDelegate , UIPopoverPresentationControllerDelegate{
 
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var navDevNutView: UIView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var navBarView: UIView!
    @IBOutlet weak var datePickerStackView: UIStackView!
    @IBOutlet weak var viewDisplay: UIView!
    @IBOutlet weak var tableViewMeal: UITableView!
    
   
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.backgroundColor = .white
        navDevNutView.addShadowToUIViewWithBackGroundColor(color: UIColor.black, cornerRadius: 1 , backGroundClr : #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))

        
        setGradientBackground(view:self.viewDisplay)
      //  navBarView.addShadowToUIView(cornerRadius: 0)
        
        tableViewMeal.register(UINib(nibName: "NutritionCell", bundle: nil), forCellReuseIdentifier: "NutritionCell")
        tableViewMeal.tableFooterView = UIView()
        tableViewMeal.separatorStyle = .none
        tableViewMeal.reloadData()
        
        let fGuesture = UITapGestureRecognizer(target: self, action: #selector(self.showDropDown(sender:)))
        datePickerStackView.addGestureRecognizer(fGuesture)
        datePickerStackView.isUserInteractionEnabled = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.catchUpdateMealNotif), name: NSNotification.Name(rawValue: "updateMealNotif"), object: nil)
        
  
    }
    

    
    @objc func catchUpdateMealNotif(_ userInfo: Notification){
              DispatchQueue.main.async {
                self.tableViewMeal.reloadData()
              }
       }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    @objc func showDropDown(sender: AnyObject){
        
                   let controller = DatePickerVC()
                   controller.delegate = self
                   controller.preferredContentSize = CGSize(width: view.bounds.size.width,height: 374)
                   controller.modalPresentationStyle = UIModalPresentationStyle.popover
                   let popController = controller.popoverPresentationController
                   popController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
                   popController?.delegate = self
                   popController?.sourceRect = CGRect(x: self.view.bounds.midX , y:self.view.bounds.midY , width: 0, height: 0 )
                   popController?.sourceView =  view
                   self.present(controller, animated: true, completion: nil)
        
    }
    
    
    func selectedDate(selected: String) {
        lblDate.text = selected
    }
    
    @IBAction func btnAddNewMealClicked(_ sender: UIButton) {
        
        showInputDialog(title: "Add A Meal",
                        subtitle: "Please enter the meal below.",
                        actionTitle: "DONE",
                        cancelTitle: "CLOSE",
                        inputPlaceholder: "Your Meal Name",
                        inputKeyboardType: .default)
        { (input:String?) in
            print("The new meal is \(input ?? "")")
        }
    }
    
    @IBAction func btnAnalysisClicked(_ sender: UIButton) {
             let analysisVC = AnalysisVC.init(nibName: "AnalysisVC", bundle: nil)
             analysisVC.modalPresentationStyle = .fullScreen
             self.present(analysisVC, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return 6
       }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let cell = tableView.dequeueReusableCell(withIdentifier: "NutritionCell", for: indexPath) as! NutritionCell
        
        cell.btnAdd.tag = indexPath.row
        cell.btnAdd.addTarget(self, action:  #selector(addBtnPressed(sender:)), for: .touchUpInside)
        
//        cell.btnMore.tag = indexPath.row
//        cell.btnMore.addTarget(self, action:  #selector(moreBtnPressed(sender:)), for: .touchUpInside)
           
           return cell
           
       }
       

       
       func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
       }
    
    
    @objc func addBtnPressed(sender: UIButton) {
        print(sender.tag)
        let addMealVC = AddNewMealVC.init(nibName: "AddNewMealVC", bundle: nil)
        addMealVC.modalPresentationStyle = .fullScreen
        self.present(addMealVC, animated: true, completion: nil)
    }
    
    
    @objc func moreBtnPressed(sender: UIButton) {
        print(sender.tag)
        let controller = MoreVC()
        controller.preferredContentSize = CGSize(width: 80,height: 80)
        controller.modalPresentationStyle = UIModalPresentationStyle.popover
        let popController = controller.popoverPresentationController
        popController?.permittedArrowDirections = UIPopoverArrowDirection.down
        popController?.delegate = self
        let viewForSource = sender as UIView
        popController?.sourceView = viewForSource
        // the position of the popover where it's showed
        popController?.sourceRect = viewForSource.bounds
        
        self.present(controller, animated: true, completion: nil)
    }
       
    @IBAction func btnAddWaterClicked(_ sender: UIButton) {
        
        let addWaterVC = AddWaterVC.init(nibName: "AddWaterVC", bundle: nil)
        addWaterVC.modalPresentationStyle = .fullScreen
        self.present(addWaterVC, animated: true, completion: nil)
    }
   
    
    @IBAction func btnDashBoardClicked(_ sender: UIButton) {
        let goalDashboardVC = GoalDashboardVC.init(nibName: "GoalDashboardVC", bundle: nil)
        goalDashboardVC.modalPresentationStyle = .fullScreen
        self.present(goalDashboardVC, animated: true, completion: nil)
    }
    
    
    @IBAction func btnFootStepsClicked(_ sender: UIButton) {
        
                let footStepsVC = FootStepsVC.init(nibName: "FootStepsVC", bundle: nil)
                footStepsVC.modalPresentationStyle = .fullScreen
                self.present(footStepsVC, animated: true, completion: nil)
    }
    
    
}
