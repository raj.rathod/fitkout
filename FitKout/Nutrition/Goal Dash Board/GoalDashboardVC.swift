//
//  GoalDashboardVC.swift
//  FitKout
//
//  Created by HT-Admin on 31/10/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class GoalDashboardVC: UIViewController {

    @IBOutlet weak var cGDView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let controller = GDTabsVC()
        controller.view.frame = CGRect(x: 0, y: 0, width: self.cGDView.bounds.size.width, height: self.cGDView.bounds.size.height )
        self.cGDView.addSubview(controller.view)
        self.addChild(controller)
        
        
    }


   
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
