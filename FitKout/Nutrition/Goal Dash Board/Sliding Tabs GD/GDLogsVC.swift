//
//  GDLogsVC.swift
//  FitKout
//
//  Created by HT-Admin on 31/10/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class GDLogsVC: UIViewController {
    
    
    @IBOutlet weak var lblminWeight: UILabel!
    
    @IBOutlet weak var lblMaxWeight: UILabel!
    
    @IBOutlet weak var lblSliderVal: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }


  
    
    @IBAction func weightSliderChanged(_ sender: UISlider) {
        let currentValue = Double(sender.value).truncate(places: 3)
        lblSliderVal.text = "\(currentValue) Kg"
    }
    
    
    @IBAction func btnSetGoalClicked(_ sender: UIButton) {
        
    }
    
}
