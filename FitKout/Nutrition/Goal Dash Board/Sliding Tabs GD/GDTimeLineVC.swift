//
//  GDTimeLineVC.swift
//  FitKout
//
//  Created by HT-Admin on 31/10/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class GDTimeLineVC: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    
     let arrayTitles = ["Goal : Weight Gain","Goal : Weight Loss","Goal : Stay fit","Goal : Weight Gain","Goal : Weight Loss"]
    let arrayStart = ["StartDate :2019-10-22","StartDate :2019-10-22","StartDate :2019-10-22","StartDate :2019-10-22","StartDate :2019-10-22","StartDate :2019-10-22"]
     let arrayEnd = ["StartDate :2019-11-22","StartDate :2019-11-22","StartDate :2019-11-22","StartDate :2019-11-22","StartDate :2019-11-22","StartDate :2019-11-22"]
    
    @IBOutlet weak var gdTableTimeLine: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
               gdTableTimeLine.register(UINib(nibName: "GDTimelineCell", bundle: nil), forCellReuseIdentifier: "GDTimelineCell")
               gdTableTimeLine.tableFooterView = UIView()
               gdTableTimeLine.separatorStyle = .none
               gdTableTimeLine.reloadData()
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayTitles.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "GDTimelineCell", for: indexPath) as! GDTimelineCell
         
            cell.btnLevelStatus.layer.borderWidth = 2.0
            cell.btnLevelStatus.layer.borderColor = #colorLiteral(red: 0, green: 0.1529411765, blue: 0.5058823529, alpha: 1)
            
            cell.btnLevelStatus.setTitle(arrayTitles[indexPath.row], for: .normal)
            
            cell.lblStartDate.text = arrayStart[indexPath.row]
            cell.lblEndDate.text = arrayEnd[indexPath.row]

            return cell
            
        }
        
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 110
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        }

}
