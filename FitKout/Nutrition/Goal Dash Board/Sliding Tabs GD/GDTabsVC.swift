//
//  CircularColorPickerVC.swift
//  Spintly
//
//  Created by HT-Admin on 29/08/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit


class GDTabsVC: UIViewController {

    // init slidingTabController to add to main view later
    // or you can just extends the UISimpleSlidingController
    // see in SimpleViewControllerTwo.swift
    
    private let slidingTabController = UISimpleSlidingTabController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
            setupUI()
    }
    
    private func setupUI(){
        
        let arrayTitles = ["TIMELINE","LOGS"]
        // view
        view.backgroundColor = .white
        view.addSubview(slidingTabController.view) // add slidingTabController to main view
        
        // navigation
        navigationItem.title = "Sliding Tab Example"
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController?.navigationBar.barStyle = .black
        
        
        
        for str in arrayTitles {
            
            if ( str == "TIMELINE" ){
                            slidingTabController.addItem(item: GDTimeLineVC(), title: str)
                        }
                        else
                        {
                            slidingTabController.addItem(item: GDLogsVC(), title: str)
                        }
            
        }
        
        // MARK: slidingTabController
      /*  slidingTabController.addItem(item: SimpleItemViewControllerOne(), title: "First") // add first item
        slidingTabController.addItem(item: SimpleItemViewControllerTwo(), title: "Second") // add second item
        slidingTabController.addItem(item: SimpleItemViewControllerThree(), title: "Third") // add other item
        slidingTabController.addItem(item: SimpleItemViewControllerThree(), title: "Forth") // add other item
        slidingTabController.addItem(item: SimpleItemViewControllerThree(), title: "Fifth") // add other item
        slidingTabController.addItem(item: SimpleItemViewControllerThree(), title: "Sixth") // add other item
        slidingTabController.addItem(item: SimpleItemViewControllerThree(), title: "Seventh") // add other item
        */
        
        slidingTabController.setHeaderActiveColor(color: .black) // default blue
        slidingTabController.setHeaderInActiveColor(color: .black) // default gray
        slidingTabController.setHeaderBackgroundColor(color: .white) // default white
        slidingTabController.setCurrentPosition(position: 0) // default 0
        slidingTabController.setStyle(style: .fixed) // default fixed
        slidingTabController.build() // build
    }
    
 
    
    
    
    
}

