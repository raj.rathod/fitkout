//
//  AddNewMealVC.swift
//  FitKout
//
//  Created by HT-Admin on 29/10/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class AddNewMealVC: UIViewController ,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate{
    
    var arrayMeal = [String]()
    var filtered:[String] = []

    
    var searchActive = false
    var selectedArray:[String] = []
    var selectedRows:[IndexPath] = []
    
    @IBOutlet weak var searchAddMeal: UISearchBar!
    @IBOutlet weak var tableAddMeal: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        searchAddMeal.delegate = self
        
        tableAddMeal.register(UINib(nibName: "AddMealCell", bundle: nil), forCellReuseIdentifier: "AddMealCell")
        tableAddMeal.tableFooterView = UIView()
        tableAddMeal.separatorStyle = .none
        tableAddMeal.reloadData()
        
        arrayMeal = ["Brown Rice","Wheat" ,"Brown Cread","Veg Juice","Raw Veg","Proteins","Eggs"]
        
       addMeal()
    }


    @IBAction func btnDoneClicked(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(searchActive) {
            return filtered.count
        }else{
            return arrayMeal.count
            }
        
          }
          
          func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
              let cell = tableView.dequeueReusableCell(withIdentifier: "AddMealCell", for: indexPath) as! AddMealCell
            
            if(searchActive){
                cell.lblMeal.text = filtered[indexPath.row]
            } else {
                cell.lblMeal.text = arrayMeal[indexPath.row]
            }
            cell.lblQty.text = "100 gm"
            cell.btnAddMeal.tag = indexPath.row
            cell.btnAddMeal.addTarget(self, action:  #selector(addMealBtnPressed(sender:)), for: .touchUpInside)
             
            if selectedRows.contains(indexPath)
            {
               cell.btnAddMeal.setImage(#imageLiteral(resourceName: "right_tick"), for: .normal)
            }
            else
            {
                cell.btnAddMeal.setImage(#imageLiteral(resourceName: "add_meal"), for: .normal)
            }
              return cell
              
          }
          
          
          func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
              return 80
          }
          
          func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            self.searchAddMeal.resignFirstResponder()
            
          }
       
       
       @objc func addMealBtnPressed(sender: UIButton) {
         self.searchAddMeal.resignFirstResponder()
           print(sender.tag)
        let selectedIndexPath = IndexPath(row: sender.tag, section: 0)
              
              if self.selectedRows.contains(selectedIndexPath)
              {
                  self.selectedRows.remove(at: self.selectedRows.index(of: selectedIndexPath)!)
                
                if(searchActive){
                    selectedArray = selectedArray.filter(){$0 != filtered[sender.tag]}
                } else {
        selectedArray = selectedArray.filter(){$0 != arrayMeal[sender.tag]}

                }
                  print(selectedArray)

              }
              else
              {
                  self.selectedRows.append(selectedIndexPath)
                
                if(searchActive){
                    selectedArray.append(filtered[sender.tag])
                 }
                else
                {
                selectedArray.append(arrayMeal[sender.tag])

                 }
                  print(selectedArray)
              }
              self.tableAddMeal.reloadData()
           
       }
    
    
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.text = nil
        searchBar.resignFirstResponder()
        tableAddMeal.resignFirstResponder()
        self.searchAddMeal.showsCancelButton = false
        DispatchQueue.main.async {
          self.tableAddMeal.reloadData()
        }
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.resignFirstResponder()
    }


    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        filtered = arrayMeal.filter({ (text) -> Bool in
            let tmp:NSString = text as NSString
            let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
            return range.location != NSNotFound
        })

        print(filtered)
        if (filtered.count == 0){
            searchActive = false
        }
        else{
            searchActive = true
        }
        
       DispatchQueue.main.async {
         self.tableAddMeal.reloadData()
       }
     
    }
    
    
    
    
    
    func addMeal() -> Void {
              let headers = [
                              "Authorization": "Bearer fitkout",
                              "Content-Type": "application/json",
                              "cache-control": "no-cache",
                            ]
                
                let params =
                    [[
                        "id" :"",
                        "Date":"2019-11-26",
                        "Userid":"1238",
                        "MealId":"1",
                        "FoodId":"535",
                        "Qty":"76800",
                        "UnitTypeId":"1",
                        "ExtraMeal":"0"]]
                
                var request = URLRequest(url: URL(string: "https://fitkout.com.cs-mum-27.bluehostwebservers.com/Fitkout/index.php/api/NutritionApi/food_perday_insert")!)
                request.httpMethod = "POST"
                request.allHTTPHeaderFields = headers
                request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])

                let session = URLSession.shared
                let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
                 
                    print(response as Any)
                 
                        do {
                        let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                       print(json)
                             }
                        catch {
                               print("error")
                              }
                })

                task.resume()
    }
    
    
}
