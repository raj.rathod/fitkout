//
//  FootStepsVC.swift
//  FitKout
//
//  Created by HT-Admin on 01/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class FootStepsVC: UIViewController ,UITableViewDelegate,UITableViewDataSource , UICollectionViewDataSource,UICollectionViewDelegate ,DatePickerVCDelegate , UIPopoverPresentationControllerDelegate{
    
    
    
    var workOutArray = [ "Ab Circuit" , "Fat Loss" , "Muscle Gain"]
    
    var categoriesArray = [ "Aerobic" , "Anaerobic" , "Chest","Back" , "Yoga" , "Shoulder"]
    var programesArray = [ "12 weeks muscle" , "Fat loss" , "Muscle gain","Strength training" , "Shredded" ]

    @IBOutlet weak var lblDateFS: UILabel!
    
    @IBOutlet weak var tableWkHistory: UITableView!
    @IBOutlet weak var tableWSet: UITableView!
    
    @IBOutlet weak var datePickerStackFS: UIStackView!
    
    @IBOutlet weak var collectionViewCategories: UICollectionView!
    
    @IBOutlet weak var collectionPrograms: UICollectionView!
    
    @IBOutlet weak var circularProgress: CircularProgressBig!
  
    
    @IBOutlet weak var setView: UIView!
    @IBOutlet weak var whView: UIView!
    @IBOutlet weak var progressCntView: UIView!
    @IBOutlet weak var wcpView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableWkHistory.register(UINib(nibName: "FsTableCell", bundle: nil), forCellReuseIdentifier: "FsTableCell")
        tableWkHistory.tableFooterView = UIView()
        tableWkHistory.separatorStyle = .none
        tableWkHistory.reloadData()
        
        tableWSet.register(UINib(nibName: "FSWorkoutCell", bundle: nil), forCellReuseIdentifier: "FSWorkoutCell")
        tableWSet.tableFooterView = UIView()
        tableWSet.separatorStyle = .none
        tableWSet.reloadData()
        
        collectionViewCategories.register(UINib(nibName: "CategoriesCell", bundle: nil), forCellWithReuseIdentifier: "CategoriesCell")
        collectionPrograms.register(UINib(nibName: "CategoriesCell", bundle: nil), forCellWithReuseIdentifier: "CategoriesCell")

        circularProgress.trackClr = #colorLiteral(red: 0.9215686275, green: 0.9215686275, blue: 0.9215686275, alpha: 1)
        circularProgress.progressClr = #colorLiteral(red: 0.9803921569, green: 0.7176470588, blue: 0.3882352941, alpha: 1)
        circularProgress.setProgressWithAnimation(duration: 1.0, value: 0.53)
        
        let fsGuesture = UITapGestureRecognizer(target: self, action: #selector(self.showDropDownForFS(sender:)))
        datePickerStackFS.addGestureRecognizer(fsGuesture)
        datePickerStackFS.isUserInteractionEnabled = true
        
        setView.addShadowToUIView(cornerRadius: 20)
        setView.addShadowToUIView(color: UIColor.lightGray, cornerRadius: 20)
        
        whView.addShadowToUIView(color: UIColor.lightGray, cornerRadius: 20)
        progressCntView.addShadowToUIView(color: UIColor.lightGray, cornerRadius: 20)
        wcpView.addShadowToUIView(color: UIColor.lightGray, cornerRadius: 20)



    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
           return UIModalPresentationStyle.none
       }
    
    @objc func showDropDownForFS(sender: AnyObject){
          
         let controller = DatePickerVC()
         controller.delegate = self
         controller.preferredContentSize = CGSize(width: view.bounds.size.width,height: 374)
         controller.modalPresentationStyle = UIModalPresentationStyle.popover
         let popController = controller.popoverPresentationController
         popController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
         popController?.delegate = self
         popController?.sourceRect = CGRect(x: self.view.bounds.midX , y:self.view.bounds.midY , width: 0, height: 0 )
         popController?.sourceView =  view
                     self.present(controller, animated: true, completion: nil)
          
      }
    
    func selectedDate(selected: String) {
        lblDateFS.text = selected
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableWkHistory {
            return 2
        }
        else{
            return 3
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tableWkHistory {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FsTableCell", for: indexPath) as! FsTableCell
             
            
             
             cell.btnFsMore.tag = indexPath.row
             cell.btnFsMore.addTarget(self, action:  #selector(moreBtnPressed(sender:)), for: .touchUpInside)
            
            return cell
        }
        else{
             let wCell = tableView.dequeueReusableCell(withIdentifier: "FSWorkoutCell", for: indexPath) as! FSWorkoutCell
            
            wCell.lblWname.text = workOutArray[indexPath.row]
            return wCell
        }
             
             
             
             
         }
         
         
         func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            
            if tableView == tableWkHistory {
                       return 80
                   }
                   else{
                       return 200
                   }
         }
         
         func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
         }
    
    @objc func moreBtnPressed(sender: UIButton) {
        print(sender.tag)
      
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
              return 1
          }

          func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
           if collectionView == collectionViewCategories
           {
            return categoriesArray.count
           }
           else
           {
            return programesArray.count
            }
          }

       
       
          func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
              let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoriesCell", for: indexPath) as! CategoriesCell
            
            if collectionView == collectionViewCategories {
                cell.lblPname.text = categoriesArray[indexPath.item]
                cell.imgProgram.image = #imageLiteral(resourceName: "temp_categories")
                cell.imgProgram.contentMode = .scaleAspectFill
                 return cell;
            }else{
                cell.lblPname.text = programesArray[indexPath.item]
                cell.imgProgram.image = #imageLiteral(resourceName: "temp_categories")
                cell.imgProgram.contentMode = .scaleAspectFill
                return cell;
            }
          
             
          }
    
    
    
    
    @IBAction func btnAddPhysciallClicked(_ sender: UIButton) {
        let physicalActivityVC = PhysicalActivityVC.init(nibName: "PhysicalActivityVC", bundle: nil)
        physicalActivityVC.modalPresentationStyle = .fullScreen
        self.present(physicalActivityVC, animated: true, completion: nil)
    }
    
    
    
    @IBAction func btnBckClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnCatAllClicked(_ sender: UIButton) {
        let addMealVC = FSAllCategoriesVC.init(nibName: "FSAllCategoriesVC", bundle: nil)
        addMealVC.modalPresentationStyle = .fullScreen
        self.present(addMealVC, animated: true, completion: nil)
    }
    
    
    @IBAction func btnProgAllClicked(_ sender: UIButton) {
        
        let addMealVC = FSAllProgrammesVC.init(nibName: "FSAllProgrammesVC", bundle: nil)
        addMealVC.modalPresentationStyle = .fullScreen
        self.present(addMealVC, animated: true, completion: nil)
    }
    
    
    @IBAction func btnMiniSetClicked(_ sender: UIButton) {
    }
    
    
    @IBAction func btnBeginerClicked(_ sender: UIButton) {
        let workoutVC = WorkoutVC.init(nibName: "WorkoutVC", bundle: nil)
        workoutVC.modalPresentationStyle = .fullScreen
        self.present(workoutVC, animated: true, completion: nil)
    }
    
    @IBAction func btnIntermediateClicked(_ sender: UIButton) {
        let workoutVC = WorkoutVC.init(nibName: "WorkoutVC", bundle: nil)
        workoutVC.modalPresentationStyle = .fullScreen
        self.present(workoutVC, animated: true, completion: nil)
    }
    
    @IBAction func btnAdvanceClicked(_ sender: UIButton) {
        let workoutVC = WorkoutVC.init(nibName: "WorkoutVC", bundle: nil)
        workoutVC.modalPresentationStyle = .fullScreen
        self.present(workoutVC, animated: true, completion: nil)
    }
}
