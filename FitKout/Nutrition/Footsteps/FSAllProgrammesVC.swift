//
//  FSAllProgrammesVC.swift
//  FitKout
//
//  Created by HT-Admin on 01/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class FSAllProgrammesVC: UIViewController , UICollectionViewDataSource,UICollectionViewDelegate  {

    @IBOutlet weak var collectionPrograms: UICollectionView!
    
     var programesArray = [ "12 weeks muscle" , "Fat loss" , "Muscle gain","Strength training" , "Shredded"]

     override func viewDidLoad() {
         super.viewDidLoad()

         // Do any additional setup after loading the view.
         
         collectionPrograms.register(UINib(nibName: "CategoriesAllFSCell", bundle: nil), forCellWithReuseIdentifier: "CategoriesAllFSCell")
     }


    func numberOfSections(in collectionView: UICollectionView) -> Int {
                 return 1
             }

             func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
             
               return programesArray.count
             
             }

          
          
             func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
                 let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoriesAllFSCell", for: indexPath) as! CategoriesAllFSCell
               
                   cell.lblProgram.text = programesArray[indexPath.item]
                  // cell.imgProgram.image = #imageLiteral(resourceName: "workout_image")
                    return cell;
             
             }
    
    
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
