//
//  FSAllCategoriesVC.swift
//  FitKout
//
//  Created by HT-Admin on 01/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class FSAllCategoriesVC: UIViewController , UICollectionViewDataSource,UICollectionViewDelegate {
    
    
    var workoutArray = [WorkoutCategory]()


    @IBOutlet weak var collectionViewCategories: UICollectionView!
    
    var categoriesArray = [ "Aerobic" , "Anaerobic" , "Chest","Back" , "Yoga" , "Shoulder","Biceps","Triceps"]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        getAllWorkouts()

        
        collectionViewCategories.register(UINib(nibName: "CategoriesAllFSCell", bundle: nil), forCellWithReuseIdentifier: "CategoriesAllFSCell")
    }


   func numberOfSections(in collectionView: UICollectionView) -> Int {
                return 1
            }

            func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            
              return categoriesArray.count
            
            }

         
         
            func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoriesAllFSCell", for: indexPath) as! CategoriesAllFSCell
//                let mcModel: WorkoutCategory
//                mcModel = workoutArray[indexPath.row]
//                cell.lblProgram.text = mcModel.exercise_info_title
           //     cell.imgProgram.loadImageUsingCache(withUrl: mcModel.thumbnail)
                
                  cell.lblProgram.text = categoriesArray[indexPath.item]
                  cell.imgProgram.image = #imageLiteral(resourceName: "temp_categories")
                   return cell;
            
            }
    
    func getAllWorkouts() -> Void {
        getDataFromUrl(url: "https://fitkout.com.cs-mum-27.bluehostwebservers.com/Fitkout/index.php/api/ExerciseApi/getWorkoutCategory") { (response) in
            print(response)
                      var wArray = [WorkoutCategory]()

                      for anItem in response as! [Dictionary<String, Any>] {
                          
                      if let netWorkObj = WorkoutCategory(dictionary: anItem as [String : Any]) {
                              
                              print(netWorkObj)
                      wArray.append(netWorkObj)
                       }
                       self.workoutArray = wArray
                       DispatchQueue.main.async {
                            self.collectionViewCategories.reloadData()
                             }
                      }
        }
    }

    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
         self.dismiss(animated: true, completion: nil)
    }
}
