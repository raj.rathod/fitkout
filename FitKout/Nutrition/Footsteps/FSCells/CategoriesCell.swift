//
//  CategoriesCell.swift
//  FitKout
//
//  Created by HT-Admin on 01/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class CategoriesCell: UICollectionViewCell {
    
    @IBOutlet weak var imgProgram: UIImageView!
    
    @IBOutlet weak var lblPname: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        // reset custom properties to default values
        self.lblPname.text = ""
        self.imgProgram.image = nil
      
    }
    
    
}
