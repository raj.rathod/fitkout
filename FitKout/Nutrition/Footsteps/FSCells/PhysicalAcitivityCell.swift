//
//  PhysicalAcitivityCell.swift
//  FitKout
//
//  Created by HT-Admin on 06/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class PhysicalAcitivityCell: UITableViewCell {
    
    
    @IBOutlet weak var lblActName: UILabel!
    
    @IBOutlet weak var btnActAdd: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
