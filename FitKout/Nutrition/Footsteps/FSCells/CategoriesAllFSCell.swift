//
//  CategoriesAllFSCell.swift
//  FitKout
//
//  Created by HT-Admin on 01/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class CategoriesAllFSCell: UICollectionViewCell {
    
    @IBOutlet weak var imgProgram: UIImageView!
    @IBOutlet weak var lblProgram: UILabel!
    
    @IBOutlet weak var cntViewForFSP: UIView!
    
    @IBInspectable var cornerRadius: CGFloat = 10
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 3
          @IBInspectable var shadowColor: UIColor? = UIColor.darkGray
          @IBInspectable var shadowOpacity: Float = 0.6
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        // reset custom properties to default values
        self.imgProgram.image = nil //and etc.
        self.lblProgram.text = ""
    }
    
  
    override func layoutSubviews() {

           cntViewForFSP.layer.cornerRadius = cornerRadius
           cntViewForFSP.layer.masksToBounds = false
           cntViewForFSP.layer.shadowColor = shadowColor?.cgColor
           cntViewForFSP.layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
           cntViewForFSP.layer.shadowOpacity = shadowOpacity
           cntViewForFSP.layer.borderWidth = 0
           cntViewForFSP.layer.borderColor = UIColor(red:169, green:169, blue:169, alpha:1.0).cgColor

       }
}
