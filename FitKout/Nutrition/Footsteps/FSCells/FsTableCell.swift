//
//  FsTableCell.swift
//  FitKout
//
//  Created by HT-Admin on 01/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class FsTableCell: UITableViewCell {

    
    @IBOutlet weak var lblNameofExercise: UILabel!
    
    @IBOutlet weak var lblFsCal: UILabel!
    
    @IBOutlet weak var btnFsMore: UIButton!
    
    @IBOutlet weak var lblFsTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
