//
//  FSWorkoutCell.swift
//  FitKout
//
//  Created by HT-Admin on 01/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class FSWorkoutCell: UITableViewCell {
    
    @IBOutlet weak var imgWorkout: UIImageView!
    @IBOutlet weak var lblWname: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
