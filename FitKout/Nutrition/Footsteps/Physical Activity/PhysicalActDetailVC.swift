//
//  PhysicalActDetailVC.swift
//  FitKout
//
//  Created by HT-Admin on 06/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class PhysicalActDetailVC: UIViewController {
    
    var actName = ""
    
    @IBOutlet weak var navBarTitle: UINavigationItem!
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var imgAct: UIImageView!
    @IBOutlet weak var tfSpeed: UITextField!
    @IBOutlet weak var tfDuration: UITextField!
    @IBOutlet weak var tfSets: UITextField!
    @IBOutlet weak var tfCalBurnt: UITextField!
    
    @IBOutlet weak var btnReps: UIButton!
    @IBOutlet weak var btnMins: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        hideKeyboardWhenTappedAround()
        navBarTitle.title = actName
        
        btnMins.layer.borderWidth = 1.0
        btnMins.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        btnReps.layer.borderWidth = 1.0
        btnReps.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        tfSets.layer.borderWidth = 1.0
        tfSets.layer.borderColor = #colorLiteral(red: 0, green: 0.8126131296, blue: 0.8772806525, alpha: 1)
        tfSpeed.layer.borderWidth = 1.0
        tfSpeed.layer.borderColor = #colorLiteral(red: 0, green: 0.8126131296, blue: 0.8772806525, alpha: 1)
        tfDuration.layer.borderWidth = 1.0
        tfDuration.layer.borderColor = #colorLiteral(red: 0, green: 0.8126131296, blue: 0.8772806525, alpha: 1)
        tfCalBurnt.layer.borderWidth = 1.0
        tfCalBurnt.layer.borderColor = #colorLiteral(red: 0, green: 0.8126131296, blue: 0.8772806525, alpha: 1)
        
        tfSets.setLeftPaddingPoints(10)
        tfSpeed.setLeftPaddingPoints(10)
        tfDuration.setLeftPaddingPoints(10)
        tfCalBurnt.setLeftPaddingPoints(10)
    }

    
    @IBAction func btnDurationClicked(_ sender: UIButton) {
    }
    
    
    @IBAction func btnRepsClicked(_ sender: UIButton) {
    }
    
    @IBAction func btnTrackClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)

    }
    
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
