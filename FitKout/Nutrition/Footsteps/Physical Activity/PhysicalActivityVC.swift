//
//  PhysicalActivityVC.swift
//  FitKout
//
//  Created by HT-Admin on 05/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class PhysicalActivityVC: UIViewController ,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate{
    
    @IBOutlet weak var searchActivity: UISearchBar!
    
    @IBOutlet weak var tableViewActivity: UITableView!
    
    var arrayMeal = [String]()
    var filtered:[String] = []
       
    var searchActive = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       // self.searchActivity.setCustomBackgroundColor (color: #colorLiteral(red: 0, green: 0.8126131296, blue: 0.8772806525, alpha: 1))

        searchActivity.delegate = self
        
        tableViewActivity.register(UINib(nibName: "PhysicalAcitivityCell", bundle: nil), forCellReuseIdentifier: "PhysicalAcitivityCell")
        tableViewActivity.tableFooterView = UIView()
        tableViewActivity.separatorStyle = .none
        tableViewActivity.reloadData()
        
        arrayMeal = [ "Incline Inner Biceps Curl" , "Incline Dumbbell Hammer Curl" , "Leg Press","Leg Extension","Chest press","Running" ,"Jumping","Walking","Running"]
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(searchActive) {
            return filtered.count
        }else{
            return arrayMeal.count
            }
        
          }
          
          func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
              let cell = tableView.dequeueReusableCell(withIdentifier: "PhysicalAcitivityCell", for: indexPath) as! PhysicalAcitivityCell
            
            if(searchActive){
                cell.lblActName.text = filtered[indexPath.row]
            } else {
                cell.lblActName.text = arrayMeal[indexPath.row]

            }
            cell.btnActAdd.tag = indexPath.row
            cell.btnActAdd.addTarget(self, action:  #selector(addMealBtnPressed(sender:)), for: .touchUpInside)
              
              return cell
              
          }
          
          
          func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
              return 40
          }
          
          func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            self.searchActivity.resignFirstResponder()
            let physicalActDetailVC = PhysicalActDetailVC.init(nibName: "PhysicalActDetailVC", bundle: nil)
            physicalActDetailVC.modalPresentationStyle = .fullScreen
            if(searchActive){
                physicalActDetailVC.actName  = filtered[indexPath.row]
            } else {
                physicalActDetailVC.actName = arrayMeal[indexPath.row]
            }
            self.present(physicalActDetailVC, animated: true, completion: nil)
            
          }
       
       
       @objc func addMealBtnPressed(sender: UIButton) {
         self.searchActivity.resignFirstResponder()
           print(sender.tag)
           
       }
    
    
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.text = nil
        searchBar.resignFirstResponder()
        tableViewActivity.resignFirstResponder()
        self.searchActivity.showsCancelButton = false
        DispatchQueue.main.async {
          self.tableViewActivity.reloadData()
        }
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.resignFirstResponder()
    }


    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        filtered = arrayMeal.filter({ (text) -> Bool in
            let tmp:NSString = text as NSString
            let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
            return range.location != NSNotFound
        })

        print(filtered)
        if (filtered.count == 0){
            searchActive = false
        }
        else{
            searchActive = true
        }
        
       DispatchQueue.main.async {
         self.tableViewActivity.reloadData()
       }
     
    }
    
    
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    

}
