//
//  MoreVC.swift
//  FitKout
//
//  Created by HT-Admin on 29/10/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class MoreVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(self.catchUpdateMealNotif), name: NSNotification.Name(rawValue: "updateMealNotif"), object: nil)
    }
    
    @objc func catchUpdateMealNotif(_ userInfo: Notification){
           DispatchQueue.main.async {
           self.dismiss(animated: true, completion: nil)
           }
    }
    
    @IBAction func btnEditClicked(_ sender: UIButton) {
        let foodUpdateVC = FoodUpdateVC.init(nibName: "FoodUpdateVC", bundle: nil)
         foodUpdateVC.modalPresentationStyle = .fullScreen
        self.present(foodUpdateVC, animated: true, completion: nil)
    }
    
    @IBAction func btnDeleteClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
