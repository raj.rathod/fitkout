//
//  ProfileVC.swift
//  FitKout
//
//  Created by HT-Admin on 28/10/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {

    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.backgroundColor = .white

    }
   
    
    @IBAction func btnProfileClicked(_ sender: UIButton) {
        let myProfileVC = MyProfileVC.init(nibName: "MyProfileVC", bundle: nil)
        myProfileVC.modalPresentationStyle = .fullScreen
   self.present(myProfileVC, animated: true, completion: nil)
    }
    
    @IBAction func btnLogoutClicked(_ sender: UIButton) {
         let viewController:ViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewController") as! ViewController
        viewController.modalPresentationStyle = .fullScreen
       self.present(viewController, animated: false, completion: nil)
    }
    
   
    @IBAction func btnMyPlansClicked(_ sender: UIButton) {
        let myPlansVC = MyPlansVC.init(nibName: "MyPlansVC", bundle: nil)
        myPlansVC.modalPresentationStyle = .fullScreen
        self.present(myPlansVC, animated: true, completion: nil)
    }
    
    @IBAction func btnGoalClicked(_ sender: UIButton) {
        let goalDashboardVC = GoalDashboardVC.init(nibName: "GoalDashboardVC", bundle: nil)
        goalDashboardVC.modalPresentationStyle = .fullScreen
        self.present(goalDashboardVC, animated: true, completion: nil)
    }
}
