//
//  FavFoodCVCell.swift
//  FitKout
//
//  Created by HT-Admin on 25/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class FavFoodCVCell: UICollectionViewCell {
    
    @IBOutlet weak var imgFoof: UIImageView!
    @IBOutlet weak var lblFood: UILabel!
    @IBOutlet weak var lblFoodDesc: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
