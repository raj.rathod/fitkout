//
//  AddGymCVCell.swift
//  FitKout
//
//  Created by HT-Admin on 22/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class AddGymCVCell: UICollectionViewCell {
    
    
    @IBOutlet weak var cntGymDetailView: UIView!
    
    @IBInspectable var cornerRadius: CGFloat = 10
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 3
    @IBInspectable var shadowColor: UIColor? = UIColor.darkGray
    @IBInspectable var shadowOpacity: Float = 0.6
    
    @IBOutlet weak var lblgymName: UILabel!
    @IBOutlet weak var imgAddGym: UIImageView!
    @IBOutlet weak var btnAdd: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {

               cntGymDetailView.layer.cornerRadius = cornerRadius
               cntGymDetailView.layer.masksToBounds = false
               cntGymDetailView.layer.shadowColor = shadowColor?.cgColor
               cntGymDetailView.layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
               cntGymDetailView.layer.shadowOpacity = shadowOpacity
               cntGymDetailView.layer.borderWidth = 0
               cntGymDetailView.layer.borderColor = UIColor(red:169, green:169, blue:169, alpha:1.0).cgColor

           }

}
