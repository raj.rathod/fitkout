//
//  GymDetailsCell.swift
//  FitKout
//
//  Created by HT-Admin on 22/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class GymDetailsCell: UITableViewCell {
    
    
    @IBOutlet weak var imgGym: UIImageView!
    @IBOutlet weak var cntView: UIView!
    @IBOutlet weak var lblLocations: UILabel!
    @IBOutlet weak var lblGymName: UILabel!
    @IBOutlet weak var btnRemove: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
