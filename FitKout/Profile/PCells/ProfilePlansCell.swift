//
//  ProfilePlansCell.swift
//  FitKout
//
//  Created by HT-Admin on 27/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class ProfilePlansCell: UITableViewCell {
    
    @IBOutlet weak var imgPlan: UIImageView!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var btnDetails: UIButton!
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var viewCntnt: UIView!
    
    @IBInspectable var cornerRadius: CGFloat = 10
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 0
    @IBInspectable var shadowColor: UIColor? = UIColor.darkGray
    @IBInspectable var shadowOpacity: Float = 0.2
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {

        viewCntnt.layer.cornerRadius = cornerRadius
        viewCntnt.layer.masksToBounds = false
        viewCntnt.layer.shadowColor = shadowColor?.cgColor
        viewCntnt.layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        viewCntnt.layer.shadowOpacity = shadowOpacity
        viewCntnt.layer.borderWidth = 1.0
        viewCntnt.layer.borderColor = UIColor(red:169, green:169, blue:169, alpha:1.0).cgColor

    }
    
}
