//
//  ProfileChatsVC.swift
//  FitKout
//
//  Created by HT-Admin on 27/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class ProfileChatsVC: UIViewController,UIWebViewDelegate {
    
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
         webView.delegate = self
         let url = URL(string: "http://fitkout.com/")
         let requestObj = URLRequest(url: url! as URL)
         webView.loadRequest(requestObj)
        
    }

    func webViewDidStartLoad(_ webView: UIWebView) {
       self.view.activityStartAnimating(activityColor: #colorLiteral(red: 0.9321846366, green: 0.9321846366, blue: 0.9321846366, alpha: 1), backgroundColor: UIColor.black.withAlphaComponent(0.3))
       }
       
       
    func webViewDidFinishLoad(_ webView: UIWebView) {
           self.view.activityStopAnimating()
       }
       
    func webView(_ webView: UIWebView, didFailLoadWithError error: NSError?) {
           self.view.activityStopAnimating()
       }
       

    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    

}
