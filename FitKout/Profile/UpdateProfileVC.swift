//
//  UpdateProfileVC.swift
//  FitKout
//
//  Created by HT-Admin on 25/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class UpdateProfileVC: UIViewController ,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var tfUname: UITextField!
    @IBOutlet weak var tfHeight: UITextField!
    @IBOutlet weak var tfCity: UITextField!
    @IBOutlet weak var tfWeight: UITextField!
    @IBOutlet weak var tfName: UITextField!
    
    var imagePickerController = UIImagePickerController()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
      designUI()
      
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped(tapGestureRecognizer:)))
        imgProfile.isUserInteractionEnabled = true
        imgProfile.addGestureRecognizer(tapGestureRecognizer)
        
        imagePickerController.delegate = self
        self.imagePickerController.allowsEditing = true

        
    }
    
    
    // MARK: IMAGE PICKER METHODS
       @objc func viewTapped(tapGestureRecognizer: UITapGestureRecognizer)
       {
           print("pcikker tapped")
           let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
           alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
               self.openCamera()
           }))
           
           alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
               self.openGallary()
           }))
           
           alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
           
           /*If you want work actionsheet on ipad
            then you have to use popoverPresentationController to present the actionsheet,
            otherwise app will crash on iPad */
           switch UIDevice.current.userInterfaceIdiom {
           case .pad:
               alert.popoverPresentationController?.sourceView = view
               alert.popoverPresentationController?.sourceRect = view.bounds
               alert.popoverPresentationController?.permittedArrowDirections = .up
           default:
               break
           }
           
           self.present(alert, animated: true, completion: nil)
       }
       
       func openCamera()
       {
           if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
           {
               
               imagePickerController.sourceType = UIImagePickerController.SourceType.camera
               self.present(imagePickerController, animated: true, completion: nil)
               
           }
           else
           {
               let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
               alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
               self.present(alert, animated: true, completion: nil)
           }
       }
       
       func openGallary()
       {
           imagePickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
           self.present(imagePickerController, animated: true, completion: nil)
       }
       
       func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
           picker.dismiss(animated: true, completion: nil)
       }
       
       // adding image to imageview
       
       internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
           
        var image : UIImage!

        if let img = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
               {
                   image = img

               }
        else if let img = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
               {
                   image = img
               }

           imgProfile.image = image
           self.dismiss(animated: true, completion: nil)
       }
       // MARK: IMAGE PICKER METHODS END
    
    

    func designUI() -> Void {
        imgProfile.layer.borderWidth = 2
        imgProfile.layer.borderColor = #colorLiteral(red: 0.003921568627, green: 0.7764705882, blue: 0.8470588235, alpha: 1)
        tfUname.layer.borderWidth = 1
        tfUname.layer.borderColor = #colorLiteral(red: 0.003921568627, green: 0.7764705882, blue: 0.8470588235, alpha: 1)
        tfName.layer.borderWidth = 1
        tfName.layer.borderColor = #colorLiteral(red: 0.003921568627, green: 0.7764705882, blue: 0.8470588235, alpha: 1)
        tfCity.layer.borderWidth = 1
        tfCity.layer.borderColor = #colorLiteral(red: 0.003921568627, green: 0.7764705882, blue: 0.8470588235, alpha: 1)
        tfHeight.layer.borderWidth = 1
        tfHeight.layer.borderColor = #colorLiteral(red: 0.003921568627, green: 0.7764705882, blue: 0.8470588235, alpha: 1)
        tfWeight.layer.borderWidth = 1
        tfWeight.layer.borderColor = #colorLiteral(red: 0.003921568627, green: 0.7764705882, blue: 0.8470588235, alpha: 1)
    }
    
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
