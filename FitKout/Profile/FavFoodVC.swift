//
//  FavFoodVC.swift
//  FitKout
//
//  Created by HT-Admin on 25/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class FavFoodVC: UIViewController ,UICollectionViewDataSource,UICollectionViewDelegate {
    
    @IBOutlet weak var cvProteinFood: UICollectionView!
    @IBOutlet weak var cvFatsFood: UICollectionView!
    @IBOutlet weak var cvCarbsFood: UICollectionView!
    @IBOutlet weak var cvMixedFood: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        cvProteinFood.register(UINib(nibName: "FavFoodCVCell", bundle: nil), forCellWithReuseIdentifier: "FavFoodCVCell")
        cvFatsFood.register(UINib(nibName: "FavFoodCVCell", bundle: nil), forCellWithReuseIdentifier: "FavFoodCVCell")
        cvCarbsFood.register(UINib(nibName: "FavFoodCVCell", bundle: nil), forCellWithReuseIdentifier: "FavFoodCVCell")
        cvMixedFood.register(UINib(nibName: "FavFoodCVCell", bundle: nil), forCellWithReuseIdentifier: "FavFoodCVCell")
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
           if collectionView == cvProteinFood
           {
            return 5
           }
           else if collectionView == cvFatsFood
           {
            return 5
            }else if collectionView == cvCarbsFood
            {
             return 5
            }else
            {
              return 5
            }
        
          }

       
       
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == cvProteinFood
        {
               let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FavFoodCVCell", for: indexPath) as! FavFoodCVCell
               return cell;
            
        }else  if collectionView == cvFatsFood {
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FavFoodCVCell", for: indexPath) as! FavFoodCVCell
            return cell;
        } else  if collectionView == cvCarbsFood
        {
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FavFoodCVCell", for: indexPath) as! FavFoodCVCell
            return cell;

        }
        else
        {
           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FavFoodCVCell", for: indexPath) as! FavFoodCVCell
               return cell;

        }


          }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
                if collectionView == cvProteinFood
               {

               }else  if collectionView == cvFatsFood
               {

               } else  if collectionView == cvCarbsFood
               {

               }
               else
               {

               }

    }
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
