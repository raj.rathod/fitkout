//
//  MyPlansVC.swift
//  FitKout
//
//  Created by HT-Admin on 27/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class MyPlansVC: UIViewController ,DatePickerVCDelegate , UIPopoverPresentationControllerDelegate {
   
    @IBOutlet weak var datePickerStack: UIStackView!
    @IBOutlet weak var viewPlans: UIView!
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var navBar: UINavigationBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        TransparentnavigationBar(navBar:self.navBar)

        let fGuesture = UITapGestureRecognizer(target: self, action: #selector(self.showDropDownForPlans(sender:)))
        datePickerStack.addGestureRecognizer(fGuesture)
        datePickerStack.isUserInteractionEnabled = true
        
        self.viewPlans.clipsToBounds = true
        self.viewPlans.layer.cornerRadius = 40
        self.viewPlans.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        setGradientBackgroundReverse(view:self.view)
    }

    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
           return UIModalPresentationStyle.none
       }
    
    @objc func showDropDownForPlans(sender: AnyObject){
          
      let controller = DatePickerVC()
      controller.delegate = self
      controller.preferredContentSize = CGSize(width: view.bounds.size.width,height: 374)
      controller.modalPresentationStyle = UIModalPresentationStyle.popover
      let popController = controller.popoverPresentationController
      popController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
      popController?.delegate = self
      popController?.sourceRect = CGRect(x: self.view.bounds.midX , y:self.view.bounds.midY , width: 0, height: 0 )
      popController?.sourceView =  view
      self.present(controller, animated: true, completion: nil)
          
      }
    
    
    func selectedDate(selected: String) {
        lblDate.text = selected
       }
    
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnViewAllPlansClicked(_ sender: UIButton) {
  let profilePlansVC = ProfilePlansVC.init(nibName: "ProfilePlansVC", bundle: nil)
  profilePlansVC.modalPresentationStyle = .fullScreen
  self.present(profilePlansVC, animated: true, completion: nil)
    }
}
