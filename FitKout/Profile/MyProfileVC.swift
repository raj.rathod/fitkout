//
//  MyProfileVC.swift
//  FitKout
//
//  Created by HT-Admin on 22/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class MyProfileVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate , UICollectionViewDelegateFlowLayout , UIPopoverPresentationControllerDelegate{
    
    @IBOutlet weak var viewChat: UIView!
    @IBOutlet weak var viewHglts: UIView!
    @IBOutlet weak var viewPlans: UIView!
    
    @IBOutlet weak var btnGym: UIButton!
    @IBOutlet weak var btnFF: UIButton!
    @IBOutlet weak var btnSplmt: UIButton!
    @IBOutlet weak var btnGoal: UIButton!
    @IBOutlet weak var btnProf: UIButton!
    
    @IBOutlet weak var viewProfile: UIView!
    @IBOutlet weak var viewGallery: UIView!
    @IBOutlet weak var viewUserImage: UIView!
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblFitnessGoal: UILabel!
    
    @IBOutlet weak var followingStack: UIStackView!
    @IBOutlet weak var postsStack: UIStackView!
    @IBOutlet weak var followersStack: UIStackView!
    
    @IBOutlet weak var collectionGallery: UICollectionView!
    
    var imgArray = [ #imageLiteral(resourceName: "workout_image"),#imageLiteral(resourceName: "nurtion_icon"),#imageLiteral(resourceName: "nober_gym_icon"),#imageLiteral(resourceName: "background_image"),#imageLiteral(resourceName: "temp_categories"),#imageLiteral(resourceName: "personalt_trainer_icon"), #imageLiteral(resourceName: "temp_circuit"),#imageLiteral(resourceName: "temp_categories"),#imageLiteral(resourceName: "nober_gym_icon"),#imageLiteral(resourceName: "background_image"),#imageLiteral(resourceName: "temp_categories"),#imageLiteral(resourceName: "personalt_trainer_icon")]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        ButtonDesign()
        
        setBackGroundImageWithImage(image : #imageLiteral(resourceName: "temp_circuit") , targetView : viewProfile)
        setBackGroundImageWithImage(image : #imageLiteral(resourceName: "temp_circuit") , targetView : viewUserImage)
        viewUserImage.layer.borderWidth = 3
        viewUserImage.layer.borderColor = #colorLiteral(red: 0.003921568627, green: 0.7764705882, blue: 0.8470588235, alpha: 1).cgColor
        
        viewGallery.clipsToBounds = true
        viewGallery.layer.cornerRadius = 40
        viewGallery.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        collectionGallery.register(UINib(nibName: "PGalleryCell", bundle: nil), forCellWithReuseIdentifier: "PGalleryCell")
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.hgltsAction))
        self.viewHglts.addGestureRecognizer(gesture)

        let gestureChat = UITapGestureRecognizer(target: self, action:  #selector(self.chatAction))
        self.viewChat.addGestureRecognizer(gestureChat)
        
        let gesturePlans = UITapGestureRecognizer(target: self, action:  #selector(self.plansAction))
        self.viewPlans.addGestureRecognizer(gesturePlans)
        
        let fGuesture = UITapGestureRecognizer(target: self, action: #selector(self.launchFollwers(sender:)))
        followersStack.addGestureRecognizer(fGuesture)
        followersStack.isUserInteractionEnabled = true
        
        let fGuesture1 = UITapGestureRecognizer(target: self, action: #selector(self.launchFollwing(sender:)))
        followingStack.addGestureRecognizer(fGuesture1)
        followingStack.isUserInteractionEnabled = true

    }
    
    @objc func launchFollwers(sender: AnyObject){
        let follwersVC = FollwersVC.init(nibName: "FollwersVC", bundle: nil)
        follwersVC.modalPresentationStyle = .fullScreen
        follwersVC.positon = 0
        self.present(follwersVC, animated: true, completion: nil)
        
    }
    
    @objc func launchFollwing(sender: AnyObject){
        let follwersVC = FollwersVC.init(nibName: "FollwersVC", bundle: nil)
        follwersVC.modalPresentationStyle = .fullScreen
        follwersVC.positon = 1
        self.present(follwersVC, animated: true, completion: nil)
    }
    
    @objc func plansAction(sender : UITapGestureRecognizer) {
       let profilePlansVC = ProfilePlansVC.init(nibName: "ProfilePlansVC", bundle: nil)
       profilePlansVC.modalPresentationStyle = .fullScreen
       self.present(profilePlansVC, animated: true, completion: nil)
    }
    
    @objc func chatAction(sender : UITapGestureRecognizer) {
       let profileChatsVC = ProfileChatsVC.init(nibName: "ProfileChatsVC", bundle: nil)
       profileChatsVC.modalPresentationStyle = .fullScreen
       self.present(profileChatsVC, animated: true, completion: nil)
    }
    
   @objc func hgltsAction(sender : UITapGestureRecognizer) {
       let highlightsVC = HighlightsVC.init(nibName: "HighlightsVC", bundle: nil)
       highlightsVC.modalPresentationStyle = .fullScreen
       self.present(highlightsVC, animated: true, completion: nil)
    }
    

    //this method is for the size of items
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           let width = collectionView.frame.width/3
        //   let height : CGFloat = 180.0
           return CGSize(width: width, height: width)
       }
       //these methods are to configure the spacing between items

       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
           return UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
       }

       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
           return 0
       }

       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
           return 0
       }
       

    func numberOfSections(in collectionView: UICollectionView) -> Int {
                  return 1
              }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
              
        return imgArray.count
              
              }

           
           
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PGalleryCell", for: indexPath) as! PGalleryCell
                
        cell.imgPGallery.image = imgArray[indexPath.item]
          return cell;
              
              }
     
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
         
     }
     
    func ButtonDesign() -> Void {
        btnFF.layer.borderWidth = 2
        btnFF.layer.borderColor = UIColor.white.cgColor
        btnGym.layer.borderWidth = 2
        btnGym.layer.borderColor = UIColor.white.cgColor
        btnProf.layer.borderWidth = 2
        btnProf.layer.borderColor = UIColor.white.cgColor
        btnSplmt.layer.borderWidth = 2
        btnSplmt.layer.borderColor = UIColor.white.cgColor
        btnGoal.layer.borderWidth = 2
        btnGoal.layer.borderColor = UIColor.white.cgColor
    }
    
    
  
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnFollowClicked(_ sender: UIButton) {
    }
    
    @IBAction func btnGymsClicked(_ sender: UIButton) {
      let pGymDetailsVC = PGymDetailsVC.init(nibName: "PGymDetailsVC", bundle: nil)
           pGymDetailsVC.modalPresentationStyle = .fullScreen
    self.present(pGymDetailsVC, animated: true, completion: nil)
        
    }
    
    @IBAction func btnSupplimentsClicked(_ sender: UIButton) {
  let supplimentsVC = SupplimentsVC.init(nibName: "SupplimentsVC", bundle: nil)
   supplimentsVC.modalPresentationStyle = .fullScreen
   self.present(supplimentsVC, animated: true, completion: nil)
    }
    
    @IBAction func btnFavFoodClicked(_ sender: UIButton) {
        let favFoodVC = FavFoodVC.init(nibName: "FavFoodVC", bundle: nil)
        favFoodVC.modalPresentationStyle = .fullScreen
        self.present(favFoodVC, animated: true, completion: nil)
    }
    
    @IBAction func btnProfileClicked(_ sender: UIButton) {
        let updateProfileVC = UpdateProfileVC.init(nibName: "UpdateProfileVC", bundle: nil)
        updateProfileVC.modalPresentationStyle = .fullScreen
        self.present(updateProfileVC, animated: true, completion: nil)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
                 return UIModalPresentationStyle.none
             }
    
    @IBAction func btnGoalClicked(_ sender: UIButton) {
             let controller = FitnessGoalVC()
             controller.preferredContentSize = CGSize(width: 200,height: 200)
             controller.modalPresentationStyle = UIModalPresentationStyle.popover
             let popController = controller.popoverPresentationController
             popController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
             popController?.delegate = self
             popController?.sourceRect = CGRect(x: self.view.bounds.midX , y:self.view.bounds.midY , width: 0, height: 0 )
             popController?.sourceView = self.view;
             self.present(controller, animated: true, completion: nil)
    
    }
    
    
}
