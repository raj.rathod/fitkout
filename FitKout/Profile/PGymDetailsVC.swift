//
//  PGymDetailsVC.swift
//  FitKout
//
//  Created by HT-Admin on 22/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class PGymDetailsVC: UIViewController,UITableViewDelegate,UITableViewDataSource  {
    
    @IBOutlet weak var tablePGym: UITableView!
    
    var gymArray = [ "Basilo Gym 1" , "Basilo Gym 2" , "Basilo Gym 3" , "Basilo Gym 4" , "Basilo Gym","Basilo Gym 5"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
             tablePGym.register(UINib(nibName: "GymDetailsCell", bundle: nil), forCellReuseIdentifier: "GymDetailsCell")
             tablePGym.tableFooterView = UIView()
             tablePGym.separatorStyle = .none
             tablePGym.reloadData()
         }

    
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return gymArray.count
            
    }
            
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
              
    let cell = tableView.dequeueReusableCell(withIdentifier: "GymDetailsCell", for: indexPath) as! GymDetailsCell
               
    cell.lblGymName.text = gymArray[indexPath.row]
    cell.lblLocations.text = "Panaji"
    
    cell.btnRemove.tag = indexPath.row
    cell.btnRemove.addTarget(self, action:  #selector(removeBtnPressed(sender:)), for: .touchUpInside)
    
     return cell
               
                 }
                 
                 
func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                    
     return 280
         }
                 
func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
{
                     
 }
    
    @objc func removeBtnPressed(sender: UIButton) {
        print(sender.tag)
        
        gymArray.remove(at: sender.tag)
        self.tablePGym.reloadData()
       }
    
    
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnAddGymClicked(_ sender: UIButton) {
        let searchAndAddGymVC = SearchAndAddGymVC.init(nibName: "SearchAndAddGymVC", bundle: nil)
        searchAndAddGymVC.modalPresentationStyle = .fullScreen
       self.present(searchAndAddGymVC, animated: true, completion: nil)
    }
    
}
