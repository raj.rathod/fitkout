//
//  SearchAndAddGymVC.swift
//  FitKout
//
//  Created by HT-Admin on 22/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class SearchAndAddGymVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate , UICollectionViewDelegateFlowLayout ,UISearchBarDelegate {
    
    var filtered:[String] = []

    
    var searchActive = false
    var selectedArray:[String] = []
    var selectedRows:[IndexPath] = []
    
    @IBOutlet weak var searchBar: UISearchBar!
    
  var gymArray = [ "Basilo Gym 1" , "Basilo Gym 2" , "Basilo Gym 3" , "Basilo Gym 4" , "Basilo Gym","Basilo Gym 5"]
    @IBOutlet weak var collectionSearchGym: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        searchBar.delegate = self

        hideKeyboardWhenTappedAround()
        
        collectionSearchGym.register(UINib(nibName: "AddGymCVCell", bundle: nil), forCellWithReuseIdentifier: "AddGymCVCell")

    }


    //this method is for the size of items
         func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
             let width = collectionView.frame.width/2
             let height : CGFloat = 250.0
             return CGSize(width: width, height: height)
         }
         //these methods are to configure the spacing between items

         func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
             return UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
         }

         func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
             return 0
         }

         func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
             return 0
         }
         

      func numberOfSections(in collectionView: UICollectionView) -> Int {
                    return 1
                }

      func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
                if(searchActive) {
                    return filtered.count
                }else{
                     return gymArray.count
                    }
       
                
                }

             
             
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddGymCVCell", for: indexPath) as! AddGymCVCell
        if(searchActive){
            cell.lblgymName.text = filtered[indexPath.row]
        } else {
            cell.lblgymName.text = gymArray[indexPath.item]
        }
        
        //  cell.imgPGallery.image = imgArray[indexPath.item]
            return cell;
                
                }
       
       func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
           
       }

    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.text = nil
        searchBar.resignFirstResponder()
        collectionSearchGym.resignFirstResponder()
        self.searchBar.showsCancelButton = false
        DispatchQueue.main.async {
          self.collectionSearchGym.reloadData()
        }
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.resignFirstResponder()
    }


    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        filtered = gymArray.filter({ (text) -> Bool in
            let tmp:NSString = text as NSString
            let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
            return range.location != NSNotFound
        })

        print(filtered)
        if (filtered.count == 0){
            searchActive = false
        }
        else{
            searchActive = true
        }
        
       DispatchQueue.main.async {
         self.collectionSearchGym.reloadData()
       }
     
    }
    
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
