//
//  SupplimentsVC.swift
//  FitKout
//
//  Created by HT-Admin on 25/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class SupplimentsVC: UIViewController ,UICollectionViewDataSource,UICollectionViewDelegate , UICollectionViewDelegateFlowLayout {
    
   var splmtArray = [ "Powder 1" , "Powder 2" , "Powder 3" , "Powder 4" , "Powder 5","Powder 6"]
    
    @IBOutlet weak var collectionSplmt: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        collectionSplmt.register(UINib(nibName: "AddGymCVCell", bundle: nil), forCellWithReuseIdentifier: "AddGymCVCell")

    }

    //this method is for the size of items
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
               let width = collectionView.frame.width/2
               let height : CGFloat = 250.0
               return CGSize(width: width, height: height)
           }
           //these methods are to configure the spacing between items

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
               return UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
           }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
               return 0
           }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
               return 0
           }
           

   func numberOfSections(in collectionView: UICollectionView) -> Int {
                      return 1
                  }

   func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
                
          return splmtArray.count
                     
                  }

               
               
   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddGymCVCell", for: indexPath) as! AddGymCVCell
              cell.lblgymName.text = splmtArray[indexPath.item]
              cell.btnAdd.setTitle("REMOVE", for: .normal)
         
          //  cell.imgPGallery.image = imgArray[indexPath.item]
    cell.btnAdd.tag = indexPath.row
    cell.btnAdd.addTarget(self, action:  #selector(removeBtnPressed(sender:)), for: .touchUpInside)
    
           return cell;
                  
                  }
         
   func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
             
         }
    
    @objc func removeBtnPressed(sender: UIButton) {
        print(sender.tag)
        
        splmtArray.remove(at: sender.tag)
        self.collectionSplmt.reloadData()
       }
    
    
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnAddSplmtClicked(_ sender: UIButton) {
        let addSupplementsVC = AddSupplementsVC.init(nibName: "AddSupplementsVC", bundle: nil)
          addSupplementsVC.modalPresentationStyle = .fullScreen
          self.present(addSupplementsVC, animated: true, completion: nil)
    }
    
}
