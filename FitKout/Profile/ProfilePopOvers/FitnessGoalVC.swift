//
//  FitnessGoalVC.swift
//  FitKout
//
//  Created by HT-Admin on 25/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class FitnessGoalVC: UIViewController {

    @IBOutlet weak var btnEdit: UIButton!
    
    @IBOutlet weak var lblFGoal: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        lblFGoal.text = "Stay Fit"
        btnEdit.layer.cornerRadius = 22
        btnEdit.layer.borderWidth = 1
        btnEdit.layer.borderColor = #colorLiteral(red: 0.003921568627, green: 0.7764705882, blue: 0.8470588235, alpha: 1)
        
    }

    @IBAction func btnEditClicked(_ sender: UIButton) {
        let goalDashboardVC = GoalDashboardVC.init(nibName: "GoalDashboardVC", bundle: nil)
        goalDashboardVC.modalPresentationStyle = .fullScreen
        self.present(goalDashboardVC, animated: true, completion: nil)
    }
    
    @IBAction func btnCloseClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
