//
//  FollwersVC.swift
//  FitKout
//
//  Created by HT-Admin on 09/12/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class FollwersVC: UIViewController {

    var positon = 0
    
    @IBOutlet weak var viewFF: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let controller = FFTabsVC()
        controller.position = self.positon
        controller.view.frame = CGRect(x: 0, y: 0, width: self.viewFF.bounds.size.width, height: self.viewFF.bounds.size.height )
        self.viewFF.addSubview(controller.view)
        self.addChild(controller)
    }


    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    

}
