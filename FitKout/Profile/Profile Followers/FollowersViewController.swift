//
//  FollowersViewController.swift
//  FitKout
//
//  Created by HT-Admin on 09/12/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class FollowersViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    
    var communityArray = [CommunitySearch]()

    @IBOutlet weak var tableFollowers: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tableFollowers.register(UINib(nibName: "FFCell", bundle: nil), forCellReuseIdentifier: "FFCell")
        tableFollowers.tableFooterView = UIView()
        tableFollowers.separatorStyle = .none
        tableFollowers.reloadData()
        
        self.view.activityStartAnimating(activityColor: #colorLiteral(red: 0.003921568627, green: 0.7764705882, blue: 0.8470588235, alpha: 1), backgroundColor: .clear)
        searchUserThroughApiData(search : "")
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return communityArray.count
           }
            
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                let cell = tableView.dequeueReusableCell(withIdentifier: "FFCell", for: indexPath) as! FFCell
             
                let csModel: CommunitySearch
                csModel = communityArray[indexPath.row]
                cell.lblName.text = csModel.CommunityName
                cell.lblNickName.text = "Normal"
                cell.lblNickName.text = csModel.CommunityName
                cell.imgUser.loadImageUsingCache(withUrl : csModel.ProfileUrl)

                return cell
                
            }
            
            
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                return 80
            }
            
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           let csModel: CommunitySearch
           csModel = communityArray[indexPath.row]
           
               let myProfileVC = MyProfileVC.init(nibName: "MyProfileVC", bundle: nil)
               myProfileVC.modalPresentationStyle = .fullScreen
               self.present(myProfileVC, animated: true, completion: nil)

            }
    
    func searchUserThroughApiData(search : String) -> Void {
              
                 getDataFromUrl(url: "https://fitkout.com.cs-mum-27.bluehostwebservers.com/Fitkout/index.php/api/Community/search?LoginId=1083&Keyword=\(search)&MaxCount=20&Offset=0") { (response) in
                     print(response)
                  
                  if response.count > 0
                  {
                      var cArray = [CommunitySearch]()
                      for anItem in response as! [Dictionary<String, Any>] {
                          
                      if let netWorkObj = CommunitySearch(dictionary: anItem as [String : Any])
                      {
                      cArray.append(netWorkObj)
                       }
                      
                        self.communityArray = cArray
                       DispatchQueue.main.async {
                        self.tableFollowers.reloadData()
                       self.view.activityStopAnimating()
                          }
                         }
                  }
                  else
                  {
                      self.communityArray.removeAll()
                      DispatchQueue.main.async {
                      self.tableFollowers.reloadData()
                   self.view.activityStopAnimating()

                      }
                  }
                 }
             }


}
