//
//  FFCell.swift
//  FitKout
//
//  Created by HT-Admin on 09/12/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class FFCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblNickName: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
