//
//  FFTabsVC.swift
//  FitKout
//
//  Created by HT-Admin on 09/12/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class FFTabsVC: UIViewController {

    private let slidingTabController = UISimpleSlidingTabController()

    var position = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()

    }
    
    private func setupUI(){
        
        let arrayTitles = ["Followers","Following"]
        // view
        view.backgroundColor = .white
        view.addSubview(slidingTabController.view) // add slidingTabController to main view
        
        // navigation
        navigationItem.title = "Sliding Tab Example"
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController?.navigationBar.barStyle = .black
        
        
        
        for str in arrayTitles {
            
            if ( str == "Followers" ){
                            slidingTabController.addItem(item: FollowersViewController(), title: str)
                        }
                        else
                        {
                            slidingTabController.addItem(item: FollowingViewController(), title: str)
                        }
            
        }
        
 
        
        slidingTabController.setHeaderActiveColor(color: .black) // default blue
        slidingTabController.setHeaderInActiveColor(color: .black) // default gray
        slidingTabController.setHeaderBackgroundColor(color: .white) // default white
        slidingTabController.setCurrentPosition(position: position) // default 0
        slidingTabController.setStyle(style: .fixed) // default fixed
        slidingTabController.build() // build
    }
    

}
