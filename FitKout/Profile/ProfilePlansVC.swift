//
//  ProfilePlansVC.swift
//  FitKout
//
//  Created by HT-Admin on 27/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class ProfilePlansVC: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var tablePlans: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
         tablePlans.register(UINib(nibName: "ProfilePlansCell", bundle: nil), forCellReuseIdentifier: "ProfilePlansCell")
         tablePlans.tableFooterView = UIView()
         tablePlans.separatorStyle = .none
         tablePlans.reloadData()
        
                 }

            
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 5
                    
            }
                    
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                      
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfilePlansCell", for: indexPath) as! ProfilePlansCell
            
            cell.btnDetails.tag = indexPath.row
            cell.btnDetails.addTarget(self, action:  #selector(detailsBtnPressed(sender:)), for: .touchUpInside)
            
             return cell
                       
              }
                         
                         
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                            
             return 150
                 }
                         
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
        {
                             
         }
    
    
    @objc func detailsBtnPressed(sender: UIButton) {
        print(sender.tag)
        let plansDetailsVC = PlansDetailsVC.init(nibName: "PlansDetailsVC", bundle: nil)
        plansDetailsVC.modalPresentationStyle = .fullScreen
        self.present(plansDetailsVC, animated: true, completion: nil)
      
       }
    
   
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
