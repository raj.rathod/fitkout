//
//  PostsTVCell.swift
//  FitKout
//
//  Created by HT-Admin on 02/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class PostsTVCell: UITableViewCell {
   
    @IBInspectable var cornerRadius: CGFloat = 10
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 3
    @IBInspectable var shadowColor: UIColor? = UIColor.darkGray
    @IBInspectable var shadowOpacity: Float = 0.6
 
    
    @IBOutlet weak var lblTotalLikes: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblDays: UILabel!
    @IBOutlet weak var imgPost: UIImageView!
    @IBOutlet weak var cntPostView: UIView!
    
    @IBOutlet weak var btnLike: UIButton!
    
    @IBOutlet weak var btnComment: UIButton!
    
    @IBOutlet weak var btnShare: UIButton!
    
    @IBOutlet weak var lblCmntdUser: UILabel!
    @IBOutlet weak var btnViewAllCmnts: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {

              cntPostView.layer.cornerRadius = cornerRadius
              cntPostView.layer.masksToBounds = false
              cntPostView.layer.shadowColor = shadowColor?.cgColor
              cntPostView.layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
              cntPostView.layer.shadowOpacity = shadowOpacity
              cntPostView.layer.borderWidth = 0
              cntPostView.layer.borderColor = UIColor(red:169, green:169, blue:169, alpha:1.0).cgColor

          }
}
