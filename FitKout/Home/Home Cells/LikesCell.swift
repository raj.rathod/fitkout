//
//  LikesCell.swift
//  FitKout
//
//  Created by HT-Admin on 21/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class LikesCell: UITableViewCell {

    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUName: UILabel!
    @IBOutlet weak var lblPostCmt: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
