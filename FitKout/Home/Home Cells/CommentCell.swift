//
//  CommentCell.swift
//  FitKout
//
//  Created by HT-Admin on 21/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {

    @IBOutlet weak var imgCmtdUser: UIImageView!
    
    @IBOutlet weak var lblCmtUName: UILabel!
    @IBOutlet weak var lblCmt: UILabel!
    @IBOutlet weak var lblCmtdDate: UILabel!
    @IBOutlet weak var btnReply: UIButton!
    
    @IBOutlet weak var btnDelete: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
