//
//  LikesVC.swift
//  FitKout
//
//  Created by HT-Admin on 21/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class LikesVC: UIViewController ,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableLiked: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tableLiked.register(UINib(nibName: "LikesCell", bundle: nil), forCellReuseIdentifier: "LikesCell")
        tableLiked.tableFooterView = UIView()
        tableLiked.separatorStyle = .none
        tableLiked.reloadData()
    }

   
    
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         
             return 5
     }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
             let cell = tableView.dequeueReusableCell(withIdentifier: "LikesCell", for: indexPath) as! LikesCell
              
             return cell
        
          }
          
          
          func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
             
               return 100
          }
          
          func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         
          }

    
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
           self.dismiss(animated: true, completion: nil)
       }
}
