//
//  HomeTabVC.swift
//  FitKout
//
//  Created by HT-Admin on 28/10/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class HomeTabVC: UIViewController ,UICollectionViewDataSource,UICollectionViewDelegate ,UITableViewDelegate,UITableViewDataSource  {
    
    
    
    let imgCollection = [
    [#imageLiteral(resourceName: "temp_categories"),#imageLiteral(resourceName: "background_image"), #imageLiteral(resourceName: "temp_circuit"),#imageLiteral(resourceName: "workout_image")],
    [#imageLiteral(resourceName: "rice_image"),#imageLiteral(resourceName: "nober_gym_icon")],
    [#imageLiteral(resourceName: "nurtion_icon"),#imageLiteral(resourceName: "background_image"), #imageLiteral(resourceName: "temp_categories")],
    [#imageLiteral(resourceName: "personalt_trainer_icon")],
    [#imageLiteral(resourceName: "basmati_rice"), #imageLiteral(resourceName: "personalt_trainer_icon"), #imageLiteral(resourceName: "workout_image"), #imageLiteral(resourceName: "temp_categories"),#imageLiteral(resourceName: "basmati_rice"), #imageLiteral(resourceName: "background_image"), #imageLiteral(resourceName: "workout_image"), #imageLiteral(resourceName: "temp_categories")],
    [#imageLiteral(resourceName: "basmati_rice"), #imageLiteral(resourceName: "personalt_trainer_icon"), #imageLiteral(resourceName: "workout_image"), #imageLiteral(resourceName: "temp_categories"),#imageLiteral(resourceName: "basmati_rice"), #imageLiteral(resourceName: "background_image"), #imageLiteral(resourceName: "workout_image"), #imageLiteral(resourceName: "temp_categories"),#imageLiteral(resourceName: "basmati_rice"), #imageLiteral(resourceName: "personalt_trainer_icon"), #imageLiteral(resourceName: "workout_image"), #imageLiteral(resourceName: "temp_categories"),#imageLiteral(resourceName: "basmati_rice"), #imageLiteral(resourceName: "personalt_trainer_icon"), #imageLiteral(resourceName: "workout_image"), #imageLiteral(resourceName: "temp_categories")],
    [#imageLiteral(resourceName: "basmati_rice"), #imageLiteral(resourceName: "personalt_trainer_icon"), #imageLiteral(resourceName: "workout_image"), #imageLiteral(resourceName: "temp_categories"),#imageLiteral(resourceName: "basmati_rice"), #imageLiteral(resourceName: "personalt_trainer_icon"), #imageLiteral(resourceName: "workout_image"), #imageLiteral(resourceName: "temp_categories")],
    [#imageLiteral(resourceName: "basmati_rice"), #imageLiteral(resourceName: "personalt_trainer_icon"), #imageLiteral(resourceName: "workout_image"), #imageLiteral(resourceName: "temp_categories")],
    [#imageLiteral(resourceName: "basmati_rice"), #imageLiteral(resourceName: "personalt_trainer_icon"), #imageLiteral(resourceName: "workout_image"), #imageLiteral(resourceName: "temp_categories")],
    [#imageLiteral(resourceName: "basmati_rice"), #imageLiteral(resourceName: "personalt_trainer_icon"), #imageLiteral(resourceName: "workout_image"), #imageLiteral(resourceName: "temp_categories"),#imageLiteral(resourceName: "background_image"), #imageLiteral(resourceName: "personalt_trainer_icon"), #imageLiteral(resourceName: "workout_image"), #imageLiteral(resourceName: "temp_categories"),#imageLiteral(resourceName: "basmati_rice"), #imageLiteral(resourceName: "dummy2"), #imageLiteral(resourceName: "workout_image"), #imageLiteral(resourceName: "background_image"),#imageLiteral(resourceName: "basmati_rice"), #imageLiteral(resourceName: "personalt_trainer_icon"), #imageLiteral(resourceName: "workout_image"), #imageLiteral(resourceName: "temp_categories")]
    ]
    
    var actionButton: ActionButton!


    @IBOutlet weak var navDevideView: UIView!
    @IBOutlet weak var collectionpostHome: UICollectionView!
    @IBOutlet weak var tableviewPosts: UITableView!
    
    var imgUArr = [#imageLiteral(resourceName: "dummy2"),#imageLiteral(resourceName: "dummy"),#imageLiteral(resourceName: "workout_image"),#imageLiteral(resourceName: "dummy2"),#imageLiteral(resourceName: "dummy"),#imageLiteral(resourceName: "workout_image"),#imageLiteral(resourceName: "dummy2"),#imageLiteral(resourceName: "dummy"),#imageLiteral(resourceName: "workout_image"),#imageLiteral(resourceName: "dummy2")]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.backgroundColor = .white
        navDevideView.addShadowToUIViewWithBackGroundColor(color: UIColor.black, cornerRadius: 1 , backGroundClr : #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))

        
        collectionpostHome.register(UINib(nibName: "PostUserCVCell", bundle: nil), forCellWithReuseIdentifier: "PostUserCVCell")
        
        tableviewPosts.register(UINib(nibName: "PostsTVCell", bundle: nil), forCellReuseIdentifier: "PostsTVCell")
        tableviewPosts.tableFooterView = UIView()
        tableviewPosts.separatorStyle = .none
        tableviewPosts.reloadData()
        
//       collectionpostHome.isHidden = true
//      tableviewPosts.isHidden = true
        
        
            // Floating Menu
        let cameraImage = UIImage(named: "camera.png")!
               let videoImage = UIImage(named: "video.png")!

        let camera = ActionButtonItem(title: "Add Post Image", image: cameraImage , backgroundColor : .systemYellow)
               camera.action = { item in print("Add Post Image...") }

        let video = ActionButtonItem(title: "Add Post Video", image: videoImage, backgroundColor : .systemPink)
               video.action = { item in print("Add Post Video...") }

               actionButton = ActionButton(attachedToView: self.view, items: [camera, video])
               actionButton.action = { button in button.toggleMenu() }
         // Floating Menu end
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
            let cell = tableView.dequeueReusableCell(withIdentifier: "PostsTVCell", for: indexPath) as! PostsTVCell
             
             
        cell.btnLike.tag = indexPath.row
        cell.btnLike.addTarget(self, action:  #selector(likeBtnPressed(sender:)), for: .touchUpInside)
        cell.btnComment.tag = indexPath.row
        cell.btnComment.addTarget(self, action:  #selector(commentBtnPressed(sender:)), for: .touchUpInside)
        cell.btnShare.tag = indexPath.row
        cell.btnShare.addTarget(self, action:  #selector(shareBtnPressed(sender:)), for: .touchUpInside)
            
            return cell
       
         }
         
         
         func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            
              return 450
         }
         
         func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
         }

   func numberOfSections(in collectionView: UICollectionView) -> Int {
               return 1
           }

   func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
           
             return 10
           
           }

        
        
   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
               let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostUserCVCell", for: indexPath) as! PostUserCVCell
               if indexPath.row != 0 {
                cell.btnEdit.isHidden = true
                 }
              else
               {
                 cell.btnEdit.isHidden = false
                }
                 cell.imgPost.image = imgUArr[indexPath.row]
                 cell.imgPost.contentMode = .scaleAspectFill
    

                  return cell;
           }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
         if indexPath.row != 0 {
          let viewController:StoryViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "StoryViewController") as! StoryViewController
              viewController.imageCollection = imgCollection
              viewController.rowIndex = indexPath.item
              viewController.usersArray = self.imgUArr
          viewController.modalPresentationStyle = .fullScreen
          self.present(viewController, animated: false, completion: nil)
           }
        else
         {
            self.showAlert(title: "Would you like to Add Strories", msg: "")
          }

    }
    
    
    @objc func likeBtnPressed(sender: UIButton) {
          print(sender.tag)
        let likeVC = LikesVC.init(nibName: "LikesVC", bundle: nil)
        likeVC.modalPresentationStyle = .fullScreen
       self.present(likeVC, animated: true, completion: nil)
         
      }
    
    @objc func commentBtnPressed(sender: UIButton) {
        print(sender.tag)
       let commentVC = CommentVC.init(nibName: "CommentVC", bundle: nil)
       commentVC.modalPresentationStyle = .fullScreen
       self.present(commentVC, animated: true, completion: nil)
        
    }
    @objc func shareBtnPressed(sender: UIButton) {
        print(sender.tag)
       let shareVC = ShareVC.init(nibName: "ShareVC", bundle: nil)
       shareVC.modalPresentationStyle = .fullScreen
       self.present(shareVC, animated: true, completion: nil)
        
    }
    
    
    @IBAction func btnCommunityClicked(_ sender: UIButton) {
        let communityHomeVC = CommunityHomeVC.init(nibName: "CommunityHomeVC", bundle: nil)
        communityHomeVC.modalPresentationStyle = .fullScreen
        self.present(communityHomeVC, animated: true, completion: nil)
    }
    
    @IBAction func btnSearchClicked(_ sender: UIButton) {
        let homeSearchVC = HomeSearchVC.init(nibName: "HomeSearchVC", bundle: nil)
        homeSearchVC.modalPresentationStyle = .fullScreen
        self.present(homeSearchVC, animated: true, completion: nil)
    }
    
    
}
