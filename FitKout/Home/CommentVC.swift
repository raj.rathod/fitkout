//
//  CommentVC.swift
//  FitKout
//
//  Created by HT-Admin on 21/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class CommentVC: UIViewController , UITextViewDelegate,UITableViewDelegate,UITableViewDataSource  {
    
    @IBOutlet weak var imgCmtUser: UIImageView!
    @IBOutlet weak var tfTypeCmt: UITextView!
    @IBOutlet weak var tableCmts: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.tfTypeCmt.layer.borderColor = #colorLiteral(red: 0.003921568627, green: 0.7764705882, blue: 0.8470588235, alpha: 1)
        self.tfTypeCmt.layer.borderWidth = 1
        self.tfTypeCmt.layer.cornerRadius = 15
        
        tfTypeCmt.delegate = self
        tfTypeCmt.text = "Write a Comment..."
        tfTypeCmt.textColor = UIColor.lightGray
        
        hideKeyboardWhenTappedAround()
        
       NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
       NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        tableCmts.register(UINib(nibName: "CommentCell", bundle: nil), forCellReuseIdentifier: "CommentCell")
        tableCmts.tableFooterView = UIView()
        tableCmts.separatorStyle = .none
        tableCmts.reloadData()
        
        }
    
    func textViewDidBeginEditing(_ textView: UITextView) {

        if tfTypeCmt.textColor == UIColor.lightGray {
            tfTypeCmt.text = ""
            tfTypeCmt.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {

        if tfTypeCmt.text == "" {
           tfTypeCmt.text = "Write a Comment..."
           tfTypeCmt.textColor = UIColor.lightGray
        }
    }

        @objc func keyboardWillShow(notification: NSNotification) {
            if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                if self.view.frame.origin.y == 0 {
                    self.view.frame.origin.y -= keyboardSize.height
                }
            }
        }

        @objc func keyboardWillHide(notification: NSNotification) {
            if self.view.frame.origin.y != 0 {
                self.view.frame.origin.y = 0
            }
        }
   
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
                return 5
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          
                let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as! CommentCell
                 cell.btnReply.tag = indexPath.row
                 cell.btnReply.addTarget(self, action:  #selector(replyBtnPressed(sender:)), for: .touchUpInside)
            cell.btnDelete.tag = indexPath.row
            cell.btnDelete.addTarget(self, action:  #selector(deleteBtnPressed(sender:)), for: .touchUpInside)
                return cell
           
             }
             
             
             func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                
                  return 100
             }
             
             func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
             }
    
    @objc func deleteBtnPressed(sender: UIButton) {
        print(sender.tag)
       
    }
    @objc func replyBtnPressed(sender: UIButton) {
        print(sender.tag)
       
    }
    
    @IBAction func btnSendClicked(_ sender: UIButton) {
    }
    
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
