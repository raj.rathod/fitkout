//
//  ViewController.swift
//  FitKout
//
//  Created by HT-Admin on 21/10/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tfEmail: UITextField!
    
    @IBOutlet weak var tfPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        
        // bacground image setting
        setBackGroundImage()
        hideKeyboardWhenTappedAround()


    }



    @IBAction func btnLoginClicked(_ sender: UIButton) {
    let passString: NSString = tfPassword.text! as NSString
  
        if ((tfEmail.text!.isValidEmail()) && passString.length > 3)
        {
              if Reachability.isConnectedToNetwork()
              {
            LoginWithCredentials(userName: tfEmail.text!, password: tfPassword.text!)

              }
              else
              {
               self.showAlert(title: "Network Error!", msg: "Please Check Your Connectivity")
               }
            
        }
        else if !(tfEmail.text!.isValidEmail()){
            self.showAlert(title: "Invalid email!", msg: "Please enter valid email")
        } else if (passString.length < 3)
        {
            self.showAlert(title: "Invalid Password!", msg: "Password should not less than 3 charecters")
        }
        
        
    }
    
    @IBAction func btnForgotPassClicked(_ sender: UIButton) {
//        let signUpVC = RegisterSecVC.init(nibName: "RegisterSecVC", bundle: nil)
//        signUpVC.modalPresentationStyle = .fullScreen
//        self.present(signUpVC, animated: true, completion: nil)
        let signUpVC = RegisterThirdVC.init(nibName: "RegisterThirdVC", bundle: nil)
        signUpVC.modalPresentationStyle = .fullScreen
        self.present(signUpVC, animated: true, completion: nil)
        
    }
    
    @IBAction func btnRegisterClicked(_ sender: UIButton) {
        let signUpVC = RegisterFirstVC.init(nibName: "RegisterFirstVC", bundle: nil)
        signUpVC.modalPresentationStyle = .fullScreen
        self.present(signUpVC, animated: true, completion: nil)
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
         guard let text = textField.text else { return true }
         let newLength = text.count + string.count - range.length
         return newLength <= 10
    }
    
    @IBAction func btnSkipTapped(_ sender: UIButton) {
        let viewController:HomeViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        viewController.modalPresentationStyle = .fullScreen
        self.present(viewController, animated: false, completion: nil)
 
    }
    
    
    func LoginWithCredentials(userName : String , password : String) -> Void {
        
        self.view.activityStartAnimating(activityColor: #colorLiteral(red: 0, green: 0.9914394021, blue: 1, alpha: 1), backgroundColor: UIColor.black.withAlphaComponent(0.3))
        
        let headers = [
                      "Authorization": "Bearer fitkout",
                      "Content-Type": "application/json",
                      "cache-control": "no-cache",
                      ]
        
        let params = ["email":userName, "password":password] as Dictionary<String, String>

        var request = URLRequest(url: URL(string: "https://fitkout.com.cs-mum-27.bluehostwebservers.com/Fitkout/index.php/api/ApiCaller/login")!)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])

        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            
            print(response as Any)

            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200 {
                
                DispatchQueue.main.async {
                    self.view.activityStopAnimating()
                   let viewController:HomeViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                  viewController.modalPresentationStyle = .fullScreen
                  self.present(viewController, animated: false, completion: nil)
                }
                
            }else
            {
                self.view.activityStopAnimating()

                var message = ""
               do {
                   let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                 print(json)
                               
               message = json["Msg"]  as! String
                               
                     } catch {
                      print("error")
                     }
                self.showAlert(title: message , msg: "")

            }
            
            
        })

        task.resume()
    }
    
    
}

