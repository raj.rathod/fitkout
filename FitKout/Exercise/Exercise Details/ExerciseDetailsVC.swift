//
//  ExerciseDetailsVC.swift
//  FitKout
//
//  Created by HT-Admin on 04/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import MediaPlayer
import AudioToolbox

class ExerciseDetailsVC: UIViewController  , UIScrollViewDelegate , AVPlayerViewControllerDelegate{
    
    var setNameStr = ""
    
    @IBOutlet weak var lblSetName: UILabel!
    
    @IBOutlet weak var cntPageView: UIView!
    
    var sliderImagesArray = NSMutableArray()
    
    @IBOutlet weak var textDescription: UITextView!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let controller = PageViewController()
        controller.view.frame = CGRect(x: 0, y:0, width: self.cntPageView.bounds.size.width, height: self.cntPageView.bounds.size.height )
        self.cntPageView.addSubview(controller.view)
        self.addChild(controller)
        
        lblSetName.text = setNameStr
      
    }
   
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnVideoClicked(_ sender: UIButton) {
        let videoURL = URL(string: "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4")
        let player = AVPlayer(url: videoURL!)
        let playervc = AVPlayerViewController()
        playervc.delegate = self
        playervc.player = player
        self.present(playervc, animated: true) {
            playervc.player!.play()
        }
    }
    
    
}

