//
//  PageViewController.swift
//  FitKout
//
//  Created by HT-Admin on 04/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class PageViewController: UIViewController,UIScrollViewDelegate
    {
        let imagelist =  [#imageLiteral(resourceName: "temp_circuit"), #imageLiteral(resourceName: "temp_categories"), #imageLiteral(resourceName: "dummy2"), #imageLiteral(resourceName: "workout_image"), #imageLiteral(resourceName: "dummy")]
        var scrollView = UIScrollView()

        var pageControl = UIPageControl()

        var yPosition:CGFloat = 0
        var scrollViewContentSize:CGFloat=0;

        override func viewDidLoad() {
            super.viewDidLoad()
            
           pageControl  = UIPageControl(frame:CGRect(x: 0, y: 240, width: self.view.frame.width, height: 40))
           scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 240))

            configurePageControl()

            scrollView.delegate = self
            scrollView.showsHorizontalScrollIndicator = false
            scrollView.showsVerticalScrollIndicator = false
            self.view.addSubview(scrollView)
            for  i in stride(from: 0, to: imagelist.count, by: 1) {
                var frame = CGRect.zero
                frame.origin.x = self.scrollView.frame.size.width * CGFloat(i)
                frame.origin.y = 0
                frame.size = self.scrollView.frame.size
                self.scrollView.isPagingEnabled = true

                let myImage:UIImage = imagelist[i]
                let myImageView:UIImageView = UIImageView()
                myImageView.image = myImage
                myImageView.contentMode = .scaleToFill
                myImageView.frame = frame

                scrollView.addSubview(myImageView)
            }

            self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width * CGFloat(imagelist.count), height: self.scrollView.frame.size.height)
            pageControl.addTarget(self, action: Selector(("changePage:")), for: UIControl.Event.valueChanged)
            // Do any additional setup after loading the view.
        }
        func configurePageControl() {
            // The total number of pages that are available is based on how many available colors we have.
            self.pageControl.numberOfPages = imagelist.count
            self.pageControl.currentPage = 0
            self.pageControl.tintColor = UIColor.red
            self.pageControl.pageIndicatorTintColor = UIColor.black
            self.pageControl.currentPageIndicatorTintColor = UIColor.lightGray
            self.view.addSubview(pageControl)

        }

        // MARK : TO CHANGE WHILE CLICKING ON PAGE CONTROL
        func changePage(sender: AnyObject) -> () {
            let x = CGFloat(pageControl.currentPage) * scrollView.frame.size.width
            scrollView.setContentOffset(CGPoint(x: x,y :0), animated: true)
        }

        func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {

            let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
            pageControl.currentPage = Int(pageNumber)
        }
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
    }
