//
//  WorkoutVC.swift
//  FitKout
//
//  Created by HT-Admin on 04/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit
 

class WorkoutVC: UIViewController ,UICollectionViewDataSource,UICollectionViewDelegate , UICollectionViewDelegateFlowLayout {
    
    var workoutArray = [WorkoutCategory]()

  var workoutsArray = [ "Steps Mills" , "Strength Training" , "Ab Straight sets","Biceps" , "Ab Circuit" , "AB Straight sets","Abs Workout"]
    
    
    @IBOutlet weak var collectionWorkout: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        getAllWorkouts()
        
        collectionWorkout.register(UINib(nibName: "CategoriesAllFSCell", bundle: nil), forCellWithReuseIdentifier: "CategoriesAllFSCell")
        
       

    }
    //this method is for the size of items
      func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
          let width = collectionView.frame.width/2
       //   let height : CGFloat = 180.0
          return CGSize(width: width, height: width)
      }
      //these methods are to configure the spacing between items

      func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
          return UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
      }

      func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
          return 0
      }

      func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
          return 0
      }
      

   func numberOfSections(in collectionView: UICollectionView) -> Int {
                 return 1
             }

   func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
             
               return workoutsArray.count
             
             }

          
          
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
                 let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoriesAllFSCell", for: indexPath) as! CategoriesAllFSCell
               
                   cell.lblProgram.text = workoutsArray[indexPath.item]
                  cell.imgProgram.image = #imageLiteral(resourceName: "temp_categories")
                    return cell;
             
             }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
      
        let exerciseSetsVC = ExerciseSetsVC.init(nibName: "ExerciseSetsVC", bundle: nil)
        exerciseSetsVC.modalPresentationStyle = .fullScreen
        self.present(exerciseSetsVC, animated: true, completion: nil)
    }
    
    
    
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }

    
    func getAllWorkouts() -> Void {
        getDataFromUrl(url: "https://fitkout.com.cs-mum-27.bluehostwebservers.com/Fitkout/index.php/api/ExerciseApi/getWorkoutCategory") { (response) in
            print(response)
                      var wArray = [WorkoutCategory]()

                      for anItem in response as! [Dictionary<String, Any>] {
                          
                          if let netWorkObj = WorkoutCategory(dictionary: anItem as [String : Any]) {
                              
                              print(netWorkObj)
                      wArray.append(netWorkObj)
                       }
                       self.workoutArray = wArray
                       DispatchQueue.main.async {
                            self.collectionWorkout.reloadData()
                             }
                      }
        }
    }

}
