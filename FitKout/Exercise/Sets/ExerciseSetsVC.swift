//
//  ExerciseSetsVC.swift
//  FitKout
//
//  Created by HT-Admin on 04/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class ExerciseSetsVC: UIViewController ,UICollectionViewDataSource,UICollectionViewDelegate , UICollectionViewDelegateFlowLayout {
    
    var setsArray = [ "Push Up" , "Incline Dumbbell Hammer Curl" , "Leg Press","Wide - Grip Pull Down" ]

    @IBOutlet weak var collectionSets: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        collectionSets.register(UINib(nibName: "ExerciseSetsCell", bundle: nil), forCellWithReuseIdentifier: "ExerciseSetsCell")

    }


  //this method is for the size of items
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         let width = collectionView.frame.width/2
         let height : CGFloat = 260
         return CGSize(width: width, height: height)
     }
     //these methods are to configure the spacing between items

     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
         return UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
     }

     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
         return 0
     }

     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
         return 0
     }
     

  func numberOfSections(in collectionView: UICollectionView) -> Int {
                return 1
            }

  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            
              return setsArray.count
            
            }

         
         
   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExerciseSetsCell", for: indexPath) as! ExerciseSetsCell
                  cell.lblSetName.text = "3 SETS"
                  cell.lblSetOne.text = "3 Reps - 7kg"
                  cell.lblSet.text = setsArray[indexPath.item]
                 // cell.imgProgram.image = #imageLiteral(resourceName: "workout_image")
                   return cell;
            
            }
   
   func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let exerciseDetailsVC = ExerciseDetailsVC.init(nibName: "ExerciseDetailsVC", bundle: nil)
    exerciseDetailsVC.modalPresentationStyle = .fullScreen
    exerciseDetailsVC.setNameStr = setsArray[indexPath.item]
    self.present(exerciseDetailsVC, animated: true, completion: nil)
       
   }
    
    
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    

}
