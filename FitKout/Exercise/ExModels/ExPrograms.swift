//
//  ExPrograms.swift
//  FitKout
//
//  Created by HT-Admin on 06/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import Foundation
class ExPrograms {
    var exercise_info_id:String = ""
    var exercise_info_title: String = ""
    var thumbnail: String = ""
    var type: String = ""
    
    
    init?(dictionary:[String:Any]) {
        guard let exercise_info_id = dictionary["exercise_info_id"],
            let exercise_info_title = dictionary["exercise_info_title"],
            let thumbnail = dictionary["thumbnail"],
            let type = dictionary["type"]
           
         else{ return}
        
        self.exercise_info_id = exercise_info_id as! String
        self.exercise_info_title = exercise_info_title as! String
        self.thumbnail = thumbnail as! String
        self.type = type as! String
        
    }
}
