//
//  ExerciseSetsCell.swift
//  FitKout
//
//  Created by HT-Admin on 04/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class ExerciseSetsCell: UICollectionViewCell {
    
    @IBInspectable var cornerRadius: CGFloat = 10
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 3
    @IBInspectable var shadowColor: UIColor? = UIColor.darkGray
    @IBInspectable var shadowOpacity: Float = 0.6
    
    @IBOutlet weak var cntSetView: UIView!
    @IBOutlet weak var imgSet: UIImageView!
    
    @IBOutlet weak var lblSet: UILabel!
    
    @IBOutlet weak var lblSetName: UILabel!
    @IBOutlet weak var lblSetOne: UILabel!
    @IBOutlet weak var lblSetTwo: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    override func layoutSubviews() {

              cntSetView.layer.cornerRadius = cornerRadius
              cntSetView.layer.masksToBounds = false
              cntSetView.layer.shadowColor = shadowColor?.cgColor
              cntSetView.layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
              cntSetView.layer.shadowOpacity = shadowOpacity
              cntSetView.layer.borderWidth = 0
              cntSetView.layer.borderColor = UIColor(red:169, green:169, blue:169, alpha:1.0).cgColor

          }

}
