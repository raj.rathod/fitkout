//
//  ExerciseVC.swift
//  FitKout
//
//  Created by HT-Admin on 28/10/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class ExerciseVC: UIViewController , UICollectionViewDataSource,UICollectionViewDelegate {
    
    var workoutArray = [WorkoutCategory]()
    var programsArray = [ExPrograms]()


    
    var categoriesArray = [ "Aerobic" , "Anaerobic" , "Chest","Back" , "Yoga" , "Shoulder"]
       var programesArray = [ "12 weeks muscle" , "Fat loss" , "Muscle gain","Strength training" , "Shredded" , "Others"]
   
    
    @IBOutlet weak var navbarDevEx: UIView!
    @IBOutlet weak var cvCatExercise: UICollectionView!
    @IBOutlet weak var cvProgExercise: UICollectionView!
    @IBOutlet weak var exCntView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        getAllWorkouts()
        getPrograms()
        
        self.view.backgroundColor = .white
        navbarDevEx.addShadowToUIViewWithBackGroundColor(color: UIColor.black, cornerRadius: 1 , backGroundClr : #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))

        
        exCntView.addShadowToUIView(color: UIColor.lightGray, cornerRadius: 20)

        cvCatExercise.register(UINib(nibName: "CategoriesCell", bundle: nil), forCellWithReuseIdentifier: "CategoriesCell")
        cvProgExercise.register(UINib(nibName: "CategoriesCell", bundle: nil), forCellWithReuseIdentifier: "CategoriesCell")
        
      // exCntView.isHidden = true
    }
    
  
   func numberOfSections(in collectionView: UICollectionView) -> Int {
             return 1
         }

   func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          if collectionView == cvCatExercise
          {
           return categoriesArray.count
          }
          else
          {
           return programesArray.count
           }
         }

      
      
   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
             let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoriesCell", for: indexPath) as! CategoriesCell
           
           if collectionView == cvCatExercise {
//               let mcModel: WorkoutCategory
//               mcModel = workoutArray[indexPath.row]
//            cell.lblPname.text = mcModel.exercise_info_title
//
//          cell.imgProgram.loadImageUsingCache(withUrl : mcModel.thumbnail)
            
            cell.lblPname.text = categoriesArray[indexPath.row]
            cell.imgProgram.image = #imageLiteral(resourceName: "temp_categories")
            return cell;
            
           }else{
//             let mcModel: ExPrograms
//                            mcModel = programsArray[indexPath.row]
//                          print(mcModel.exercise_info_title)
//            cell.lblPname.text = mcModel.exercise_info_title
//            cell.imgProgram.downloaded(from: mcModel.thumbnail)
               cell.lblPname.text = programesArray[indexPath.item]
               cell.imgProgram.image = #imageLiteral(resourceName: "temp_categories")
               return cell;
           }
         
            
         }
    
      
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            
             if collectionView == cvCatExercise {
                //            let mcModel: Workout
                //            mcModel = workoutArray[indexPath.row]
                //            print(mcModel.exercise_info_title)
             }else{
//                let mcModel: ExPrograms
//                mcModel = programsArray[indexPath.row]
//              print(mcModel.exercise_info_title)
            }
                      

    //        let exerciseSetsVC = ExerciseSetsVC.init(nibName: "ExerciseSetsVC", bundle: nil)
    //        exerciseSetsVC.modalPresentationStyle = .fullScreen
    //        self.present(exerciseSetsVC, animated: true, completion: nil)
        }
        
   
   
    @IBAction func btnBeginerClicked(_ sender: UIButton) {
        let workoutVC = WorkoutVC.init(nibName: "WorkoutVC", bundle: nil)
        workoutVC.modalPresentationStyle = .fullScreen
        self.present(workoutVC, animated: true, completion: nil)
    }
    
    @IBAction func btnInterMediateClicked(_ sender: UIButton) {
        let workoutVC = WorkoutVC.init(nibName: "WorkoutVC", bundle: nil)
        workoutVC.modalPresentationStyle = .fullScreen
        self.present(workoutVC, animated: true, completion: nil)
    }
    
    @IBAction func btnAdvanceClicked(_ sender: UIButton) {
        let workoutVC = WorkoutVC.init(nibName: "WorkoutVC", bundle: nil)
        workoutVC.modalPresentationStyle = .fullScreen
        self.present(workoutVC, animated: true, completion: nil)
    }
    
    @IBAction func btnCatAllClicked(_ sender: UIButton) {
        let allCatVC = FSAllCategoriesVC.init(nibName: "FSAllCategoriesVC", bundle: nil)
        allCatVC.modalPresentationStyle = .fullScreen
        self.present(allCatVC, animated: true, completion: nil)
    }
    
    @IBAction func btnProgViewAll(_ sender: UIButton) {
        let allProgVC = FSAllProgrammesVC.init(nibName: "FSAllProgrammesVC", bundle: nil)
        allProgVC.modalPresentationStyle = .fullScreen
        self.present(allProgVC, animated: true, completion: nil)
    }
    
    @IBAction func btnFootStepsClicked(_ sender: UIButton) {
        let footStepsVC = FootStepsVC.init(nibName: "FootStepsVC", bundle: nil)
        footStepsVC.modalPresentationStyle = .fullScreen
        self.present(footStepsVC, animated: true, completion: nil)
    }
    
    func getAllWorkouts() -> Void {
        getDataFromUrl(url: "https://fitkout.com.cs-mum-27.bluehostwebservers.com/Fitkout/index.php/api/ExerciseApi/getWorkoutCategory") { (response) in
            
             var wArray = [WorkoutCategory]()
             for anItem in response as! [Dictionary<String, Any>] {
                          
             if let netWorkObj = WorkoutCategory(dictionary: anItem as [String : Any]) {
                      wArray.append(netWorkObj)
                       }
                
                       self.workoutArray = wArray
                       DispatchQueue.main.async {
                            self.cvCatExercise.reloadData()
                             }
                      }
        }
    }
    
    
    func getPrograms() -> Void {
           getDataFromUrl(url: "https://fitkout.com.cs-mum-27.bluehostwebservers.com/Fitkout/index.php/api/ExerciseApi/getPrograms") { (response) in
               print(response)
                         var pArray = [ExPrograms]()

                         for anItem in response as! [Dictionary<String, Any>] {
                             
                             if let netWorkObj = ExPrograms(dictionary: anItem as [String : Any]) {
                                 
                                 print(netWorkObj)
                         pArray.append(netWorkObj)
                          }
                          self.programsArray = pArray
                          DispatchQueue.main.async {
                               self.cvProgExercise.reloadData()
                                }
                         }
           }
       }
    
}
