//
//  BusinessVC.swift
//  FitKout
//
//  Created by HT-Admin on 28/10/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class BusinessVC: UIViewController ,UICollectionViewDataSource,UICollectionViewDelegate {

    @IBOutlet weak var navBarBusiness: UIView!
    @IBOutlet weak var cntntBView: UIView!
    
    @IBOutlet weak var collectioncatBns: UICollectionView!
    @IBOutlet weak var collectionGym: UICollectionView!
    @IBOutlet weak var collectionTrainer: UICollectionView!
    @IBOutlet weak var collectionNutrition: UICollectionView!
    @IBOutlet weak var collectionYoga: UICollectionView!
    
    var catArray = ["Gym","Trainer","Nutrition","Yoga"]
    var gymArray = ["Basillo Gym","Basillo Gym","Basillo Gym","Basillo Gym"]
    var TrainerArray = ["Arun Kumar","Moin Shah","Kunal","Niranjan"]
    var NutritionArray = ["Sakshi Sharma","Helen Janet","Kunal","Niranjan"]
    var yogaArray = ["Apurva Anand","Kunal","Niranjan","Bryan"]

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.backgroundColor = .white
        navBarBusiness.addShadowToUIViewWithBackGroundColor(color: UIColor.black, cornerRadius: 1 , backGroundClr : #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
        
        cntntBView.addShadowToUIView(color: UIColor.lightGray, cornerRadius: 20)

        collectioncatBns.register(UINib(nibName: "BCatCell", bundle: nil), forCellWithReuseIdentifier: "BCatCell")
        
        collectionGym.register(UINib(nibName: "CatDetailCell", bundle: nil), forCellWithReuseIdentifier: "CatDetailCell")
        collectionTrainer.register(UINib(nibName: "CatDetailCell", bundle: nil), forCellWithReuseIdentifier: "CatDetailCell")
        collectionNutrition.register(UINib(nibName: "CatDetailCell", bundle: nil), forCellWithReuseIdentifier: "CatDetailCell")
        collectionYoga.register(UINib(nibName: "CatDetailCell", bundle: nil), forCellWithReuseIdentifier: "CatDetailCell")
    }
    

   func numberOfSections(in collectionView: UICollectionView) -> Int {
              return 1
          }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
           if collectionView == collectioncatBns
           {
            return catArray.count
           }
           else if collectionView == collectionGym
           {
            return gymArray.count
            }else if collectionView == collectionTrainer
            {
             return TrainerArray.count
             }else if collectionView == collectionNutrition
             {
              return NutritionArray.count
              }else
              {
               return yogaArray.count
               }
        
          }

       
       
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectioncatBns
        {
               let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BCatCell", for: indexPath) as! BCatCell
                                       cell.lblCatName.text = catArray[indexPath.item]
                                       cell.imgCat.image = #imageLiteral(resourceName: "temp_circuit")
                                        return cell;
        }else  if collectionView == collectionGym {
            let gCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CatDetailCell", for: indexPath) as! CatDetailCell
            gCell.lblName.text = gymArray[indexPath.item]
            gCell.lblLocation.text = "Panaji"
            gCell.imgCatDetail.image = #imageLiteral(resourceName: "nober_gym_icon")
            return gCell
        } else  if collectionView == collectionTrainer
        {
            let gCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CatDetailCell", for: indexPath) as! CatDetailCell
            gCell.lblName.text = TrainerArray[indexPath.item]
            gCell.imgCatDetail.image = #imageLiteral(resourceName: "personalt_trainer_icon")
            gCell.lblLocation.text = "Panaji"

            return gCell
        }else if collectionView == collectionNutrition
        {
            
          let gCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CatDetailCell", for: indexPath) as! CatDetailCell
            gCell.lblName.text = NutritionArray[indexPath.item]
            gCell.imgCatDetail.image = #imageLiteral(resourceName: "nurtion_icon")
            gCell.lblLocation.text = "Panaji"
                return gCell
            
        }
        else
        {
            let gCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CatDetailCell", for: indexPath) as! CatDetailCell
                       gCell.lblName.text = yogaArray[indexPath.item]
                       gCell.lblLocation.text = "Panaji"
               //  gCell.imgCatDetail.image = #imageLiteral(resourceName: "nurtion_icon")
                           return gCell
        }


          }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collectioncatBns
               {
                     let businessSearchVC = BusinessSearchVC.init(nibName: "BusinessSearchVC", bundle: nil)
                     businessSearchVC.modalPresentationStyle = .fullScreen
                     self.present(businessSearchVC, animated: true, completion: nil)
               }else  if collectionView == collectionGym
               {
                        let businessdetailsVC = BusinessdetailsVC.init(nibName: "BusinessdetailsVC", bundle: nil)
                        businessdetailsVC.modalPresentationStyle = .fullScreen
                        self.present(businessdetailsVC, animated: true, completion: nil)
               } else  if collectionView == collectionTrainer
               {
                  let trainerDetailVC = TrainerDetailVC.init(nibName: "TrainerDetailVC", bundle: nil)
                trainerDetailVC.trainerName = TrainerArray[indexPath.item]
                   trainerDetailVC.modalPresentationStyle = .fullScreen
                               self.present(trainerDetailVC, animated: true, completion: nil)
               }else if collectionView == collectionNutrition
               {
                  let trainerDetailVC = TrainerDetailVC.init(nibName: "TrainerDetailVC", bundle: nil)
                trainerDetailVC.trainerName = NutritionArray[indexPath.item]
                   trainerDetailVC.modalPresentationStyle = .fullScreen
                             self.present(trainerDetailVC, animated: true, completion: nil)
                 
               }
               else
               {
                   let trainerDetailVC = TrainerDetailVC.init(nibName: "TrainerDetailVC", bundle: nil)
                  trainerDetailVC.trainerName = yogaArray[indexPath.item]
                 trainerDetailVC.modalPresentationStyle = .fullScreen
                           self.present(trainerDetailVC, animated: true, completion: nil)
               }

    }
    
    @IBAction func btnYogaAllClicked(_ sender: UIButton) {
        let yogaAllVC = YogaAllVC.init(nibName: "YogaAllVC", bundle: nil)
        yogaAllVC.modalPresentationStyle = .fullScreen
        self.present(yogaAllVC, animated: true, completion: nil)
    }
    
    @IBAction func btnNutAllClicked(_ sender: UIButton) {
        let nutritionAllVC = NutritionAllVC.init(nibName: "NutritionAllVC", bundle: nil)
        nutritionAllVC.modalPresentationStyle = .fullScreen
        self.present(nutritionAllVC, animated: true, completion: nil)
    }
    
    @IBAction func btnTrainerAllClicked(_ sender: UIButton) {
        let trainerAllVC = TrainerAllVC.init(nibName: "TrainerAllVC", bundle: nil)
        trainerAllVC.modalPresentationStyle = .fullScreen
        self.present(trainerAllVC, animated: true, completion: nil)
    }
    
    @IBAction func btnGymAllClicked(_ sender: UIButton) {
        let gymAllVC = GymAllVC.init(nibName: "GymAllVC", bundle: nil)
        gymAllVC.modalPresentationStyle = .fullScreen
        self.present(gymAllVC, animated: true, completion: nil)
    }
    
}
