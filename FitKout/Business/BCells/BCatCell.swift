//
//  BCatCell.swift
//  FitKout
//
//  Created by HT-Admin on 04/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class BCatCell: UICollectionViewCell {
    
    
    @IBOutlet weak var imgCat: UIImageView!
    @IBOutlet weak var lblCatName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
