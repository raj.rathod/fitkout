//
//  BViewAllCell.swift
//  FitKout
//
//  Created by HT-Admin on 08/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class BViewAllCell: UITableViewCell {
    
    @IBOutlet weak var cntViewAllV: UIView!
    @IBOutlet weak var imgViewAll: UIImageView!
    @IBOutlet weak var lblNameViewAll: UILabel!
    @IBOutlet weak var lblLctnVwAll: UILabel!
    
    @IBInspectable var cornerRadius: CGFloat = 10
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 0
    @IBInspectable var shadowColor: UIColor? = UIColor.darkGray
    @IBInspectable var shadowOpacity: Float = 0.2
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {

        cntViewAllV.layer.cornerRadius = cornerRadius
        cntViewAllV.layer.masksToBounds = false
        cntViewAllV.layer.shadowColor = shadowColor?.cgColor
        cntViewAllV.layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        cntViewAllV.layer.shadowOpacity = shadowOpacity
        cntViewAllV.layer.borderWidth = 1.0
        cntViewAllV.layer.borderColor = UIColor(red:169, green:169, blue:169, alpha:1.0).cgColor

    }
    
}
