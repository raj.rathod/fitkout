//
//  BusinessdetailsVC.swift
//  FitKout
//
//  Created by HT-Admin on 08/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class BusinessdetailsVC: UIViewController ,UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout , UITableViewDelegate,UITableViewDataSource{
    
    
    @IBOutlet weak var ratingBar: StarsView!
    @IBOutlet weak var lblRating: UILabel!
    
    var catArray = [#imageLiteral(resourceName: "nober_gym_icon"),#imageLiteral(resourceName: "temp_categories"),#imageLiteral(resourceName: "nurtion_icon"),#imageLiteral(resourceName: "personalt_trainer_icon")]

    @IBOutlet weak var pageController: UIPageControl!
    @IBOutlet weak var collectionDetails: UICollectionView!
    @IBOutlet weak var cntDetailsView: UIView!
    
    
    @IBOutlet weak var tableCmts: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        cntDetailsView.clipsToBounds = true
        cntDetailsView.layer.cornerRadius = 30
        cntDetailsView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        collectionDetails.register(UINib(nibName: "BusinessDetailCell", bundle: nil), forCellWithReuseIdentifier: "BusinessDetailCell")
        
        ratingBar.rating = 5
        
        tableCmts.register(UINib(nibName: "BSRatingCell", bundle: nil), forCellReuseIdentifier: "BSRatingCell")
        tableCmts.tableFooterView = UIView()
        
    }
    
    
    //this method is for the size of items
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            let width = collectionView.frame.width
            let height : CGFloat = 280
            return CGSize(width: width, height: height)
        }
        //these methods are to configure the spacing between items

        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
            return UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
        }

        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 0
        }

        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 0
        }
        

    func numberOfSections(in collectionView: UICollectionView) -> Int {
                 return 1
             }

       func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
               return catArray.count
           
             }

          
          
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
           
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BusinessDetailCell", for: indexPath) as! BusinessDetailCell
            cell.imgBusinessdetails.image = catArray[indexPath.item]
            self.pageController.numberOfPages = catArray.count
            return cell;
         
             }
       
       func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
          
       }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageController.currentPage = Int(pageNumber)
    }
    
    
    
  // MARK: tableview methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
            let cell = tableView.dequeueReusableCell(withIdentifier: "BSRatingCell", for: indexPath) as! BSRatingCell
         cell.lblRater.text = "Raj"
         cell.lblCmt.text = "Panaji"
         cell.imgRater.image = #imageLiteral(resourceName: "nober_gym_icon")
         cell.ratingBarView.rating = 5
            return cell
       
         }
         
         
         func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            
              return 160
         }
         
         func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
         }
    
    
    
    
    
    
    
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func pageControllClicked(_ sender: UIPageControl) {
        pageController.currentPage = catArray.count
    }
    
    
}
