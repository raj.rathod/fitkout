//
//  PlanOptnCell.swift
//  FitKout
//
//  Created by HT-Admin on 11/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class PlanOptnCell: UITableViewCell {

    @IBOutlet weak var btnChoose: UIButton!
    
    @IBOutlet weak var imgCheckBox: UIImageView!
    
    @IBOutlet weak var lblPln: UILabel!
    
    @IBOutlet weak var lblPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
