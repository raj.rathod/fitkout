//
//  PlansBCell.swift
//  FitKout
//
//  Created by HT-Admin on 11/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class PlansBCell: UITableViewCell {
    
    
    @IBOutlet weak var cntView: UIView!
    
    @IBOutlet weak var lblPlan: UILabel!
    
    @IBOutlet weak var lblRange: UILabel!
    
    @IBOutlet weak var imgPlans: UIImageView!
    
    @IBOutlet weak var btnDetails: UIButton!
    
    
    @IBInspectable var cornerRadius: CGFloat = 10
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 0
    @IBInspectable var shadowColor: UIColor? = UIColor.darkGray
    @IBInspectable var shadowOpacity: Float = 0.2
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {

           cntView.layer.cornerRadius = cornerRadius
           cntView.layer.masksToBounds = false
           cntView.layer.shadowColor = shadowColor?.cgColor
           cntView.layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
           cntView.layer.shadowOpacity = shadowOpacity
           cntView.layer.borderWidth = 1.0
           cntView.layer.borderColor = UIColor(red:169, green:169, blue:169, alpha:1.0).cgColor

       }
    
}
