//
//  PlansDetailCell.swift
//  FitKout
//
//  Created by HT-Admin on 11/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class PlansDetailCell: UICollectionViewCell {
    
    
    @IBOutlet weak var lblPlnType: UILabel!
    @IBOutlet weak var lblPln: UILabel!
    @IBOutlet weak var imgPln: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
