//
//  BSRatingCell.swift
//  FitKout
//
//  Created by HT-Admin on 08/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class BSRatingCell: UITableViewCell {
    
    
    @IBOutlet weak var imgRater: UIImageView!
    @IBOutlet weak var lblRater: UILabel!
    
    @IBOutlet weak var ratingBarView: StarsView!
    
    @IBOutlet weak var lblCmt: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
