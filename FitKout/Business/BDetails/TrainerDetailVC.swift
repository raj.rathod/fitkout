//
//  TrainerDetailVC.swift
//  FitKout
//
//  Created by HT-Admin on 11/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class TrainerDetailVC: UIViewController  ,UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout , UITableViewDelegate,UITableViewDataSource{
    
    var catArray = [#imageLiteral(resourceName: "nober_gym_icon"),#imageLiteral(resourceName: "temp_categories"),#imageLiteral(resourceName: "nurtion_icon"),#imageLiteral(resourceName: "personalt_trainer_icon")]
    
    var trainerName = ""

    @IBOutlet weak var collectionTrainer: UICollectionView!
    @IBOutlet weak var pageCntrl: UIPageControl!
    @IBOutlet weak var cntView: UIView!
    @IBOutlet weak var rtngView: StarsView!
    @IBOutlet weak var lblStreet: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var lblNoFlwrs: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var tablePlans: UITableView!
    @IBOutlet weak var tableReviews: UITableView!
    @IBOutlet weak var navTrainerName: UINavigationItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        navTrainerName.title = trainerName
        lblName.text = trainerName

        
        cntView.clipsToBounds = true
        cntView.layer.cornerRadius = 30
        cntView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
               
        collectionTrainer.register(UINib(nibName: "BusinessDetailCell", bundle: nil), forCellWithReuseIdentifier: "BusinessDetailCell")
        
        tableReviews.register(UINib(nibName: "BSRatingCell", bundle: nil), forCellReuseIdentifier: "BSRatingCell")
               tableReviews.tableFooterView = UIView()
        tablePlans.register(UINib(nibName: "PlansBCell", bundle: nil), forCellReuseIdentifier: "PlansBCell")
               tablePlans.tableFooterView = UIView()
          tablePlans.separatorStyle = .none
    }

    //this method is for the size of items
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            let width = collectionView.frame.width
            let height : CGFloat = 280
            return CGSize(width: width, height: height)
        }
        //these methods are to configure the spacing between items

        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
            return UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
        }

        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 0
        }

        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 0
        }
        

    func numberOfSections(in collectionView: UICollectionView) -> Int {
                 return 1
             }

       func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
               return catArray.count
           
             }

          
          
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
           
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BusinessDetailCell", for: indexPath) as! BusinessDetailCell
            cell.imgBusinessdetails.image = catArray[indexPath.item]
            self.pageCntrl.numberOfPages = catArray.count
            return cell;
         
             }
       
       func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
          
       }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageCntrl.currentPage = Int(pageNumber)
    }
  
    
    // MARK: tableview methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tableReviews {
            return 10
        }else
        {
            return 3
        }
            
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tableReviews
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BSRatingCell", for: indexPath) as! BSRatingCell
                 cell.lblRater.text = "Raj"
                 cell.lblCmt.text = "Panaji"
                 cell.imgRater.image = #imageLiteral(resourceName: "nober_gym_icon")
                 cell.ratingBarView.rating = 5
                    return cell
        }
        else
        {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlansBCell", for: indexPath) as! PlansBCell
           cell.btnDetails.tag = indexPath.row
           cell.btnDetails.addTarget(self, action:  #selector(detailsBtnPressed(sender:)), for: .touchUpInside)
                return cell
        }
      
        
       
         }
         
         
         func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            
            if tableView == tableReviews {
                       return 160
                   }else
                   {
                       return 130
                   }
              
         }
         
         func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
         }
    
    @objc func detailsBtnPressed(sender: UIButton) {
        print(sender.tag)
        let plansDetailsVC = PlansDetailsVC.init(nibName: "PlansDetailsVC", bundle: nil)
        plansDetailsVC.modalPresentationStyle = .fullScreen
        self.present(plansDetailsVC, animated: true, completion: nil)
        
    }
    
    @IBAction func btnFollowClicked(_ sender: UIButton) {
    }
    
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
