//
//  SlotTimeCell.swift
//  FitKout
//
//  Created by HT-Admin on 12/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class SlotTimeCell: UITableViewCell {

    @IBOutlet weak var lblStartTime: UILabel!
    
    @IBOutlet weak var imgCheckBox: UIImageView!
    @IBOutlet weak var lblEndTime: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
