//
//  ChooseNutCell.swift
//  FitKout
//
//  Created by HT-Admin on 11/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class ChooseNutCell: UICollectionViewCell {
    
    @IBOutlet weak var imgNut: UIImageView!
    @IBOutlet weak var lblNutrition: UILabel!
    @IBOutlet weak var lblExprnce: UILabel!
    
    @IBOutlet weak var cntView: UIView!
    
    @IBOutlet weak var imgCheck: UIImageView!
    
    
    @IBInspectable var cornerRadius: CGFloat = 10
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 0
    @IBInspectable var shadowColor: UIColor? = UIColor.darkGray
    @IBInspectable var shadowOpacity: Float = 0.4
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutSubviews() {

              cntView.layer.cornerRadius = cornerRadius
              cntView.layer.masksToBounds = false
              cntView.layer.shadowColor = shadowColor?.cgColor
              cntView.layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
              cntView.layer.shadowOpacity = shadowOpacity
              cntView.layer.borderWidth = 1.0
              cntView.layer.borderColor = UIColor(red:169, green:169, blue:169, alpha:1.0).cgColor

          }
       
}
