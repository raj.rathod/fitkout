//
//  SlotTimeVC.swift
//  FitKout
//
//  Created by HT-Admin on 12/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class SlotTimeVC: UIViewController , UITableViewDelegate,UITableViewDataSource, DatePickerVCDelegate , UIPopoverPresentationControllerDelegate{
    
   
    @IBOutlet weak var btnDatePicker: UIButton!
    

    var selectedIndex : [Int:Int]?
    var startArr = [ "02.00 AM", "03.00 AM", "04.00 AM" , "05.00 AM", "06.00 AM"]
    var endArr = [ "02.30 AM", "03.30 AM", "04.30 AM" , "05.30 AM", "06.30 AM"]

    @IBOutlet weak var tableSlotTime: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        selectedIndex = [Int:Int]()
        
        tableSlotTime.register(UINib(nibName: "SlotTimeCell", bundle: nil), forCellReuseIdentifier: "SlotTimeCell")
        tableSlotTime.tableFooterView = UIView()
        tableSlotTime.separatorStyle = .none
    }

    
    // MARK: tableview methods
          
          func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return startArr.count
          }
          
          func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
              
            let cell = tableView.dequeueReusableCell(withIdentifier: "SlotTimeCell", for: indexPath) as! SlotTimeCell
           cell.lblStartTime.text = startArr[indexPath.row]
           cell.lblEndTime.text = endArr[indexPath.row]

           if let val = selectedIndex![indexPath.section]{
               if indexPath.row == val{
                  cell.imgCheckBox.image = #imageLiteral(resourceName: "green_right")
                   
               }
               else{
                   
                 cell.imgCheckBox.image = nil

               }
           }
           else{
               
             cell.imgCheckBox.image = nil
           }
             return cell

               }
               
               
               func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                   return 60
                    
               }
               
               func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                   selectedIndex?.removeAll()
                   selectedIndex?.updateValue(indexPath.row, forKey: indexPath.section)
                   self.tableSlotTime.reloadData()
              
               }

    
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnCntnueClicked(_ sender: UIButton) {
        
     let plansDetailsVC = CheckHabitsVC.init(nibName: "CheckHabitsVC", bundle: nil)
     plansDetailsVC.modalPresentationStyle = .fullScreen
     self.present(plansDetailsVC, animated: true, completion: nil)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
          return UIModalPresentationStyle.none
      }
    @IBAction func btnSelectDateClicked(_ sender: UIButton) {
        
        let controller = DatePickerVC()
        controller.delegate = self
        controller.preferredContentSize = CGSize(width: view.bounds.size.width,height: 374)
        controller.modalPresentationStyle = UIModalPresentationStyle.popover
        let popController = controller.popoverPresentationController
        popController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
        popController?.delegate = self
        popController?.sourceRect = CGRect(x: self.view.bounds.midX , y:self.view.bounds.midY , width: 0, height: 0 )
        popController?.sourceView =  view
        self.present(controller, animated: true, completion: nil)
    }
    
    func selectedDate(selected: String) {
        btnDatePicker.setTitle(selected, for: .normal)
       }
    
}
