//
//  BookPlansVC.swift
//  FitKout
//
//  Created by HT-Admin on 11/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class BookPlansVC: UIViewController {
    
    var slctdPlan = ""
    var slctdPrice = ""

    
    @IBOutlet weak var lblSlctdPlan: UILabel!
    @IBOutlet weak var lblPlanPrice: UILabel!
    @IBOutlet weak var lblTotalValue: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       
        lblSlctdPlan.text = slctdPlan
        lblPlanPrice.text = "₹ " + slctdPrice + "/-"
        lblTotalValue.text = "₹ " + slctdPrice + "/-"

    }


    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnProceedBtnClicked(_ sender: UIButton) {
               let chooseSlotVC = ChooseSlotVC.init(nibName: "ChooseSlotVC", bundle: nil)
               chooseSlotVC.modalPresentationStyle = .fullScreen
               self.present(chooseSlotVC, animated: true, completion: nil)
    }
    
    
}
