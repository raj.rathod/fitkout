//
//  PlansDetailsVC.swift
//  FitKout
//
//  Created by HT-Admin on 11/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class PlansDetailsVC: UIViewController ,UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout , UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var viewScrollCnt: UIView!
    @IBOutlet weak var collectionPlns: UICollectionView!
    @IBOutlet weak var pgCntlPlns: UIPageControl!
    @IBOutlet weak var viewPlans: UIView!
    
    var selecetedPlan = ""
    var selectedPrice = ""
    
    var selectedIndex : [Int:Int]?
    var plansArr = [ "1 month", "3 Months", "6 Months" , "1 Year"]
    var priceArr = [ "399", "1099", "2099" , "4099"]

    var catArray = [#imageLiteral(resourceName: "nober_gym_icon"),#imageLiteral(resourceName: "temp_categories"),#imageLiteral(resourceName: "nurtion_icon"),#imageLiteral(resourceName: "personalt_trainer_icon")]

    @IBOutlet weak var tableChoosePlan: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        selectedIndex = [Int:Int]()

        
        collectionPlns.register(UINib(nibName: "PlansDetailCell", bundle: nil), forCellWithReuseIdentifier: "PlansDetailCell")
        
        viewScrollCnt.addShadowToUIView(color: UIColor.lightGray, cornerRadius: 20)
        viewPlans.addShadowToUIView(color: UIColor.lightGray, cornerRadius: 20)

        tableChoosePlan.register(UINib(nibName: "PlanOptnCell", bundle: nil), forCellReuseIdentifier: "PlanOptnCell")
        tableChoosePlan.tableFooterView = UIView()
        tableChoosePlan.separatorStyle = .none
    }


    // MARK: COLLECTION VIEW METHODS
   //this method is for the size of items
          func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
              let width = collectionView.frame.width
              let height : CGFloat = 320
              return CGSize(width: width, height: height)
          }
          //these methods are to configure the spacing between items

          func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
              return UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
          }

          func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
              return 0
          }

          func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
              return 0
          }
          

      func numberOfSections(in collectionView: UICollectionView) -> Int {
                   return 1
               }

         func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
                 return catArray.count
             
               }

         func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
             
              let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlansDetailCell", for: indexPath) as! PlansDetailCell
              cell.imgPln.image = catArray[indexPath.item]
              self.pgCntlPlns.numberOfPages = catArray.count
              return cell;
           
               }
         
         func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            
         }
      
      func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
          let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
          pgCntlPlns.currentPage = Int(pageNumber)
      }
    
    // MARK: COLLECTION VIEW METHODS END

    
    // MARK: tableview methods
       
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
               return 3
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           
         let cell = tableView.dequeueReusableCell(withIdentifier: "PlanOptnCell", for: indexPath) as! PlanOptnCell
        cell.lblPln.text = plansArr[indexPath.row]
        cell.lblPrice.text = priceArr[indexPath.row]
        if let val = selectedIndex![indexPath.section]{
            if indexPath.row == val{
               cell.imgCheckBox.image = #imageLiteral(resourceName: "checked")
                
            }
            else{
                
              cell.imgCheckBox.image = #imageLiteral(resourceName: "grayUnchecked")

            }
        }
        else{
            
          cell.imgCheckBox.image = #imageLiteral(resourceName: "grayUnchecked")
        }
          return cell

            }
            
            
            func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                return 80
                 
            }
            
            func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                selecetedPlan = ""
                selecetedPlan = plansArr[indexPath.row]
                selectedPrice = priceArr[indexPath.row]
                selectedIndex?.removeAll()
                selectedIndex?.updateValue(indexPath.row, forKey: indexPath.section)
                self.tableChoosePlan.reloadData()
           
            }
       
    
    @IBAction func btnBookClicked(_ sender: UIButton) {
        
        if selectedPrice.isEmpty && selecetedPlan.isEmpty {
            self.showAlert(title: "Please Select Your Plan" , msg: "")
        }
        else{
            let bookPlansVC = BookPlansVC.init(nibName: "BookPlansVC", bundle: nil)
                    bookPlansVC.slctdPlan = selecetedPlan
                    bookPlansVC.slctdPrice = selectedPrice
                    bookPlansVC.modalPresentationStyle = .fullScreen
                    self.present(bookPlansVC, animated: true, completion: nil)
        }
        
        
    }
    
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
