//
//  ChooseSlotVC.swift
//  FitKout
//
//  Created by HT-Admin on 11/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class ChooseSlotVC: UIViewController ,UICollectionViewDataSource,UICollectionViewDelegate{
    
    var selectedNtn = ""
    var selectedIndex : [Int:Int]?
    
    @IBOutlet weak var collectionChooseNut: UICollectionView!
    
    var nameArray = ["SAKSHI SHARMA","APURVA ANAND","HELER","SAKSHI SHARMA","APURVA ANAND","HELER"]
    var expArray = ["2 years of experience","2 years of experience","2 years of experience","2 years of experience","2 years of experience","2 years of experience"]
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        selectedIndex = [Int:Int]()

        collectionChooseNut.register(UINib(nibName: "ChooseNutCell", bundle: nil), forCellWithReuseIdentifier: "ChooseNutCell")

    }


     func numberOfSections(in collectionView: UICollectionView) -> Int {
                 return 1
             }

       func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
               return 5
             }
          
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
           
         
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChooseNutCell", for: indexPath) as! ChooseNutCell
        cell.lblNutrition.text = nameArray[indexPath.item]
        cell.lblExprnce.text = expArray[indexPath.row]
        
        if let val = selectedIndex![indexPath.section]{
            if indexPath.row == val{
               cell.imgCheck.image = #imageLiteral(resourceName: "green_right")
                
            }
            else{
                
              cell.imgCheck.image = nil

            }
        }
        else{
            
          cell.imgCheck.image = nil
        }
        return cell;
          

             }
       
       func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedNtn = nameArray[indexPath.row]
        selectedIndex?.removeAll()
        selectedIndex?.updateValue(indexPath.row, forKey: indexPath.section)
        self.collectionChooseNut.reloadData()
        
   //       let businessdetailsVC = BusinessdetailsVC.init(nibName: "BusinessdetailsVC", bundle: nil)
   //       businessdetailsVC.modalPresentationStyle = .fullScreen
   //       self.present(businessdetailsVC, animated: true, completion: nil)
       }
    
    
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnProceedClicked(_ sender: UIButton) {
        
        let slotTimeVC = SlotTimeVC.init(nibName: "SlotTimeVC", bundle: nil)
        slotTimeVC.modalPresentationStyle = .fullScreen
        self.present(slotTimeVC, animated: true, completion: nil)
    }
    
}
