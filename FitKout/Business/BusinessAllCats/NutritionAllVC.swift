//
//  NutritionAllVC.swift
//  FitKout
//
//  Created by HT-Admin on 08/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class NutritionAllVC: UIViewController ,UITableViewDelegate,UITableViewDataSource {
    

    @IBOutlet weak var tableNutritionAll: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tableNutritionAll.register(UINib(nibName: "BViewAllCell", bundle: nil), forCellReuseIdentifier: "BViewAllCell")
        tableNutritionAll.tableFooterView = UIView()
        tableNutritionAll.separatorStyle = .none
        tableNutritionAll.reloadData()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                 
                     return 10
             }
             
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
               
                     let cell = tableView.dequeueReusableCell(withIdentifier: "BViewAllCell", for: indexPath) as! BViewAllCell
                      cell.lblNameViewAll.text = "Sakshi Sharma"
                      cell.lblLctnVwAll.text = "Panaji"
                      cell.imgViewAll.image = #imageLiteral(resourceName: "nurtion_icon")
                     return cell
                
                  }
                  
                  
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                     
                       return 280
                  }
                  
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                 let trainerDetailVC = TrainerDetailVC.init(nibName: "TrainerDetailVC", bundle: nil)
                 trainerDetailVC.trainerName = "Arun kumar"
                trainerDetailVC.modalPresentationStyle = .fullScreen
                 self.present(trainerDetailVC, animated: true, completion: nil)
                  }

    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
         self.dismiss(animated: true, completion: nil)
    }
    
}
