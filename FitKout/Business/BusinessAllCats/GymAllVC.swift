//
//  GymAllVC.swift
//  FitKout
//
//  Created by HT-Admin on 08/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class GymAllVC: UIViewController ,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableGymAll: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tableGymAll.register(UINib(nibName: "BViewAllCell", bundle: nil), forCellReuseIdentifier: "BViewAllCell")
        tableGymAll.tableFooterView = UIView()
        tableGymAll.separatorStyle = .none
        tableGymAll.reloadData()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           
               return 10
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         
               let cell = tableView.dequeueReusableCell(withIdentifier: "BViewAllCell", for: indexPath) as! BViewAllCell
            cell.lblNameViewAll.text = "Basillo Gym"
            cell.lblLctnVwAll.text = "Panaji"
            cell.imgViewAll.image = #imageLiteral(resourceName: "nober_gym_icon")
               
               return cell
          
            }
            
            
            func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
               
                 return 280
            }
            
            func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                  let businessdetailsVC = BusinessdetailsVC.init(nibName: "BusinessdetailsVC", bundle: nil)
                  businessdetailsVC.modalPresentationStyle = .fullScreen
                  self.present(businessdetailsVC, animated: true, completion: nil)
            }
    
    
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    

}
