//
//  BusinessSearchVC.swift
//  FitKout
//
//  Created by HT-Admin on 12/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class BusinessSearchVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout , UITableViewDelegate,UITableViewDataSource ,SearchCityVCDelegate {
  
    
    @IBOutlet weak var lblCitySlctd: UILabel!
    @IBOutlet weak var dropDownView: UIView!
    @IBOutlet weak var collectionCategories: UICollectionView!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var tableResults: UITableView!
    
    var arrayCat = ["Yoga","Cycling","Weight Training","Swimming"]
    
    var selectedIndex : [Int:Int]?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        selectedIndex = [Int:Int]()

        
        self.btnSearch.addShadowToUIButton(cornerRadius: 15)
        self.dropDownView.addShadowToUIView(cornerRadius: 15)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dropDownViewTap(_:)))
        dropDownView.addGestureRecognizer(tap)
        
        collectionCategories.register(UINib(nibName: "ChooseOptnsCell", bundle: nil), forCellWithReuseIdentifier: "ChooseOptnsCell")
        
        tableResults.register(UINib(nibName: "BViewAllCell", bundle: nil), forCellReuseIdentifier: "BViewAllCell")
        tableResults.tableFooterView = UIView()
        tableResults.separatorStyle = .none
        tableResults.reloadData()


    }
    
    @objc func dropDownViewTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        
        let searchCityVC = SearchCityVC.init(nibName: "SearchCityVC", bundle: nil)
        searchCityVC.modalPresentationStyle = .fullScreen
        searchCityVC.delegate = self
        self.present(searchCityVC, animated: true, completion: nil)
    }

    //this method is for the size of items
           func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
               let width = collectionView.frame.width/3
               let height : CGFloat = 140
               return CGSize(width: width, height: height)
           }
           //these methods are to configure the spacing between items

           func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
               return UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
           }

           func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
               return 0
           }

           func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
               return 0
           }
           

       func numberOfSections(in collectionView: UICollectionView) -> Int {
                    return 1
                }

          func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return arrayCat.count
              
                }

             
             
          func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
              
               let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChooseOptnsCell", for: indexPath) as! ChooseOptnsCell
            
            cell.lblCtgrs.text = arrayCat[indexPath.item]
            
            if let val = selectedIndex![indexPath.section]{
                          if indexPath.row == val{
                             cell.imgCheck.image = #imageLiteral(resourceName: "blue_check")
                              
                          }
                          else{
                              
                            cell.imgCheck.image = nil

                          }
                      }
                      else{
                          
                        cell.imgCheck.image = nil
                      }
              
               return cell;
            
                }
          
          func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            
          selectedIndex?.removeAll()
            selectedIndex?.updateValue(indexPath.item, forKey: indexPath.section)
            self.collectionCategories.reloadData()
             
          }
       
     
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                 
                     return 10
             }
             
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
               
                 let cell = tableView.dequeueReusableCell(withIdentifier: "BViewAllCell", for: indexPath) as! BViewAllCell
                  cell.lblNameViewAll.text = "Basillo Gym"
                  cell.lblLctnVwAll.text = "Panaji"
                  cell.imgViewAll.image = #imageLiteral(resourceName: "nober_gym_icon")
        
         
                     return cell
                
                  }
                  
                  
      func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                     
                       return 280
                  }
                  
       func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                        let businessdetailsVC = BusinessdetailsVC.init(nibName: "BusinessdetailsVC", bundle: nil)
                        businessdetailsVC.modalPresentationStyle = .fullScreen
                        self.present(businessdetailsVC, animated: true, completion: nil)
                  }
    
    
    func selectedCity(selected: String) {
        lblCitySlctd.text = selected
      }
      
    
   
    @IBAction func btnLctnClicked(_ sender: UIButton) {
        
    }
    
    @IBAction func btnSearchClicked(_ sender: UIButton) {
        
    }
    
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
