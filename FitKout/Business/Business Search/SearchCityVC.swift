//
//  SearchCityVC.swift
//  FitKout
//
//  Created by HT-Admin on 12/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

protocol SearchCityVCDelegate
{
    func selectedCity( selected : String)
}

class SearchCityVC: UIViewController ,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {
    
    var delegate : SearchCityVCDelegate?

    
       var arrayCity = [String]()
       var filtered:[String] = []
       var searchActive = false
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableViewCity: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        searchBar.delegate = self

        
        tableViewCity.register(UINib(nibName: "PhysicalAcitivityCell", bundle: nil), forCellReuseIdentifier: "PhysicalAcitivityCell")
        tableViewCity.tableFooterView = UIView()
        tableViewCity.reloadData()
        
        arrayCity = ["Panaji","Mapusa" ,"Vasco da gama","goa","UK","Goa"]
        
    }

    
    // MARK: Tableview start

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           if(searchActive) {
               return filtered.count
           }else{
               return arrayCity.count
               }
           
             }
             
             func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                
              let cell = tableView.dequeueReusableCell(withIdentifier: "PhysicalAcitivityCell", for: indexPath) as! PhysicalAcitivityCell
               
               if(searchActive){
                   cell.lblActName.text = filtered[indexPath.row]
               } else {
                   cell.lblActName.text = arrayCity[indexPath.row]

               }
               cell.btnActAdd.isHidden = true
                 return cell
                 
             }
             
             
             func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                 return 50
             }
             
             func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
              // self.searchBar.resignFirstResponder()
                delegate?.selectedCity(selected: arrayCity[indexPath.row])
                self.dismiss(animated: true, completion: nil)
             }
    
    // MARK: Tableview end

      
       // MARK: Search bar start
       func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
           searchActive = true
       }

       func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
           searchActive = false
       }
       func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
           searchActive = false
           searchBar.text = nil
           searchBar.resignFirstResponder()
           tableViewCity.resignFirstResponder()
           self.searchBar.showsCancelButton = false
           DispatchQueue.main.async {
             self.tableViewCity.reloadData()
           }
       }
       func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
           searchActive = false
           searchBar.resignFirstResponder()
       }


       func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

           filtered = arrayCity.filter({ (text) -> Bool in
               let tmp:NSString = text as NSString
               let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
               return range.location != NSNotFound
           })

           print(filtered)
           if (filtered.count == 0){
               searchActive = false
           }
           else{
               searchActive = true
           }
           
          DispatchQueue.main.async {
            self.tableViewCity.reloadData()
          }
        
       }
       
    // MARK: Search bar end
    
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        
        self.dismiss(animated: true, completion: nil)
    }
    

}
