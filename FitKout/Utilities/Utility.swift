//
//  Utility.swift
//  FitKout
//
//  Created by HT-Admin on 21/10/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import Foundation
import UIKit

class Utility
{
   
}

extension UIViewController
{

    
    func TransparentnavigationBar(navBar:UINavigationBar) {
        navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navBar.shadowImage = UIImage()
        navBar.isTranslucent = true
    } //usage  TransparentnavigationBar(navBar:self.navBar)

    
    // MARK: background image to view
    func setBackGroundImage()
    {
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "background_image.png")
        backgroundImage.contentMode =  UIView.ContentMode.scaleAspectFill
        self.view.insertSubview(backgroundImage, at: 0)
    } // usage setBackGroundImage()
    
    // MARK: background image to view
    func setBackGroundImageWithImage(image : UIImage , targetView : UIView)
    {
        let backgroundImage = UIImageView(frame: CGRect(x: 0, y: 0, width: targetView.frame.width, height: targetView.frame.height))
        backgroundImage.backgroundColor = UIColor.black
        backgroundImage.image = image
        backgroundImage.contentMode = .scaleAspectFill
        targetView.insertSubview(backgroundImage, at: 0)
    } // usage setBackGroundImageWithImage(image : #imageLiteral(resourceName: "temp_circuit") , targetView : viewProfile)

    func setBackGroundViewToView(targetView : UIView)
    {
        let coverView = UIView(frame: CGRect(x: 0, y: 0, width: targetView.frame.width, height: targetView.frame.height))
        coverView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        targetView.insertSubview(coverView, at: 0)
    }
    
    // MARK: Gradient color to view
    func setGradientBackground(view:UIView) {
              let colorTop =  #colorLiteral(red: 0.003921568627, green: 0.7764705882, blue: 0.8470588235, alpha: 1).cgColor
              let colorBottom = #colorLiteral(red: 0.09411764706, green: 0.2156862745, blue: 0.4941176471, alpha: 1).cgColor

              let gradientLayer = CAGradientLayer()
              gradientLayer.colors = [colorTop, colorBottom]
              gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
              gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
              gradientLayer.frame = view.bounds

              view.layer.insertSublayer(gradientLayer, at:0)
          } // usage setGradientBackgroundReverse(view:self.parentView)
    
    // MARK: Gradient color to view in reverse
       func setGradientBackgroundReverse(view:UIView) {
                 let colorBottom =  #colorLiteral(red: 0.003921568627, green: 0.7764705882, blue: 0.8470588235, alpha: 1).cgColor
                 let colorTop = #colorLiteral(red: 0.09411764706, green: 0.2156862745, blue: 0.4941176471, alpha: 1).cgColor

                 let gradientLayer = CAGradientLayer()
                 gradientLayer.colors = [colorTop, colorBottom]
                 gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
                 gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
                 gradientLayer.frame = view.bounds

                 view.layer.insertSublayer(gradientLayer, at:0)
             } // usage         setGradientBackgroundReverse(view:self.parentView)

    
    
    // MARK: Auto Dismiss Alert
    func presentAlertWithTitle(title: String, message: String, options: String..., completion: @escaping (Int) -> Void) {
           let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
           for (index, option) in options.enumerated() {
               alertController.addAction(UIAlertAction.init(title: option, style: .default, handler: { (action) in
                   completion(index)
               }))
           }
           self.present(alertController, animated: true, completion: nil)
           
       }
       
    
    // MARK: Hide keyboard
       func hideKeyboardWhenTappedAround() {
           let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
           tap.cancelsTouchesInView = false
           view.addGestureRecognizer(tap)
       }
       @objc func dismissKeyboard() {
           view.endEditing(true)
       } // uasge  hideKeyboardWhenTappedAround()

    
    // MARK: toast like android
    func showToast(message : String, font: UIFont) {

        let toastLabel = UILabel(frame: CGRect(x: 0, y: self.view.frame.size.height-100, width: self.view.frame.size.width, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.font = font
        toastLabel.lineBreakMode = .byWordWrapping
        toastLabel.numberOfLines = 0
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
             toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    } // uasge showToast(message: "Please enter valid email", font: UIFont(name: "Roboto-Medium", size: 15)!)

    
    // MARK: Alert with textfiled

    func showInputDialog(title:String? = nil,
                         subtitle:String? = nil,
                         actionTitle:String? = "Add",
                         cancelTitle:String? = "Cancel",
                         inputPlaceholder:String? = nil,
                         inputKeyboardType:UIKeyboardType = UIKeyboardType.default,
                         cancelHandler: ((UIAlertAction) -> Swift.Void)? = nil,
                         actionHandler: ((_ text: String?) -> Void)? = nil) {

        let alert = UIAlertController(title: title, message: subtitle, preferredStyle: .alert)
        alert.addTextField { (textField:UITextField) in
            textField.placeholder = inputPlaceholder
            textField.keyboardType = inputKeyboardType
        }
        
        alert.addAction(UIAlertAction(title: actionTitle, style: .default, handler: { (action:UIAlertAction) in
                   guard let textField =  alert.textFields?.first else {
                       actionHandler?(nil)
                       return
                   }
                   actionHandler?(textField.text)
               }))
        
        alert.addAction(UIAlertAction(title: cancelTitle, style: .destructive, handler: cancelHandler))
        
       
        self.present(alert, animated: true, completion: nil)
    } // usage
    /* showInputDialog(title: "Add A Meal",
                          subtitle: "Please enter the meal below.",
                          actionTitle: "DONE",
                          cancelTitle: "CLOSE",
                          inputPlaceholder: "Your Meal Name",
                          inputKeyboardType: .default)
          { (input:String?) in
              print("The new meal is \(input ?? "")")
          } */
    
    
    // MARK: Api networks
    func getDataFromUrl(url: String, completion: @escaping (_ success: AnyObject) -> Void) {

        if Reachability.isConnectedToNetwork()
        {
            //@escaping...If a closure is passed as an argument to a function and it is invoked after the function returns, the closure is @escaping.
             let headers = [
                   "Authorization": "Bearer fitkout",
                   "Content-Type": "application/json",
                 ]
            
            var request = URLRequest(url: URL(string: url)! )
            request.httpMethod = "GET"
            request.allHTTPHeaderFields = headers

            let task = URLSession.shared.dataTask(with: request) { Data, response, error in

                guard let data = Data, error == nil else {  // check for fundamental networking error

                    print("error=\(error?.localizedDescription)")
                    return
                }

                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {  // check for http errors

                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print(response!)
                    return

                }

                let responseString  = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as AnyObject
                completion(responseString)

            }
            task.resume()
        }
        else
        {
       //  self.showAlert(title: "Network Error!", msg: "Please Check Your Connectivity")
         }
        

    }
    
    
    func getDataFromJson(url: String, parameter: String, completion: @escaping (_ success: [String : AnyObject]) -> Void) {

        //@escaping...If a closure is passed as an argument to a function and it is invoked after the function returns, the closure is @escaping.
         let headers = [
               "Authorization": "Bearer fitkout",
               "Content-Type": "application/json",
               "cache-control": "no-cache",
             ]
        
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        let postString = parameter

        request.httpBody = postString.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { Data, response, error in

            guard let data = Data, error == nil else {  // check for fundamental networking error

                print("error=\(error)")
                return
            }

            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {  // check for http errors

                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print(response!)
                return

            }

            let responseString  = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String : AnyObject]
            completion(responseString)

        }
        task.resume()
    }
    
    //MARK:- Create JSON String
    func createJSONParameterString(postBody:AnyObject) -> String {

        if let objectData = try? JSONSerialization.data(withJSONObject: postBody, options: JSONSerialization.WritingOptions(rawValue: 0)) {
            let objectString = String(data: objectData, encoding: .utf8)
            return objectString ?? ""
        }
        return ""
    }
} // usage  let parameters = createJSONParameterString(postBody: postBody as AnyObject)  print(parameters)
      

// MARK: Email validations
extension String {
func isValidEmail() -> Bool {
    let regex = try? NSRegularExpression(pattern: "^(((([a-zA-Z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])+(\\.([a-zA-Z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])+)*)|((\\x22)((((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(([\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x7f]|\\x21|[\\x23-\\x5b]|[\\x5d-\\x7e]|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])|(\\([\\x01-\\x09\\x0b\\x0c\\x0d-\\x7f]|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}]))))*(((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(\\x22)))@((([a-zA-Z]|\\d|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])|(([a-zA-Z]|\\d|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])([a-zA-Z]|\\d|-|\\.|_|~|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])*([a-zA-Z]|\\d|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])))\\.)+(([a-zA-Z]|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])|(([a-zA-Z]|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])([a-zA-Z]|\\d|-|_|~|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])*([a-zA-Z]|[\\x{00A0}-\\x{D7FF}\\x{F900}-\\x{FDCF}\\x{FDF0}-\\x{FFEF}])))\\.?$", options: .caseInsensitive)
    return regex?.firstMatch(in: self, options: [], range: NSMakeRange(0, self.count)) != nil
}
func isValidName() -> Bool{
    let regex = try? NSRegularExpression(pattern: "^[\\p{L}\\.]{2,30}(?: [\\p{L}\\.]{2,30}){0,2}$", options: .caseInsensitive)
    
    return regex?.firstMatch(in: self, options: [], range: NSMakeRange(0, self.count)) != nil
}
    
} //usage if !(tfEmailId.text!.isValidEmail())

// MARK: Shadow effect to Button
extension UIButton {
func addShadowToUIButton(color: UIColor = UIColor.lightGray, cornerRadius: CGFloat) {
    
    self.backgroundColor = UIColor.white
    self.layer.masksToBounds = false
    self.layer.shadowColor = color.cgColor
    self.layer.shadowOffset = CGSize(width: 0, height: 0)
    self.layer.shadowOpacity = 5.0
    self.backgroundColor = .white
    self.layer.cornerRadius = cornerRadius
}
}
// MARK: Shadow effect to Button end


// MARK: Shadow effect to view
extension UIView {
    func addShadowToUIView(color: UIColor = UIColor.lightGray, cornerRadius: CGFloat) {
        
        self.backgroundColor = UIColor.white
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowOpacity = 5.0
        self.backgroundColor = .white
        self.layer.cornerRadius = cornerRadius
    }
    
    func addShadowToUIViewWithBackGroundColor(color: UIColor = UIColor.lightGray, cornerRadius: CGFloat , backGroundClr : UIColor) {
        
        self.backgroundColor = backGroundClr
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowOpacity = 5.0
        self.backgroundColor = backGroundClr
        self.layer.cornerRadius = cornerRadius
    }
    
} // usage navBarBusiness.addShadowToUIViewWithBackGroundColor(color: UIColor.black, cornerRadius: 1 , backGroundClr : #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))



// MARK: Imageview alignment in Textfield
extension UITextField {

enum Direction {
    case Left
    case Right
}

// MARK: add image to textfield
    func withImage(direction: Direction, image: UIImage, colorSeparator: UIColor, colorBorder: UIColor ){
    let mainView = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 40))
    mainView.layer.cornerRadius = 20

    let view = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 40))
    view.backgroundColor = .clear
    view.clipsToBounds = true
    view.layer.cornerRadius = 20
    view.layer.borderWidth = CGFloat(0.0)
    view.layer.borderColor = colorBorder.cgColor
    mainView.addSubview(view)

    let imageView = UIImageView(image: image)
    imageView.contentMode = .scaleAspectFit
    imageView.frame = CGRect(x: 12.0, y: 10.0, width: 24.0, height: 24.0)
    view.addSubview(imageView)
        
       

    let seperatorView = UIView()
    seperatorView.backgroundColor = colorSeparator
    mainView.addSubview(seperatorView)

    if(Direction.Left == direction){ // image left
        seperatorView.frame = CGRect(x: 45, y: 0, width: 5, height: 40)
        self.leftViewMode = .always
        self.leftView = mainView
    } else { // image right
        seperatorView.frame = CGRect(x: 0, y: 0, width: 5, height: 40)
        self.rightViewMode = .always
        self.rightView = mainView
    }

    self.layer.borderColor = colorBorder.cgColor
    self.layer.borderWidth = CGFloat(0.0)
    self.layer.cornerRadius = 20
}

} // usage tfMentleCondition.withImage(direction: .Right, image: myImage, colorSeparator: UIColor.clear, colorBorder: UIColor.clear)



class CustomSlider: UISlider
{

    override func awakeFromNib()
    {
       super.awakeFromNib()
      // self.setThumbImage(UIImage(named:"Scale.png")!, for:.normal)

       let trackingImage = UIImage(named:"Scale.png")!
        

       self.setMinimumTrackImage(trackingImage, for:.normal)

       self.setMaximumTrackImage(trackingImage, for:.normal)

    }
    override func layoutSubviews()
    {
        super.layoutSubviews()
        super.layer.cornerRadius = self.frame.height/2  //Do something additional if you want to do!.
    }

    override func trackRect(forBounds bounds: CGRect) -> CGRect
    {
        var newBounds = super.trackRect(forBounds: bounds)
        newBounds.size.height = 40 //Size of your track
        return newBounds
    }
}

// MARK: double conversion
extension Double {

    func truncate(places: Int) -> Double {
        return Double(floor(pow(10.0, Double(places)) * self)/pow(10.0, Double(places)))
    }

} // usage let currentValue = Double(sender.value).truncate(places: 3)


// MARK: Activity Indicator
extension UIView{
    
    func activityStartAnimating(activityColor: UIColor, backgroundColor: UIColor) {
        let backgroundView = UIView()
        backgroundView.frame = UIScreen.main.bounds
        backgroundView.backgroundColor = backgroundColor
        backgroundView.tag = 475647
        
        let loadingView: UIView = UIView()
        loadingView.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        loadingView.center = backgroundView.center
        loadingView.backgroundColor = backgroundColor
        loadingView.clipsToBounds = true
       // loadingView.layer.cornerRadius = 20
        
        var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
        activityIndicator = UIActivityIndicatorView(frame: CGRect.init(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
        
        activityIndicator.autoresizingMask = [.flexibleWidth, .flexibleHeight]
       //activityIndicator.center = loadingView.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)

        activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
        activityIndicator.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)

        activityIndicator.color = activityColor
        activityIndicator.startAnimating()
        self.isUserInteractionEnabled = false
        
        loadingView.addSubview(activityIndicator)
        
        backgroundView.addSubview(loadingView)
        
        self.addSubview(backgroundView)
    }
    
    func activityStopAnimating() {
        if let background = viewWithTag(475647){
            background.removeFromSuperview()
        }
        self.isUserInteractionEnabled = true
}
}

// MARK: showing image from url
extension UIImageView {
    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFill) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
        }.resume()
    }
    func downloaded(from link: String, contentMode mode: UIView.ContentMode = .scaleAspectFill) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}

// usage    imageView.downloaded(from: urlStr as! String)



// MARK: background image to search bar
extension UIImage {
    static func imageFromLayer (layer: CALayer) -> UIImage? {
        UIGraphicsBeginImageContext(layer.frame.size)
        guard let currentContext = UIGraphicsGetCurrentContext() else {return nil}
        layer.render(in: currentContext)
        let outputImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return outputImage
    }
} // usage  self.searchActivity.imageFromLayer (color: #colorLiteral(red: 0, green: 0.8126131296, blue: 0.8772806525, alpha: 1))

// MARK: background color to search bar
extension UISearchBar {
    func setCustomBackgroundColor (color: UIColor) {
        let backgroundLayer = CALayer()
        backgroundLayer.frame = frame
        backgroundLayer.backgroundColor = color.cgColor
        if let image = UIImage.imageFromLayer(layer: backgroundLayer) {
            self.setBackgroundImage(image, for: .any, barMetrics: .default)
        }
    }
} // usage  self.searchActivity.setCustomBackgroundColor (color: #colorLiteral(red: 0, green: 0.8126131296, blue: 0.8772806525, alpha: 1))


extension UITextField {
       func setLeftPaddingPoints(_ amount:CGFloat){
           let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
           self.leftView = paddingView
           self.leftViewMode = .always
       }
       func setRightPaddingPoints(_ amount:CGFloat) {
           let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
           self.rightView = paddingView
           self.rightViewMode = .always
       }
   } // usage textField.setLeftPaddingPoints(10)
    // textField.setRightPaddingPoints(10)

// MARK: image from url 2

extension UIImage {
    convenience init?(url: URL?) {
        guard let url = url else { return nil }

        do {
            let data = try Data(contentsOf: url)
            self.init(data: data)
        } catch {
            print("Cannot load image from url: \(url) with error: \(error)")
            return nil
        }
    }
} // loading late // usage cell.imgProgram.image = UIImage(url: URL(string: mcModel.thumbnail))

// MARK: image from url with indicator view
extension UIImageView {
    public func imageFromURL(urlString: String) {

        let activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator.frame = CGRect.init(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
        activityIndicator.startAnimating()
        if self.image == nil{
            self.addSubview(activityIndicator)
        }

        URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, response, error) -> Void in

            if error != nil {
                print(error ?? "No Error")
                return
            }
            
            
            DispatchQueue.main.async(execute: { () -> Void in
               let image = UIImage(data: data!)
               activityIndicator.removeFromSuperview()
               self.image = image
            })

        }).resume()
    }
} // usage best fro single image usage cell.imgProgram.imageFromURL(urlString: mcModel.thumbnail)

// MARK: image from url with cache
let imageCache = NSCache<NSString, AnyObject>()

extension UIImageView {
    func loadImageUsingCache(withUrl urlString : String) {
        let url = URL(string: urlString)
        self.image = nil

        // check cached image
        if let cachedImage = imageCache.object(forKey: urlString as NSString) as? UIImage {
            self.image = cachedImage
            return
        }

        // if not, download image from url
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
            if error != nil {
                print(error!)
                return
            }

            DispatchQueue.main.async {
                if let image = UIImage(data: data!) {
                    imageCache.setObject(image, forKey: urlString as NSString)
                    self.image = image
                }
            }

        }).resume()
    }
}
// usage cell.imgProgram.loadImageUsingCache(withUrl : mcModel.thumbnail)



// MARK: Rating bar

extension NSMutableAttributedString{

    func starWithRating(rating:Float, outOfTotal totalNumberOfStars:NSInteger, withFontSize size:CGFloat) ->NSAttributedString{


        let currentFont = UIFont(name: "Roboto-Bold", size: size)!

        let activeStarFormat = [ NSAttributedString.Key.font:currentFont, NSAttributedString.Key.foregroundColor: UIColor.red];

        let inactiveStarFormat = [ NSAttributedString.Key.font:currentFont, NSAttributedString.Key.foregroundColor: UIColor.lightGray];

        let starString = NSMutableAttributedString()

        for i in 0..<totalNumberOfStars{

            if(rating >= Float(i+1)){
                // This is for selected star. Change the unicode value according to the font that you use
                starString.append(NSAttributedString(string: "\u{22C6} ", attributes: activeStarFormat))
            }
            else if (rating > Float(i)){
                // This is for selected star. Change the unicode value according to the font that you use
                starString.append(NSAttributedString(string: "\u{E1A1} ", attributes: activeStarFormat))
            }
            else{
                // This is for de-selected star. Change the unicode value according to the font that you use
                starString.append(NSAttributedString(string: "\u{22C6} ", attributes: inactiveStarFormat))
            }
        }

        return starString
    }
}


// MARK: image cropping
extension UIImage {
    
    func crop(to:CGSize) -> UIImage {
        
        guard let cgimage = self.cgImage else { return self }
        
        let contextImage: UIImage = UIImage(cgImage: cgimage)
        
        guard let newCgImage = contextImage.cgImage else { return self }
        
        let contextSize: CGSize = contextImage.size
        
        //Set to square
        var posX: CGFloat = 0.0
        var posY: CGFloat = 0.0
        let cropAspect: CGFloat = to.width / to.height
        
        var cropWidth: CGFloat = to.width
        var cropHeight: CGFloat = to.height
        
        if to.width > to.height { //Landscape
            cropWidth = contextSize.width
            cropHeight = contextSize.width / cropAspect
            posY = (contextSize.height - cropHeight) / 2
        } else if to.width < to.height { //Portrait
            cropHeight = contextSize.height
            cropWidth = contextSize.height * cropAspect
            posX = (contextSize.width - cropWidth) / 2
        } else { //Square
            if contextSize.width >= contextSize.height { //Square on landscape (or square)
                cropHeight = contextSize.height
                cropWidth = contextSize.height * cropAspect
                posX = (contextSize.width - cropWidth) / 2
            }else{ //Square on portrait
                cropWidth = contextSize.width
                cropHeight = contextSize.width / cropAspect
                posY = (contextSize.height - cropHeight) / 2
            }
        }
        
        let rect: CGRect = CGRect(x: posX, y: posY, width: cropWidth, height: cropHeight)
        
        // Create bitmap image from context using the rect
        guard let imageRef: CGImage = newCgImage.cropping(to: rect) else { return self}
        
        // Create a new image based on the imageRef and rotate back to the original orientation
        let cropped: UIImage = UIImage(cgImage: imageRef, scale: self.scale, orientation: self.imageOrientation)
        
        UIGraphicsBeginImageContextWithOptions(to, false, self.scale)
        cropped.draw(in: CGRect(x: 0, y: 0, width: to.width, height: to.height))
        let resized = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return resized ?? self
    }
} // usage   imgProfile.image = image??.crop(to: size)


extension UIView {
    func screenshot() -> UIImage {
        if #available(iOS 10.0, *) {
            return UIGraphicsImageRenderer(size: bounds.size).image { _ in
                drawHierarchy(in: CGRect(origin: .zero, size: bounds.size), afterScreenUpdates: true)
            }
        } else {
            UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
            drawHierarchy(in: self.bounds, afterScreenUpdates: true)
            let image = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
            UIGraphicsEndImageContext()
            return image
        }
    }
}
