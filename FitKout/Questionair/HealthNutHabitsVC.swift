//
//  HealthNutHabitsVC.swift
//  FitKout
//
//  Created by HT-Admin on 14/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class HealthNutHabitsVC: UIViewController {
    
    @IBOutlet weak var tfOne: UITextField!
    @IBOutlet weak var tfTwo: UITextField!
    @IBOutlet weak var tfThree: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        hideKeyboardWhenTappedAround()
        tfOne.layer.borderWidth = 0.5
        tfOne.layer.borderColor = UIColor.lightGray.cgColor
        tfTwo.layer.borderWidth = 0.5
        tfTwo.layer.borderColor = UIColor.lightGray.cgColor
        tfThree.layer.borderWidth = 0.5
        tfThree.layer.borderColor = UIColor.lightGray.cgColor
        
    }
    
    

    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)

    }
    
    @IBAction func btnPrevClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)

    }
    
    @IBAction func btnNextClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
