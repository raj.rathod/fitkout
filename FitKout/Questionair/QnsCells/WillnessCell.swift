//
//  WillnessCell.swift
//  FitKout
//
//  Created by HT-Admin on 14/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class WillnessCell: UITableViewCell {
    
    
    @IBOutlet weak var lblQns: UILabel!
    
    @IBOutlet weak var lblRating: UILabel!
    
    @IBOutlet weak var btnIncrement: UIButton!
    
    @IBOutlet weak var btnDecrement: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
