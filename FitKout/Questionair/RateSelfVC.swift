//
//  RateSelfVC.swift
//  FitKout
//
//  Created by HT-Admin on 14/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class RateSelfVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var countArray = [Int]()


    @IBOutlet weak var tableRatingsQns: UITableView!
    
    var raingQnsArray = ["Significantly modify diet","Take nutritional supllements each day","Modify life style (Ex: Work demands ,sleeps habits , physical activity)"]

    
    override func viewDidLoad() {
        super.viewDidLoad()

      tableRatingsQns.register(UINib(nibName: "WillnessCell", bundle: nil), forCellReuseIdentifier: "WillnessCell")
            tableRatingsQns.tableFooterView = UIView()
            tableRatingsQns.separatorStyle = .none
        
        for arr in self.raingQnsArray{
             self.countArray.append(0)
        }
        
        }

        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                
              return raingQnsArray.count
               
             }
             
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                 let cell = tableView.dequeueReusableCell(withIdentifier: "WillnessCell", for: indexPath) as! WillnessCell
              
                cell.lblQns.text = raingQnsArray[indexPath.row]
                cell.lblRating.text = "\(countArray[indexPath.row])"
                cell.btnIncrement.tag = indexPath.row
                cell.btnDecrement.tag = indexPath.row
                cell.btnIncrement.addTarget(self, action: #selector(addPressed(sender:)), for: .touchUpInside)
                cell.btnDecrement.addTarget(self, action: #selector(removePressed(sender:)), for: .touchUpInside)

                 return cell
             }
           
    @objc func stepperValueChanged(sender : UIStepper){
        if sender.stepValue != 0{
            countArray[sender.tag] = Int(sender.value)
        }
        tableRatingsQns.reloadData()
    }
    @objc func addPressed(sender : UIButton){

        countArray[sender.tag] = countArray[sender.tag] + 1
        
        print(countArray[sender.tag])
        tableRatingsQns.reloadData()
    }
    
    @objc func removePressed(sender : UIButton){

        if countArray[sender.tag] > 0
        {
            countArray[sender.tag] = countArray[sender.tag] - 1

        }
        
        print(countArray[sender.tag])
        tableRatingsQns.reloadData()
    }
    
             
      func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                 return 120
                 
             }
             
      func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                
                 
             }
    
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnNextClicked(_ sender: UIButton) {
        let qnsvC = QnsvC.init(nibName: "QnsvC", bundle: nil)
        qnsvC.modalPresentationStyle = .fullScreen
        self.present(qnsvC, animated: true, completion: nil)
    }
    
    
    @IBAction func btnPreClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)

    }
    
}
