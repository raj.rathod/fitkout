//
//  FoodPrefVC.swift
//  FitKout
//
//  Created by HT-Admin on 14/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class FoodPrefVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
      var foodPrefArray = ["Vegeterian","Non-Vegetrian","Eggeterian"]
    var selectedArray:[String] = []
    var selectedRows:[IndexPath] = []
    
    @IBOutlet weak var tableFoodPref: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tableFoodPref.register(UINib(nibName: "PlanOptnCell", bundle: nil), forCellReuseIdentifier: "PlanOptnCell")
        tableFoodPref.tableFooterView = UIView()
        tableFoodPref.separatorStyle = .none
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
          return foodPrefArray.count
           
         }
         
         func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
             let cell = tableView.dequeueReusableCell(withIdentifier: "PlanOptnCell", for: indexPath) as! PlanOptnCell
          
            cell.lblPln.text = foodPrefArray[indexPath.row]
             cell.lblPrice.isHidden = true
          
             if selectedRows.contains(indexPath)
             {
                 cell.imgCheckBox.image = #imageLiteral(resourceName: "green_right")
                 
             }
             else
             {
                 cell.imgCheckBox.image = #imageLiteral(resourceName: "grayUnchecked")
                
             }
             
             return cell
         }
         
         
         func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
             return 60
             
         }
         
         func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
             let selectedIndexPath = IndexPath(row: indexPath.row, section: 0)
            
             if self.selectedRows.contains(selectedIndexPath)
             {
                 self.selectedRows.remove(at: self.selectedRows.index(of: selectedIndexPath)!)
                 
              selectedArray = selectedArray.filter(){$0 != foodPrefArray[indexPath.row]}
                 print(selectedArray)
             }
             else
             {
                 self.selectedRows.append(selectedIndexPath)
                 selectedArray.append(foodPrefArray[indexPath.row])
                 print(selectedArray)
             }
             self.tableFoodPref.reloadData()
             
         }

    @IBAction func btnPrevClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)

    }
    
    @IBAction func btnNextClicked(_ sender: UIButton) {
        let healthNutHabitsVC = HealthNutHabitsVC.init(nibName: "HealthNutHabitsVC", bundle: nil)
      healthNutHabitsVC.modalPresentationStyle = .fullScreen
      self.present(healthNutHabitsVC, animated: true, completion: nil)
    }
    
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)

    }
}
