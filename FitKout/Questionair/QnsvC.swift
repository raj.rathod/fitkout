//
//  QnsvC.swift
//  FitKout
//
//  Created by HT-Admin on 14/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class QnsvC: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    
    
    @IBOutlet weak var tableQns: UITableView!
    
     var habitsArray = ["Sub multi input text question 1","Sub multi input text question 2","Sub multi input text question 2","Sub multi input text question 3" ,"Sub multi input text question 4"]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        hideKeyboardWhenTappedAround()

        
        tableQns.register(UINib(nibName: "QnsCell", bundle: nil), forCellReuseIdentifier: "QnsCell")
        tableQns.tableFooterView = UIView()
        tableQns.separatorStyle = .none
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
          return habitsArray.count
           
         }
         
         func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
             let cell = tableView.dequeueReusableCell(withIdentifier: "QnsCell", for: indexPath) as! QnsCell
          
            cell.lblQns.text = habitsArray[indexPath.row]
            cell.tfQns.layer.borderWidth = 0.5
            cell.tfQns.layer.borderColor = UIColor.lightGray.cgColor
           
             return cell
         }
         
         
         func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
             return 90
             
         }
         
         func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
             
         }
    
    
    @IBAction func btnPrevClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)

    }
    @IBAction func btnNextClicked(_ sender: UIButton) {
        let foodPrefVC = FoodPrefVC.init(nibName: "FoodPrefVC", bundle: nil)
        foodPrefVC.modalPresentationStyle = .fullScreen
       self.present(foodPrefVC, animated: true, completion: nil)
    }
    
    
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)

    }
    
}
