//
//  ActivityStatusVC.swift
//  FitKout
//
//  Created by HT-Admin on 23/10/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

protocol ActivityStatusVCDelegate
{
    func selectedActivityType( selected : String , activityId :String)
}

class ActivityStatusVC: UIViewController ,UITableViewDataSource,UITableViewDelegate {
    
    var activityArray = [Activity]()

    var delegate : ActivityStatusVCDelegate?
   var arr = ["Little or no exercise","Light exercise / Sports 3-5 days a week","Moderate exercise / Sports 6-7 days a week","PCOS","Intense exercise / Sports and physical job/2x training"]
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getActivities()

        tableView.register(UINib(nibName: "BulletsCell", bundle: nil), forCellReuseIdentifier: "BulletsCell")
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return activityArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BulletsCell", for: indexPath) as! BulletsCell
        
        let activityModel: Activity
        activityModel = activityArray [indexPath.row]
        cell.lblName.text = activityModel.activity_details
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let activityModel: Activity
        activityModel = activityArray [indexPath.row]
        delegate?.selectedActivityType(selected: activityModel.activity_details, activityId: activityModel.id)
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func getActivities() -> Void {
         getDataFromUrl(url: "https://fitkout.com.cs-mum-27.bluehostwebservers.com/Fitkout/index.php/api/ApiCaller/get_activities", completion: { response in
                   print(response)
            var acArray = [Activity]()

         for anItem in response as! [Dictionary<String, Any>] {
                                
         if let netWorkObj = Activity(dictionary: anItem as [String : Any]) {
          print(netWorkObj)
          acArray.append(netWorkObj)
            }
            self.activityArray = acArray
           DispatchQueue.main.async {
                  self.tableView.reloadData()
                      }
                }
               })
    }

}


class Activity {
    var AthleteKcalValueFactor:String = ""
    var DefaultDurationInMinutes:String = ""
    var DurationInMinutes: String = ""
    var GeneralKCalValue: String = ""
    var activity_details:String = ""
    var activity_type: String = ""
    var id: String = ""
    
    
    init?(dictionary:[String:Any]) {
        guard let AthleteKcalValueFactor = dictionary["AthleteKcalValueFactor"],
            let DefaultDurationInMinutes = dictionary["DefaultDurationInMinutes"],
            let DurationInMinutes = dictionary["DurationInMinutes"],
            let GeneralKCalValue = dictionary["GeneralKCalValue"],
            let activity_details = dictionary["activity_details"],
            let activity_type = dictionary["activity_type"],
            let id = dictionary["id"]
           
         else{ return}
        
        self.AthleteKcalValueFactor = AthleteKcalValueFactor as! String
        self.DefaultDurationInMinutes = DefaultDurationInMinutes as! String
        self.DurationInMinutes = DurationInMinutes as! String
        self.GeneralKCalValue = GeneralKCalValue as! String
        self.activity_details = activity_details as! String
        self.activity_type = activity_type as! String
        self.id = id as! String
        
    }
}
