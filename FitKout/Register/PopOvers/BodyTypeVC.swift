//
//  BodyTypeVC.swift
//  FitKout
//
//  Created by HT-Admin on 23/10/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

protocol BodyTypeVCDelegate
{
    func selectedBodyType( selected : String , bodyTypeID : String)
}

class BodyTypeVC: UIViewController  ,UITableViewDataSource,UITableViewDelegate{
       var bodyTypeArray = [BodyType]()
       var delegate : BodyTypeVCDelegate?
    
      @IBOutlet weak var tableView: UITableView!
      
      override func viewDidLoad() {
          super.viewDidLoad()

          // Do any additional setup after loading the view.
          
          tableView.register(UINib(nibName: "BodyTypeCell", bundle: nil), forCellReuseIdentifier: "BodyTypeCell")
        
        getBodyType()
      }


      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          // return arr.count
          return bodyTypeArray.count
      }
      
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: "BodyTypeCell", for: indexPath) as! BodyTypeCell
        
        let btModel: BodyType
        btModel = bodyTypeArray[indexPath.row]
        
        cell.lblTypeName.text = btModel.TypeTitle
        cell.imgBody.loadImageUsingCache(withUrl : btModel.TypeImg)
          return cell
      }
      
      func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let btModel: BodyType
        btModel = bodyTypeArray[indexPath.row]
        delegate?.selectedBodyType(selected: btModel.TypeTitle, bodyTypeID: btModel.TypeId)
          self.dismiss(animated: true, completion: nil)
      }
      
      func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
          return 120
      }

    @IBAction func btnCloseClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func getBodyType() -> Void {
        
      //  self.view.activityStartAnimating(activityColor: #colorLiteral(red: 0.003921568627, green: 0.7764705882, blue: 0.8470588235, alpha: 1), backgroundColor: .white)
        
         getDataFromUrl(url: "https://fitkout.com.cs-mum-27.bluehostwebservers.com/Fitkout/index.php/api/ApiCaller/getBodyDetails", completion: { response in
                   print(response)
            
            
            var btArray = [BodyType]()
            
            let bodyType = response["BodyType"]

         for anItem in bodyType as! [Dictionary<String, Any>] {

         if let netWorkObj = BodyType(dictionary: anItem as [String : Any]) {
          print(netWorkObj)
          btArray.append(netWorkObj)
            }
            self.bodyTypeArray = btArray
           DispatchQueue.main.async {
                  self.tableView.reloadData()
         //   self.view.activityStopAnimating()
                      }
                }
            
               })
    }
    
}


class BodyType {
    var TypeId:String = ""
    var TypeImg: String = ""
    var TypeTitle: String = ""
    
    init?(dictionary:[String:Any]) {
        guard let TypeId = dictionary["TypeId"],
            let TypeImg = dictionary["TypeImg"],
            let TypeTitle = dictionary["TypeTitle"]
           
         else{ return}
        
        self.TypeId = TypeId as! String
        self.TypeImg = TypeImg as! String
        self.TypeTitle = TypeTitle as! String
        
    }
}
