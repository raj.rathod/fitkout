//
//  MedicalConditionsVC.swift
//  FitKout
//
//  Created by HT-Admin on 23/10/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

protocol MedicalConditionsVCDelegate
{
    func selectedMedicalType( selected : String)
}

class MedicalConditionsVC: UIViewController ,UITableViewDataSource,UITableViewDelegate {
    
    var mCondtionsArray = [Medicalconditions]()
    var delegate : MedicalConditionsVCDelegate?
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getMedicalConditions()
        
        tableView.register(UINib(nibName: "BulletsCell", bundle: nil), forCellReuseIdentifier: "BulletsCell")
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mCondtionsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BulletsCell", for: indexPath) as! BulletsCell
        
        let mcModel: Medicalconditions
        mcModel = mCondtionsArray [indexPath.row]
        cell.lblName.text = mcModel.MedicalCondition
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismiss(animated: true, completion: nil)
        let mcModel: Medicalconditions
        mcModel = mCondtionsArray [indexPath.row]
        delegate?.selectedMedicalType(selected: mcModel.MedicalCondition)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    
    
    func getMedicalConditions() -> Void {
      //  self.view.activityStartAnimating(activityColor: #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1), backgroundColor: UIColor.black.withAlphaComponent(0.3))
           getDataFromUrl(url: "https://fitkout.com.cs-mum-27.bluehostwebservers.com/Fitkout/index.php/api/ApiCaller/get_medical_conditions") { (response) in
               print(response)
            var mcArray = [Medicalconditions]()

             for anItem in response as! [Dictionary<String, Any>] {
                                
             if let netWorkObj = Medicalconditions(dictionary: anItem as [String : Any]) {
               print(netWorkObj)
              mcArray.append(netWorkObj)
                      }
           self.mCondtionsArray = mcArray
              DispatchQueue.main.async {
                      self.tableView.reloadData()
                       }
             }
            
           }
       }


}


class Medicalconditions {
    var MedicalCondition:String = ""
    var MedicalConditionId: String = ""
    
    
    init?(dictionary:[String:Any]) {
        guard let MedicalCondition = dictionary["MedicalCondition"],
            let MedicalConditionId = dictionary["MedicalConditionId"]
           
         else{ return}
        
        self.MedicalCondition = MedicalCondition as! String
        self.MedicalConditionId = MedicalConditionId as! String
        
    }
}
