//
//  CoutnriesVC.swift
//  FitKout
//
//  Created by HT-Admin on 05/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit


protocol CoutnriesVCDelegate
{
    func selectedCountry( slctdCountry : String , countryId : String)
}

class CoutnriesVC: UIViewController,UITableViewDataSource,UITableViewDelegate{
    
    var countryArray = [Country]()

    @IBOutlet weak var tableActivity: UITableView!

    var delegate : CoutnriesVCDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
         tableActivity.register(UINib(nibName: "BulletsCell", bundle: nil), forCellReuseIdentifier: "BulletsCell")
        
        getCountriesDataFromUrl()
        
        
    }


  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // return arr.count
        return countryArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BulletsCell", for: indexPath) as! BulletsCell
        
        let countryModel: Country
        countryModel = countryArray [indexPath.row]
        cell.lblName.text = countryModel.CountryName
       
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismiss(animated: true, completion: nil)
        let countryModel: Country
        countryModel = countryArray [indexPath.row]
        delegate?.selectedCountry(slctdCountry: countryModel.CountryName, countryId: countryModel.CountryId )
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }

    
    func getCountriesDataFromUrl() -> Void {
           getDataFromUrl(url: "https://fitkout.com.cs-mum-27.bluehostwebservers.com/Fitkout/index.php/api/LocationApiCaller/get_countries", completion: { response in
                        print(response)
                 var cArray = [Country]()

                 for anItem in response as! [Dictionary<String, Any>] {
                     
                     if let netWorkObj = Country(dictionary: anItem as [String : Any]) {
                         
                         print(netWorkObj)
                 cArray.append(netWorkObj)
                  }
                  self.countryArray = cArray
                     print(cArray)
                  DispatchQueue.main.async {
                       self.tableActivity.reloadData()
                        }
                 }
       })
    }
    

   

}


class Country {
    var CountryId:String = ""
    var CountryName: String = ""
    var CountryVisible: String = ""
    
    
    init?(dictionary:[String:Any]) {
        guard let CountryId = dictionary["CountryId"],
            let CountryName = dictionary["CountryName"],
            let CountryVisible = dictionary["CountryVisible"]
           
         else{ return}
        
        self.CountryId = CountryId as! String
        self.CountryName = CountryName as! String
        self.CountryVisible = CountryVisible as! String
        
    }
}

