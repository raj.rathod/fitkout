//
//  BodyTypeCell.swift
//  FitKout
//
//  Created by HT-Admin on 23/10/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class BodyTypeCell: UITableViewCell {
    
    @IBOutlet weak var imgBody: UIImageView!
    @IBOutlet weak var lblTypeName: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
