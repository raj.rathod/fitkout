//
//  CitiesVC.swift
//  FitKout
//
//  Created by HT-Admin on 06/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit
protocol CitiesVCDelegate
{
    func selectedCity(selected : String , selectedId: String)
}

class CitiesVC: UIViewController ,UITableViewDataSource,UITableViewDelegate {
    
    var delegate : CitiesVCDelegate?

    var stateId = ""
    
    @IBOutlet weak var tableCity: UITableView!
    
    var cityArray = [City]()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getCities(stateId: stateId)
         tableCity.register(UINib(nibName: "BulletsCell", bundle: nil), forCellReuseIdentifier: "BulletsCell")
    }

    
      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return cityArray.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BulletsCell", for: indexPath) as! BulletsCell
                let cityModel: City
               cityModel = cityArray [indexPath.row]
               cell.lblName.text = cityModel.CityName
            return cell
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            self.dismiss(animated: true, completion: nil)
            let cityModel: City
            cityModel = cityArray [indexPath.row]
            delegate?.selectedCity(selected: cityModel.CityName, selectedId: cityModel.CityId)
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 50
        }
      
      
      func getCities(stateId : String) -> Void {
              getDataFromUrl(url: "https://fitkout.com.cs-mum-27.bluehostwebservers.com/Fitkout/index.php/api/LocationApiCaller/get_cities?StateId=\(stateId)", completion: { response in
                        print(response)
                  
                  var ciArray = [City]()
                  for anItem in response as! [Dictionary<String, Any>] {
                      
                      if let netWorkObj = City(dictionary: anItem as [String : Any]) {
                  ciArray.append(netWorkObj)
                          
                   }
                   self.cityArray = ciArray
                   DispatchQueue.main.async {
                        self.tableCity.reloadData()
                         }
                  }

                    })
         }
   
}


class City {
    var CityId:String = ""
    var StateId:String = ""
    var CityName: String = ""
    var CityVisible: String = ""
    
    
    init?(dictionary:[String:Any]) {
        guard let CityId = dictionary["CityId"],
            let StateId = dictionary["StateId"],
            let CityName = dictionary["CityName"],
            let CityVisible = dictionary["CityVisible"]
           
         else{ return}
        
        self.CityId = CityId as! String
        self.StateId = StateId as! String
        self.CityName = CityName as! String
        self.CityVisible = CityVisible as! String
        
    }
}
