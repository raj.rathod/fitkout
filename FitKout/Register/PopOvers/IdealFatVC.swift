//
//  IdealFatVC.swift
//  FitKout
//
//  Created by HT-Admin on 23/10/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

protocol IdealFatVCDelegate
{
    func selectedIdealFatType( selected : String , bodyFatId : String)
}

class IdealFatVC: UIViewController  ,UITableViewDataSource,UITableViewDelegate {
    
    var gender = ""
    
    var delegate : IdealFatVCDelegate?

    var idealFatArray = [IdealFat]()
    
    var arr = ["Essential fat(3-5)%","Athletes(6-13)%","Fitnesss(14-17)%","Average(18-24)%"]
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tableView.register(UINib(nibName: "BulletsCell", bundle: nil), forCellReuseIdentifier: "BulletsCell")
        
        getBodyType()
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // return arr.count
        return idealFatArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BulletsCell", for: indexPath) as! BulletsCell
        
        let btModel: IdealFat
        btModel = idealFatArray[indexPath.row]
        cell.lblName.text = btModel.Title + "(" + "\(btModel.LowerRange) - \(btModel.UpperRange)" + ")%"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let btModel: IdealFat
        btModel = idealFatArray[indexPath.row]
        delegate?.selectedIdealFatType(selected: btModel.Title, bodyFatId: btModel.Id)
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }

    func getBodyType() -> Void {
        
         getDataFromUrl(url: "https://fitkout.com.cs-mum-27.bluehostwebservers.com/Fitkout/index.php/api/ApiCaller/getBodyDetails", completion: { response in
                   print(response)
            var ifArray = [IdealFat]()
            print(self.gender)
            
            if self.gender == "Male"
            {
                self.gender = "Men"
            }else
            {
              self.gender = "Women"
            }
        let genderRefType = response[self.gender]
            

         for anItem in genderRefType as! [Dictionary<String, Any>] {

         if let netWorkObj = IdealFat(dictionary: anItem as [String : Any]) {
          print(netWorkObj)
          ifArray.append(netWorkObj)
            }
            self.idealFatArray = ifArray
           DispatchQueue.main.async {
                  self.tableView.reloadData()
                      }
                }
            
               })
    }
}


class IdealFat {
    var Id:String = ""
    var Gender:String = ""
    var LowerRange:String = ""
    var UpperRange:String = ""
    var Title: String = ""
    
    init?(dictionary:[String:Any]) {
        guard let Id = dictionary["Id"],
            let Gender = dictionary["Gender"],
            let LowerRange = dictionary["LowerRange"],
            let UpperRange = dictionary["UpperRange"],
            let Title = dictionary["Title"]
           
         else{ return}
        self.Id = Id as! String
        self.Gender = Gender as! String
        self.LowerRange = LowerRange as! String
        self.UpperRange = UpperRange as! String
        self.Title = Title as! String
        
    }
}
