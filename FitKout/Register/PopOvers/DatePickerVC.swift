//
//  DatePickerVC.swift
//  FitKout
//
//  Created by HT-Admin on 23/10/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

protocol DatePickerVCDelegate
{
    func selectedDate( selected : String)
}


class DatePickerVC: UIViewController {
    
    var delegate : DatePickerVCDelegate?

    var pickedDate = ""
    
    @IBOutlet weak var navBarDate: UINavigationBar!
    @IBOutlet weak var datePickerTitle: UINavigationItem!
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // for horizontal transform
      //  datePicker.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2.0)
        
        self.datePickerTitle.title = "Select Date"
        
        let formatter = DateFormatter()
        formatter.calendar = datePicker.calendar
        formatter.dateStyle = .medium
        formatter.timeStyle = .medium
        formatter.dateFormat = "yyyy-MM-dd"
        pickedDate = formatter.string(from: datePicker.date)
        
        let calendar = Calendar(identifier: .gregorian)

        let currentDate = Date()
        var components = DateComponents()
        components.calendar = calendar

        components.year = -18
        components.month = 12
        let maxDate = calendar.date(byAdding: components, to: currentDate)!

        components.year = -150
        let minDate = calendar.date(byAdding: components, to: currentDate)!

        datePicker.minimumDate = minDate
        datePicker.maximumDate = currentDate
    }

    @IBAction func btnCancelClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnOkClciked(_ sender: UIButton) {
        delegate?.selectedDate(selected: pickedDate)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func datePickerChanged(_ sender: UIDatePicker) {
        print("print \(sender.date)")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        let somedateString = dateFormatter.string(from: sender.date)
        pickedDate = somedateString
        print(somedateString)
    }
    
}
