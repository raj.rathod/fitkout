//
//  StatesVC.swift
//  FitKout
//
//  Created by HT-Admin on 06/11/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

protocol StatesVCDelegate
{
    func selectedState( selectedState : String , stateID : String)
}


class StatesVC: UIViewController ,UITableViewDataSource,UITableViewDelegate{

     var countryId = ""
    var stateArray = [State]()

    var delegate : StatesVCDelegate?

    @IBOutlet weak var tableState: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableState.register(UINib(nibName: "BulletsCell", bundle: nil), forCellReuseIdentifier: "BulletsCell")
        
    getStates(countryId: countryId)

    }

   
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return stateArray.count
      }
      
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: "BulletsCell", for: indexPath) as! BulletsCell
              let stateModel: State
             stateModel = stateArray [indexPath.row]
             cell.lblName.text = stateModel.StateName
       
         
          return cell
      }
      
      func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismiss(animated: true, completion: nil)
          let stateModel: State
          stateModel = stateArray [indexPath.row]
        delegate?.selectedState(selectedState: stateModel.StateName , stateID : stateModel.StateId)
      }
      
      func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
          return 50
      }
    
    
    func getStates(countryId : String) -> Void {
            getDataFromUrl(url: "https://fitkout.com.cs-mum-27.bluehostwebservers.com/Fitkout/index.php/api/LocationApiCaller/get_states?CountryId=\(countryId)", completion: { response in
                      print(response)
                
                var sArray = [State]()
                for anItem in response as! [Dictionary<String, Any>] {
                    
                    if let netWorkObj = State(dictionary: anItem as [String : Any]) {
                sArray.append(netWorkObj)
                        
                 }
                 self.stateArray = sArray
                 DispatchQueue.main.async {
                      self.tableState.reloadData()
                       }
                }

                  })
       }
   
}


class State {
    var CountryId:String = ""
    var StateId:String = ""
    var StateName: String = ""
    var StateVisible: String = ""
    
    
    init?(dictionary:[String:Any]) {
        guard let CountryId = dictionary["CountryId"],
            let StateId = dictionary["StateId"],
            let StateName = dictionary["StateName"],
            let StateVisible = dictionary["StateVisible"]
           
         else{ return}
        
        self.CountryId = CountryId as! String
        self.StateId = StateId as! String
        self.StateName = StateName as! String
        self.StateVisible = StateVisible as! String
        
    }
}
