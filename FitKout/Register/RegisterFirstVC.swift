//
//  RegisterFirstVC.swift
//  FitKout
//
//  Created by HT-Admin on 21/10/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class RegisterFirstVC: UIViewController  , UITextFieldDelegate{
    
    var userType = ""
    
    @IBOutlet weak var btnUser: UIButton!
    @IBOutlet weak var btnTrainer: UIButton!
    @IBOutlet weak var btnNutritionist: UIButton!
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var tfEmailId: UITextField!
    @IBOutlet weak var tfPass: UITextField!
    @IBOutlet weak var tfVerifyPass: UITextField!
    @IBOutlet weak var tfMobileNum: UITextField!
    
    var arrButtons:[UIButton] = []
    
    var iconClick = true
    let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
    let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 24, height: 24))

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tfMobileNum.delegate = self
        tfEmailId.delegate = self

        // bacground image setting
        setBackGroundImage()
        TransparentnavigationBar(navBar:self.navBar)
        hideKeyboardWhenTappedAround()
        
        arrButtons.append(btnUser)
        arrButtons.append(btnTrainer)
        arrButtons.append(btnNutritionist)
        
        tfPass.rightViewMode = UITextField.ViewMode.always
       
        iconClick = true
        let image = #imageLiteral(resourceName: "hide_pass")
        imageView.image = image
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imgViewTapped(tapGestureRecognizer:)))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(tapGestureRecognizer)
        tfPass.rightView = imageView
       
        
        
        tfVerifyPass.rightViewMode = UITextField.ViewMode.always
        imgView.image = image
        let tpGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imgVwTapped(tapGestureRecognizer:)))
        imgView.isUserInteractionEnabled = true
        imgView.addGestureRecognizer(tpGestureRecognizer)
        tfVerifyPass.rightView = imgView
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
       
    }
    
  
    
    @objc func imgVwTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        if(iconClick == true) {
            tfVerifyPass.isSecureTextEntry = false
            imgView.image = #imageLiteral(resourceName: "show_pass")
        } else {
            tfVerifyPass.isSecureTextEntry = true
            imgView.image = #imageLiteral(resourceName: "hide_pass")
        }
        
        iconClick = !iconClick
    }
    @objc func imgViewTapped(tapGestureRecognizer: UITapGestureRecognizer)
       {
           if(iconClick == true) {
               tfPass.isSecureTextEntry = false
               imageView.image = #imageLiteral(resourceName: "show_pass")
           } else {
               tfPass.isSecureTextEntry = true
               imageView.image = #imageLiteral(resourceName: "hide_pass")
           }
           
           iconClick = !iconClick
       }


    @IBAction func btnJoinNowClicked(_ sender: UIButton) {
        tfMobileNum.text = tfMobileNum.text?.replacingOccurrences(of: " ", with: "")
        
        let passString: NSString = tfPass.text! as NSString
        let vPassString: NSString = tfVerifyPass.text! as NSString
        let mobileNum: NSString = tfMobileNum.text! as NSString

       if !(tfEmailId.text!.isValidEmail()){
        self.showAlert(title: "Invalid email!", msg: "Please enter valid email")
        
        } else if (passString.length < 8 || vPassString.length < 8)
        {
            self.showAlert(title: "Invalid password!", msg: "Password should not less than 8 charecters")

        } else if (passString != vPassString)
        {
            self.showAlert(title: "Password Mismatch!", msg: "Password should match")
        }
        else if (mobileNum.length < 10)
        {
            self.showAlert(title: "Invalid Mobile Number!", msg: "Please enter valid mobile number")

        } else if (userType == "")
        {
            self.showAlert(title: "Please select user", msg: "")

        }
       else
       {
        RegisterForLoginDetails(userType: userType , userEmail: tfEmailId.text!, password: tfPass.text!, mobileNo: tfMobileNum.text!)
        
        }
   
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
       if textField == tfEmailId {
        emailCheck(email: tfEmailId.text!)
                   }
       else if textField == tfMobileNum
               {
                phoneCheck(phone: tfMobileNum.text!)
                }
    }
    
  func textFieldDidBeginEditing(_ textField: UITextField) {
   if textField == tfEmailId {
    self.tfEmailId.withImage(direction: .Right, image:UIImage(), colorSeparator: UIColor.clear, colorBorder: UIColor.clear)               }
   else if textField == tfMobileNum
           {
self.tfMobileNum.withImage(direction: .Right, image: UIImage(), colorSeparator: UIColor.clear, colorBorder: UIColor.clear)
            
    }

        
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
      
        if textField == tfMobileNum
        {
        return range.location < 10
        }
        else
        {
            return range.location < 100
        }
    }
    
    @IBAction func backBtnClicked(_ sender: UIBarButtonItem) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func radiBtnClicked(_ sender: UIButton) {
      
        for getradioButton in arrButtons {
            getradioButton.setImage(UIImage(named: "unchecked.png"), for: .normal)
        }
        sender.setImage(UIImage(named: "checked.png"), for: .normal)
        userType = sender.titleLabel!.text!
        
        if userType == "User"
        {
            userType = "Normal"
        }
        
        print(userType)
    }
    
       @objc func keyboardWillShow(notification: NSNotification) {

           if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
               if self.view.frame.origin.y == 0 {
                 print(keyboardSize.height)
                self.view.frame.origin.y -= keyboardSize.height - 100
                
               }
           }
       }

       @objc func keyboardWillHide(notification: NSNotification) {
           if self.view.frame.origin.y != 0 {
               self.view.frame.origin.y = 0
           }
       }
  
    func RegisterForLoginDetails(userType:  String ,userEmail : String , password : String , mobileNo : String) -> Void {
        
          if Reachability.isConnectedToNetwork()
          {
            self.view.activityStartAnimating(activityColor: #colorLiteral(red: 0, green: 0.9914394021, blue: 1, alpha: 1), backgroundColor: UIColor.black.withAlphaComponent(0.3))

                       var msg = ""
                    
                    let headers = [
                                  "Authorization": "Bearer fitkout",
                                  "Content-Type": "application/json",
                                  "cache-control": "no-cache",
                                ]
                    
                    let params = ["UserEmailId":userEmail, "UserPassword":password,"UserPhone":mobileNo, "UserType":userType,"UserLoginType":userType] as Dictionary<String, String>
                    
                    var request = URLRequest(url: URL(string: "https://fitkout.com.cs-mum-27.bluehostwebservers.com/Fitkout/index.php/api/ApiCaller/register")!)
                    request.httpMethod = "POST"
                    request.allHTTPHeaderFields = headers
                    request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])

                    let session = URLSession.shared
                    let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
                     
                        print(response as Any)
                        
                        if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 201 {
                            
            //                guard let data = data else {
            //                   print(String(describing: error))
            //                   return
            //                 }
            //                 print(String(data: data, encoding: .utf8)!)
                            
                              do {
                                  if data != nil
                                  {
                                      let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                                      print(json)
                                    
                                     let loginDetails = json["LoginDetails"] as! Dictionary<String, AnyObject>
                                    let loginId = loginDetails["LoginId"] as! Int
                                    print(loginId)
                                    DispatchQueue.main.async {
                                        self.view.activityStopAnimating()
                                       let signUpVC = RegisterSecVC.init(nibName: "RegisterSecVC", bundle: nil)
                                        signUpVC.loginID = loginId
                                       signUpVC.modalPresentationStyle = .fullScreen
                                        self.present(signUpVC, animated: true, completion: nil)
                                    }
                                  }

                                   } catch {
                                     print("error")
                                    }
                            self.view.activityStopAnimating()
                            
                        }else
                        {
                            do {
                                self.view.activityStopAnimating()
                                if data != nil
                                {
                                    let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                                    print(json)
                                    msg = json["Msg"] as! String
                                    self.showAlert(title: msg , msg: "")
                                }

                                 } catch {
                                   print("error")
                                  }
                        }
                        
                        
                    })

                    task.resume()
            
          }
          else
          {
           self.showAlert(title: "Network Error!", msg: "Please Check Your Connectivity")
           }
        

    }

func phoneCheck(phone : String) -> Void {
    
    
    if Reachability.isConnectedToNetwork()
    {
          var msg = ""
           
           let headers = [
                         "Authorization": "Bearer fitkout",
                         "Content-Type": "application/json",
                         "cache-control": "no-cache",
                       ]
           let params = ["phone":phone] as Dictionary<String, String>
           
           var request = URLRequest(url: URL(string: "https://fitkout.com.cs-mum-27.bluehostwebservers.com/Fitkout/index.php/api/ApiCaller/check_phone_availability")!)
           request.httpMethod = "POST"
           request.allHTTPHeaderFields = headers
           request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])

           let session = URLSession.shared
           let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
               
               print(response as Any)
               
             if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200 {
               
               do {
                   if data != nil
                   {
                       let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                       print(json)
                   }

                    } catch {
                      print("error")
                     }
        

               }
               else
             {
               do {
                   let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                   print(json)
                   
                   if json["code"] as! String == "7" {
                    msg = json["Msg"] as! String
                    self.showAlert(title: msg , msg: "")
                   }

                      DispatchQueue.main.async {
                 self.tfMobileNum.becomeFirstResponder()
                      }
                   
                   } catch {
                     print("error")
                   }
                   
               }
                  
               
           })

           task.resume()
    }
    else
    {
     self.showAlert(title: "Network Error!", msg: "Please Check Your Connectivity")
     }
    

    }
    
    func emailCheck(email : String) -> Void {
        
          if Reachability.isConnectedToNetwork()
          {
                    var msg = ""
                 
                 let headers = [
                               "Authorization": "Bearer fitkout",
                               "Content-Type": "application/json",
                               "cache-control": "no-cache",
                             ]
                 let params = ["email":email] as Dictionary<String, String>
                 
                 var request = URLRequest(url: URL(string: "https://fitkout.com.cs-mum-27.bluehostwebservers.com/Fitkout/index.php/api/ApiCaller/check_email_availability")!)
                 request.httpMethod = "POST"
                 request.allHTTPHeaderFields = headers
                 request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])

                 let session = URLSession.shared
                 let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
                     
                     
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200 {
                          print(httpStatus.statusCode)
             
                  DispatchQueue.main.async {
             self.tfEmailId.withImage(direction: .Right, image: #imageLiteral(resourceName: "green_right"), colorSeparator: UIColor.clear, colorBorder: UIColor.clear)
                  }

                          }
                          else
                        {
                          do {
                              let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                              print(json)
                              
                              if json["code"] as! String == "6" {
                               msg = json["Msg"] as! String
                               self.showAlert(title: msg , msg: "")

                              }
                             
                             DispatchQueue.main.async {
                        self.tfEmailId.becomeFirstResponder()
                             }
                              
                              } catch {
                                print("error")
                              }
                              
                          }
                             
                 })

                 task.resume()
          }
          else
          {
           self.showAlert(title: "Network Error!", msg: "Please Check Your Connectivity")
           }
    }
    
}
