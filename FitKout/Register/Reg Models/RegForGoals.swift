//
//  RegForGoals.swift
//  FitKout
//
//  Created by HT-Admin on 15/01/20.
//  Copyright © 2020 HT-Admin. All rights reserved.
//

import Foundation


class RegForGoals : NSObject, NSCoding{

    var bMI : String!
    var bMIStatus : String!
    var days : Int!
    var endDate : String!
    var fGoalId : String!
    var goal : String!
    var iBW : String!
    var idealBodyRangeHigher : Int!
    var idealBodyRangeLower : Int!
    var intensity : String!
    var intensityValue : Float!
    var months : Int!
    var otherIntensities : [OtherIntensity]!
    var startDate : String!
    var targetWeight : Int!
    var targetWeightRangeHigher : String!
    var targetWeightRangeLower : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        bMI = dictionary["BMI"] as? String
        bMIStatus = dictionary["BMI_Status"] as? String
        days = dictionary["Days"] as? Int
        endDate = dictionary["EndDate"] as? String
        fGoalId = dictionary["FGoalId"] as? String
        goal = dictionary["Goal"] as? String
        iBW = dictionary["IBW"] as? String
        idealBodyRangeHigher = dictionary["IdealBodyRangeHigher"] as? Int
        idealBodyRangeLower = dictionary["IdealBodyRangeLower"] as? Int
        intensity = dictionary["Intensity"] as? String
        intensityValue = dictionary["IntensityValue"] as? Float
        months = dictionary["Months"] as? Int
        startDate = dictionary["StartDate"] as? String
        targetWeight = dictionary["TargetWeight"] as? Int
        targetWeightRangeHigher = dictionary["TargetWeightRangeHigher"] as? String
        targetWeightRangeLower = dictionary["TargetWeightRangeLower"] as? String
        otherIntensities = [OtherIntensity]()
        if let otherIntensitiesArray = dictionary["OtherIntensities"] as? [[String:Any]]{
            for dic in otherIntensitiesArray{
                let value = OtherIntensity(fromDictionary: dic)
                otherIntensities.append(value)
            }
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if bMI != nil{
            dictionary["BMI"] = bMI
        }
        if bMIStatus != nil{
            dictionary["BMI_Status"] = bMIStatus
        }
        if days != nil{
            dictionary["Days"] = days
        }
        if endDate != nil{
            dictionary["EndDate"] = endDate
        }
        if fGoalId != nil{
            dictionary["FGoalId"] = fGoalId
        }
        if goal != nil{
            dictionary["Goal"] = goal
        }
        if iBW != nil{
            dictionary["IBW"] = iBW
        }
        if idealBodyRangeHigher != nil{
            dictionary["IdealBodyRangeHigher"] = idealBodyRangeHigher
        }
        if idealBodyRangeLower != nil{
            dictionary["IdealBodyRangeLower"] = idealBodyRangeLower
        }
        if intensity != nil{
            dictionary["Intensity"] = intensity
        }
        if intensityValue != nil{
            dictionary["IntensityValue"] = intensityValue
        }
        if months != nil{
            dictionary["Months"] = months
        }
        if startDate != nil{
            dictionary["StartDate"] = startDate
        }
        if targetWeight != nil{
            dictionary["TargetWeight"] = targetWeight
        }
        if targetWeightRangeHigher != nil{
            dictionary["TargetWeightRangeHigher"] = targetWeightRangeHigher
        }
        if targetWeightRangeLower != nil{
            dictionary["TargetWeightRangeLower"] = targetWeightRangeLower
        }
        if otherIntensities != nil{
            var dictionaryElements = [[String:Any]]()
            for otherIntensitiesElement in otherIntensities {
                dictionaryElements.append(otherIntensitiesElement.toDictionary())
            }
            dictionary["otherIntensities"] = dictionaryElements
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        bMI = aDecoder.decodeObject(forKey: "BMI") as? String
        bMIStatus = aDecoder.decodeObject(forKey: "BMI_Status") as? String
        days = aDecoder.decodeObject(forKey: "Days") as? Int
        endDate = aDecoder.decodeObject(forKey: "EndDate") as? String
        fGoalId = aDecoder.decodeObject(forKey: "FGoalId") as? String
        goal = aDecoder.decodeObject(forKey: "Goal") as? String
        iBW = aDecoder.decodeObject(forKey: "IBW") as? String
        idealBodyRangeHigher = aDecoder.decodeObject(forKey: "IdealBodyRangeHigher") as? Int
        idealBodyRangeLower = aDecoder.decodeObject(forKey: "IdealBodyRangeLower") as? Int
        intensity = aDecoder.decodeObject(forKey: "Intensity") as? String
        intensityValue = aDecoder.decodeObject(forKey: "IntensityValue") as? Float
        months = aDecoder.decodeObject(forKey: "Months") as? Int
        otherIntensities = aDecoder.decodeObject(forKey: "OtherIntensities") as? [OtherIntensity]
        startDate = aDecoder.decodeObject(forKey: "StartDate") as? String
        targetWeight = aDecoder.decodeObject(forKey: "TargetWeight") as? Int
        targetWeightRangeHigher = aDecoder.decodeObject(forKey: "TargetWeightRangeHigher") as? String
        targetWeightRangeLower = aDecoder.decodeObject(forKey: "TargetWeightRangeLower") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if bMI != nil{
            aCoder.encode(bMI, forKey: "BMI")
        }
        if bMIStatus != nil{
            aCoder.encode(bMIStatus, forKey: "BMI_Status")
        }
        if days != nil{
            aCoder.encode(days, forKey: "Days")
        }
        if endDate != nil{
            aCoder.encode(endDate, forKey: "EndDate")
        }
        if fGoalId != nil{
            aCoder.encode(fGoalId, forKey: "FGoalId")
        }
        if goal != nil{
            aCoder.encode(goal, forKey: "Goal")
        }
        if iBW != nil{
            aCoder.encode(iBW, forKey: "IBW")
        }
        if idealBodyRangeHigher != nil{
            aCoder.encode(idealBodyRangeHigher, forKey: "IdealBodyRangeHigher")
        }
        if idealBodyRangeLower != nil{
            aCoder.encode(idealBodyRangeLower, forKey: "IdealBodyRangeLower")
        }
        if intensity != nil{
            aCoder.encode(intensity, forKey: "Intensity")
        }
        if intensityValue != nil{
            aCoder.encode(intensityValue, forKey: "IntensityValue")
        }
        if months != nil{
            aCoder.encode(months, forKey: "Months")
        }
        if otherIntensities != nil{
            aCoder.encode(otherIntensities, forKey: "OtherIntensities")
        }
        if startDate != nil{
            aCoder.encode(startDate, forKey: "StartDate")
        }
        if targetWeight != nil{
            aCoder.encode(targetWeight, forKey: "TargetWeight")
        }
        if targetWeightRangeHigher != nil{
            aCoder.encode(targetWeightRangeHigher, forKey: "TargetWeightRangeHigher")
        }
        if targetWeightRangeLower != nil{
            aCoder.encode(targetWeightRangeLower, forKey: "TargetWeightRangeLower")
        }
    }
}




class OtherIntensity : NSObject, NSCoding{

    var dailyCalories : String!
    var intensity : String!
    var intensityId : String!
    var intensityValuePerWeekInKg : String!
    var isEnabled : Int!
    var weeklyCalories : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        dailyCalories = dictionary["DailyCalories"] as? String
        intensity = dictionary["Intensity"] as? String
        intensityId = dictionary["IntensityId"] as? String
        intensityValuePerWeekInKg = dictionary["IntensityValuePerWeekInKg"] as? String
        isEnabled = dictionary["IsEnabled"] as? Int
        weeklyCalories = dictionary["WeeklyCalories"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if dailyCalories != nil{
            dictionary["DailyCalories"] = dailyCalories
        }
        if intensity != nil{
            dictionary["Intensity"] = intensity
        }
        if intensityId != nil{
            dictionary["IntensityId"] = intensityId
        }
        if intensityValuePerWeekInKg != nil{
            dictionary["IntensityValuePerWeekInKg"] = intensityValuePerWeekInKg
        }
        if isEnabled != nil{
            dictionary["IsEnabled"] = isEnabled
        }
        if weeklyCalories != nil{
            dictionary["WeeklyCalories"] = weeklyCalories
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        dailyCalories = aDecoder.decodeObject(forKey: "DailyCalories") as? String
        intensity = aDecoder.decodeObject(forKey: "Intensity") as? String
        intensityId = aDecoder.decodeObject(forKey: "IntensityId") as? String
        intensityValuePerWeekInKg = aDecoder.decodeObject(forKey: "IntensityValuePerWeekInKg") as? String
        isEnabled = aDecoder.decodeObject(forKey: "IsEnabled") as? Int
        weeklyCalories = aDecoder.decodeObject(forKey: "WeeklyCalories") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if dailyCalories != nil{
            aCoder.encode(dailyCalories, forKey: "DailyCalories")
        }
        if intensity != nil{
            aCoder.encode(intensity, forKey: "Intensity")
        }
        if intensityId != nil{
            aCoder.encode(intensityId, forKey: "IntensityId")
        }
        if intensityValuePerWeekInKg != nil{
            aCoder.encode(intensityValuePerWeekInKg, forKey: "IntensityValuePerWeekInKg")
        }
        if isEnabled != nil{
            aCoder.encode(isEnabled, forKey: "IsEnabled")
        }
        if weeklyCalories != nil{
            aCoder.encode(weeklyCalories, forKey: "WeeklyCalories")
        }
    }
}



