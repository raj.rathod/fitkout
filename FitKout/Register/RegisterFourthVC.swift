//
//  RegisterFourthVC.swift
//  FitKout
//
//  Created by HT-Admin on 21/10/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class RegisterFourthVC: UIViewController {
    
    var loginId = 0
    var goalId = ""
    var skipped = ""
    
    var userWt = 0
    
    
    @IBOutlet weak var imgWtRef: UIImageView!
    @IBOutlet weak var lblBMI: UILabel!
    
    @IBOutlet weak var lblIdealBW: UILabel!
    
    @IBOutlet weak var lblBMIStatus: UILabel!
    
    @IBOutlet weak var lblTWRH: UILabel!
    @IBOutlet weak var lblTWRL: UILabel!
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var lblSliderValue: UILabel!
   
    @IBOutlet weak var lblQns: UILabel!
    @IBOutlet weak var lblGoalDuration: UILabel!
    var radioBtns:[UIButton] = []
    
    @IBOutlet weak var btnEasy: UIButton!
    @IBOutlet weak var btnMedium: UIButton!
    
    @IBOutlet weak var btnHard: UIButton!
    
    @IBOutlet weak var btnVeryHard: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        TransparentnavigationBar(navBar:self.navBar)
        
        radioBtns.append(btnEasy)
        radioBtns.append(btnMedium)
        radioBtns.append(btnHard)
        radioBtns.append(btnVeryHard)
        
        print(loginId)
        
        commitGoals(skipped: skipped)

    }

    @IBAction func backBtnClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func sliderChanged(_ sender: UISlider) {
        let currentValue = Double(sender.value).truncate(places: 2)
        lblSliderValue.text = "\(currentValue) Kg"
    }
    
    @IBAction func iCommitBtnClicked(_ sender: UIButton) {
        let welcomeVC = WelcomeVC.init(nibName: "WelcomeVC", bundle: nil)
        welcomeVC.modalPresentationStyle = .fullScreen
        self.present(welcomeVC, animated: true, completion: nil)
    }
    
    @IBAction func btnTermsClicked(_ sender: UIButton) {
        
        let termsVC = TermsAndConditionsVC.init(nibName: "TermsAndConditionsVC", bundle: nil)
        termsVC.modalPresentationStyle = .fullScreen
        self.present(termsVC, animated: true, completion: nil)
        
    }
    
    @IBAction func btnRadioClicked(_ sender: UIButton) {
        
        for radioButton in radioBtns {
            radioButton.setImage(UIImage(named: "grayUnchecked.png"), for: .normal)
        }
        sender.setImage(UIImage(named: "checked.png"), for: .normal)
        
    }
    
    func commitGoals(skipped : String) -> Void {
        
        if Reachability.isConnectedToNetwork()
        {
                 self.view.activityStartAnimating(activityColor: #colorLiteral(red: 0, green: 0.9914394021, blue: 1, alpha: 1), backgroundColor: UIColor.black.withAlphaComponent(0.3))
                 
                 let headers = [
                               "Authorization": "Bearer fitkout",
                               "Content-Type": "application/json",
                               "cache-control": "no-cache",
                               ]
                 
                 let params : Dictionary<String, AnyObject>?

                 if skipped == "1"
                 {
                     params = ["LoginId":loginId] as Dictionary<String, AnyObject>
                 }else
                 {
                      params = ["LoginId":loginId, "FGoalId":goalId] as Dictionary<String, AnyObject>
                 }
                 

                 var request = URLRequest(url: URL(string: "https://fitkout.com.cs-mum-27.bluehostwebservers.com/Fitkout/index.php/api/ExerciseApi/toUpdateGoalPage")!)
                 request.httpMethod = "POST"
                 request.allHTTPHeaderFields = headers
                 request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])

                 let session = URLSession.shared
                 let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
                     
                     print(response as Any)
                     do {
                           let json = try JSONSerialization.jsonObject(with: data!) as!  NSDictionary
                           print(json)
                         
                         var mcArray = [RegForGoals]()
                         let netWorkObj = RegForGoals(fromDictionary: json as! [String : Any] )
                         mcArray.append(netWorkObj)
                         
                         let iwl = String(netWorkObj.idealBodyRangeLower)
                         let iwh = String(netWorkObj.idealBodyRangeHigher)
                         
                         let goalWeight = netWorkObj.targetWeight - self.userWt
                         
                         let btnEasyTitle = netWorkObj.otherIntensities[0].intensity + "(" + netWorkObj.otherIntensities[0].intensityValuePerWeekInKg + "kg)"
                         let btnMediumTitle = netWorkObj.otherIntensities[1].intensity + "(" + netWorkObj.otherIntensities[1].intensityValuePerWeekInKg + "kg)"
                         let btnHardTitle = netWorkObj.otherIntensities[2].intensity + "(" + netWorkObj.otherIntensities[2].intensityValuePerWeekInKg + "kg)"
                         let btnVeryHardTitle = netWorkObj.otherIntensities[3].intensity + "(" + netWorkObj.otherIntensities[3].intensityValuePerWeekInKg + "kg)"

                         
                         DispatchQueue.main.async {
                             self.lblBMI.text = netWorkObj.bMI
                             self.lblBMIStatus.text = netWorkObj.bMIStatus
                             self.lblTWRL.text = netWorkObj.targetWeightRangeLower
                             self.lblTWRH.text = netWorkObj.targetWeightRangeHigher
                             self.lblIdealBW.text = iwl + "-" + iwh + "kg"
                             self.lblBMI.text = netWorkObj.bMI
                             self.lblQns.text = "HOW QUICKLY DO YOU WANT TO GAIN " + String(goalWeight) + "KG ? (WEEKLY)"
                             self.btnEasy.setTitle(btnEasyTitle, for: .normal)
                             self.btnMedium.setTitle(btnMediumTitle, for: .normal)
                             self.btnHard.setTitle(btnHardTitle, for: .normal)
                             self.btnVeryHard.setTitle(btnVeryHardTitle, for: .normal)
                             if netWorkObj.bMIStatus == "OverWeight"
                             {
                                 self.imgWtRef.image = #imageLiteral(resourceName: "overweight")
                             }else if netWorkObj.bMIStatus == "UnderWeight"
                             {
                                 self.imgWtRef.image = #imageLiteral(resourceName: "thin")

                             }else
                             {
                                 self.imgWtRef.image = #imageLiteral(resourceName: "normal")
                             }
                             self.view.activityStopAnimating()
                         }
            
                           } catch {
                             print("error")
                           }
                     

                     
                 })

                 task.resume()
        }
        else
        {
         self.showAlert(title: "Network Error!", msg: "Please Check Your Connectivity")
         }
        

    }
    
    
}



