//
//  RegisterSecVC.swift
//  FitKout
//
//  Created by HT-Admin on 21/10/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class RegisterSecVC: UIViewController  , UITextFieldDelegate ,UIPopoverPresentationControllerDelegate , MedicalConditionsVCDelegate ,ActivityStatusVCDelegate , IdealFatVCDelegate,BodyTypeVCDelegate , DatePickerVCDelegate , CoutnriesVCDelegate , StatesVCDelegate , CitiesVCDelegate{
    
    
    var loginID = 0
    
    var genderStr = ""
    var fitnessStr = ""
    
    var countryIdStr = ""
    var stateIdStr = ""
    var cityIdStr = ""
    var activityIdStr = ""
    var bodyFatIdStr = ""
    var bodyTypeIdStr = ""

    var weightStr = ""
    
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var tfFullName: UITextField!
    @IBOutlet weak var tfDateOfBirth: UITextField!
    @IBOutlet weak var tfCountry: UITextField!
    @IBOutlet weak var tfState: UITextField!
    @IBOutlet weak var tfCity: UITextField!
    @IBOutlet weak var tfWeightKgs: UITextField!
    @IBOutlet weak var tfWeightGrams: UITextField!
    @IBOutlet weak var tfHeightFeets: UITextField!
    @IBOutlet weak var tfHeightInches: UITextField!
    @IBOutlet weak var tfMentleCondition: UITextField!
    @IBOutlet weak var tfActivityStatus: UITextField!
    @IBOutlet weak var tfActivityDuration: UITextField!
    @IBOutlet weak var tfBodyType: UITextField!
    @IBOutlet weak var tfBodyFat: UITextField!
    
    
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    
    @IBOutlet weak var btnGeneral: UIButton!
    @IBOutlet weak var btnAthlete: UIButton!
    
    var rGenderBtns:[UIButton] = []
    var rBodyTypeBtns:[UIButton] = []
    
    var kgsClick = true
    
    @IBOutlet weak var btnKgsToPounds: UIButton!
    
    
    
    override func viewDidLoad()  {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // bacground image setting
        setBackGroundImage()
        TransparentnavigationBar(navBar:self.navBar)
        hideKeyboardWhenTappedAround()
        
        rGenderBtns.append(btnMale)
        rGenderBtns.append(btnFemale)
        
        rBodyTypeBtns.append(btnGeneral)
        rBodyTypeBtns.append(btnAthlete)

        tfMentleCondition.delegate = self
        tfActivityStatus.delegate = self
        tfBodyType.delegate = self
        tfBodyFat.delegate = self
        tfDateOfBirth.delegate = self
        
        tfFullName.delegate = self
        tfWeightKgs.delegate = self
        tfWeightGrams.delegate = self
        tfHeightFeets.delegate = self
        tfHeightInches.delegate = self
        tfCountry.delegate = self
        tfState.delegate = self
        tfCity.delegate = self
        
        if let myImage = UIImage(named: "dropDown"){
            tfMentleCondition.withImage(direction: .Right, image: myImage, colorSeparator: UIColor.clear, colorBorder: UIColor.clear)
            tfActivityStatus.withImage(direction: .Right, image: myImage, colorSeparator: UIColor.clear, colorBorder: UIColor.clear)
            tfBodyType.withImage(direction: .Right, image: myImage, colorSeparator: UIColor.clear, colorBorder: UIColor.clear )
            tfBodyFat.withImage(direction: .Right, image: myImage, colorSeparator: UIColor.clear, colorBorder: UIColor.clear)
        }
        
        if let myImage = UIImage(named: "date_picker_icon"){
            tfDateOfBirth.withImage(direction: .Right, image: myImage, colorSeparator: UIColor.clear, colorBorder: UIColor.clear)
          
        }
        
        
        print(loginID)
        
    }
    
        func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
            return UIModalPresentationStyle.none
        }
    
   func textFieldDidBeginEditing(_ textField: UITextField) {
    
        if textField == tfMentleCondition {
            self.view.endEditing(true)

            tfMentleCondition.resignFirstResponder()
            
              if Reachability.isConnectedToNetwork()
              {
                let controller = MedicalConditionsVC()
                controller.delegate = self
                controller.preferredContentSize = CGSize(width: view.bounds.size.width,height: 394)
                controller.modalPresentationStyle = UIModalPresentationStyle.popover
                let popController = controller.popoverPresentationController
                popController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
                popController?.delegate = self
                popController?.sourceRect = CGRect(x: self.view.bounds.midX , y:self.view.bounds.midY , width: 0, height: 0 )
                popController?.sourceView = self.view;
                self.present(controller, animated: true, completion: nil)
              }
              else
              {
               self.showAlert(title: "Network Error!", msg: "Please Check Your Connectivity")
               }

            
        } else if textField == tfActivityStatus {
            self.view.endEditing(true)

            tfActivityStatus.resignFirstResponder()

            if Reachability.isConnectedToNetwork()
            {
              let controller = ActivityStatusVC()
              controller.delegate = self
              controller.preferredContentSize = CGSize(width: view.bounds.size.width,height: 294)
              controller.modalPresentationStyle = UIModalPresentationStyle.popover
              let popController = controller.popoverPresentationController
              popController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
              popController?.delegate = self
              popController?.sourceRect = CGRect(x: self.view.bounds.midX , y:self.view.bounds.midY , width: 0, height: 0 )
              popController?.sourceView =  view
              self.present(controller, animated: true, completion: nil)
            }
            else
            {
             self.showAlert(title: "Network Error!", msg: "Please Check Your Connectivity")
             }

            
        }else if textField == tfBodyType {
            self.view.endEditing(true)

            tfBodyType.resignFirstResponder()

            if Reachability.isConnectedToNetwork()
            {
              let controller = BodyTypeVC()
              controller.delegate = self
              controller.preferredContentSize = CGSize(width: view.bounds.size.width,height: 483)
              controller.modalPresentationStyle = UIModalPresentationStyle.popover
              let popController = controller.popoverPresentationController
              popController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
              popController?.delegate = self
              popController?.sourceRect = CGRect(x: self.view.bounds.midX , y:self.view.bounds.midY , width: 0, height: 0 )
              popController?.sourceView =  view
              self.present(controller, animated: true, completion: nil)
            }
            else
            {
             self.showAlert(title: "Network Error!", msg: "Please Check Your Connectivity")
             }

            
        }else if textField == tfBodyFat {
            self.view.endEditing(true)
            tfBodyFat.resignFirstResponder()

            if Reachability.isConnectedToNetwork()
            {
              let controller = IdealFatVC()
              controller.delegate = self
              controller.preferredContentSize = CGSize(width: view.bounds.size.width,height: 244)
              controller.modalPresentationStyle = UIModalPresentationStyle.popover
              let popController = controller.popoverPresentationController
              popController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
              popController?.delegate = self
              popController?.sourceRect = CGRect(x: self.view.bounds.midX , y:self.view.bounds.midY , width: 0, height: 0 )
              popController?.sourceView =  view
              
              if genderStr == "" {
                  self.showAlert(title: "Select Gender", msg: "")
              }else
              {
                  controller.gender = genderStr
                  self.present(controller, animated: true, completion: nil)
              }
            }
            else
            {
             self.showAlert(title: "Network Error!", msg: "Please Check Your Connectivity")
             }

            
        }else if textField == tfDateOfBirth {
            self.view.endEditing(true)

            tfDateOfBirth.resignFirstResponder()
            let controller = DatePickerVC()
            controller.delegate = self
            controller.preferredContentSize = CGSize(width: view.bounds.size.width,height: 374)
            controller.modalPresentationStyle = UIModalPresentationStyle.popover
            let popController = controller.popoverPresentationController
            popController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
            popController?.delegate = self
            popController?.sourceRect = CGRect(x: self.view.bounds.midX , y:self.view.bounds.midY , width: 0, height: 0 )
            popController?.sourceView =  view
            self.present(controller, animated: true, completion: nil)
            
        }else if textField == tfCountry {
            self.view.endEditing(true)
           tfCountry.resignFirstResponder()
            if Reachability.isConnectedToNetwork()
            {
                let controller = CoutnriesVC()
                controller.delegate = self
                controller.preferredContentSize = CGSize(width: view.bounds.size.width,height: 100)
                controller.modalPresentationStyle = UIModalPresentationStyle.popover
                let popController = controller.popoverPresentationController
                popController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
                popController?.delegate = self
                popController?.sourceRect = CGRect(x: self.view.bounds.midX , y:self.view.bounds.midY , width: 0, height: 0 )
                popController?.sourceView =  self.view
                self.present(controller, animated: true, completion: nil)
            }
            else
            {
             self.showAlert(title: "Network Error!", msg: "Please Check Your Connectivity")
             }

            
        }else if textField == tfState {
            self.view.endEditing(true)
            
            if Reachability.isConnectedToNetwork()
            {
                let controller = StatesVC()
                controller.delegate = self
                controller.countryId = countryIdStr
                controller.preferredContentSize = CGSize(width: view.bounds.size.width,height: 100)
                controller.modalPresentationStyle = UIModalPresentationStyle.popover
                let popController = controller.popoverPresentationController
                popController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
                popController?.delegate = self
                popController?.sourceRect = CGRect(x: self.view.bounds.midX , y:self.view.bounds.midY , width: 0, height: 0 )
                popController?.sourceView =  self.view
                self.present(controller, animated: true, completion: nil)
            }
            else
            {
             self.showAlert(title: "Network Error!", msg: "Please Check Your Connectivity")
             }

           tfState.resignFirstResponder()
        }else if textField == tfCity {
            self.view.endEditing(true)

            if Reachability.isConnectedToNetwork()
            {
                let controller = CitiesVC()
                controller.delegate = self
                controller.stateId = stateIdStr
                controller.preferredContentSize = CGSize(width: view.bounds.size.width,height: 100)
                controller.modalPresentationStyle = UIModalPresentationStyle.popover
                let popController = controller.popoverPresentationController
                popController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
                popController?.delegate = self
                popController?.sourceRect = CGRect(x: self.view.bounds.midX , y:self.view.bounds.midY , width: 0, height: 0 )
                popController?.sourceView =  self.view
                self.present(controller, animated: true, completion: nil)
            }
            else
            {
             self.showAlert(title: "Network Error!", msg: "Please Check Your Connectivity")
             }
            tfCity.resignFirstResponder()

        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
            if textField == tfMentleCondition {
                self.view.endEditing(true)
                tfMentleCondition.resignFirstResponder()
                
                  if Reachability.isConnectedToNetwork()
                  {
                    let controller = MedicalConditionsVC()
                    controller.delegate = self
                    controller.preferredContentSize = CGSize(width: view.bounds.size.width,height: 394)
                    controller.modalPresentationStyle = UIModalPresentationStyle.popover
                    let popController = controller.popoverPresentationController
                    popController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
                    popController?.delegate = self
                    popController?.sourceRect = CGRect(x: self.view.bounds.midX , y:self.view.bounds.midY , width: 0, height: 0 )
                    popController?.sourceView = self.view;
                    self.present(controller, animated: true, completion: nil)
                  }
                  else
                  {
                   self.showAlert(title: "Network Error!", msg: "Please Check Your Connectivity")
                   }

                
            } else if textField == tfActivityStatus {
                self.view.endEditing(true)
                tfActivityStatus.resignFirstResponder()

                if Reachability.isConnectedToNetwork()
                {
                  let controller = ActivityStatusVC()
                  controller.delegate = self
                  controller.preferredContentSize = CGSize(width: view.bounds.size.width,height: 294)
                  controller.modalPresentationStyle = UIModalPresentationStyle.popover
                  let popController = controller.popoverPresentationController
                  popController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
                  popController?.delegate = self
                  popController?.sourceRect = CGRect(x: self.view.bounds.midX , y:self.view.bounds.midY , width: 0, height: 0 )
                  popController?.sourceView =  view
                  self.present(controller, animated: true, completion: nil)
                }
                else
                {
                 self.showAlert(title: "Network Error!", msg: "Please Check Your Connectivity")
                 }

                
            }else if textField == tfBodyType {
                self.view.endEditing(true)
                tfBodyType.resignFirstResponder()

                if Reachability.isConnectedToNetwork()
                {
                  let controller = BodyTypeVC()
                  controller.delegate = self
                  controller.preferredContentSize = CGSize(width: view.bounds.size.width,height: 483)
                  controller.modalPresentationStyle = UIModalPresentationStyle.popover
                  let popController = controller.popoverPresentationController
                  popController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
                  popController?.delegate = self
                  popController?.sourceRect = CGRect(x: self.view.bounds.midX , y:self.view.bounds.midY , width: 0, height: 0 )
                  popController?.sourceView =  view
                  self.present(controller, animated: true, completion: nil)
                }
                else
                {
                 self.showAlert(title: "Network Error!", msg: "Please Check Your Connectivity")
                 }

                
            }else if textField == tfBodyFat {
                self.view.endEditing(true)
                tfBodyFat.resignFirstResponder()

                if Reachability.isConnectedToNetwork()
                {
                  let controller = IdealFatVC()
                  controller.delegate = self
                  controller.preferredContentSize = CGSize(width: view.bounds.size.width,height: 244)
                  controller.modalPresentationStyle = UIModalPresentationStyle.popover
                  let popController = controller.popoverPresentationController
                  popController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
                  popController?.delegate = self
                  popController?.sourceRect = CGRect(x: self.view.bounds.midX , y:self.view.bounds.midY , width: 0, height: 0 )
                  popController?.sourceView =  view
                  
                  if genderStr == "" {
                      self.showAlert(title: "Select Gender", msg: "")
                  }else
                  {
                      controller.gender = genderStr
                      self.present(controller, animated: true, completion: nil)
                  }
                }
                else
                {
                 self.showAlert(title: "Network Error!", msg: "Please Check Your Connectivity")
                 }

                
            }else if textField == tfDateOfBirth {
                self.view.endEditing(true)
                tfDateOfBirth.resignFirstResponder()
                let controller = DatePickerVC()
                controller.delegate = self
                controller.preferredContentSize = CGSize(width: view.bounds.size.width,height: 374)
                controller.modalPresentationStyle = UIModalPresentationStyle.popover
                let popController = controller.popoverPresentationController
                popController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
                popController?.delegate = self
                popController?.sourceRect = CGRect(x: self.view.bounds.midX , y:self.view.bounds.midY , width: 0, height: 0 )
                popController?.sourceView =  view
                self.present(controller, animated: true, completion: nil)
                
            }else if textField == tfCountry {
                self.view.endEditing(true)
               tfCountry.resignFirstResponder()
                if Reachability.isConnectedToNetwork()
                {
                    let controller = CoutnriesVC()
                    controller.delegate = self
                    controller.preferredContentSize = CGSize(width: view.bounds.size.width,height: 100)
                    controller.modalPresentationStyle = UIModalPresentationStyle.popover
                    let popController = controller.popoverPresentationController
                    popController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
                    popController?.delegate = self
                    popController?.sourceRect = CGRect(x: self.view.bounds.midX , y:self.view.bounds.midY , width: 0, height: 0 )
                    popController?.sourceView =  self.view
                    self.present(controller, animated: true, completion: nil)
                }
                else
                {
                 self.showAlert(title: "Network Error!", msg: "Please Check Your Connectivity")
                 }

                
            }else if textField == tfState {
                self.view.endEditing(true)
                if Reachability.isConnectedToNetwork()
                {
                    let controller = StatesVC()
                    controller.delegate = self
                    controller.countryId = countryIdStr
                    controller.preferredContentSize = CGSize(width: view.bounds.size.width,height: 100)
                    controller.modalPresentationStyle = UIModalPresentationStyle.popover
                    let popController = controller.popoverPresentationController
                    popController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
                    popController?.delegate = self
                    popController?.sourceRect = CGRect(x: self.view.bounds.midX , y:self.view.bounds.midY , width: 0, height: 0 )
                    popController?.sourceView =  self.view
                    self.present(controller, animated: true, completion: nil)
                }
                else
                {
                 self.showAlert(title: "Network Error!", msg: "Please Check Your Connectivity")
                 }

               tfState.resignFirstResponder()
            }else if textField == tfCity {
                self.view.endEditing(true)

                if Reachability.isConnectedToNetwork()
                {
                    let controller = CitiesVC()
                    controller.delegate = self
                    controller.stateId = stateIdStr
                    controller.preferredContentSize = CGSize(width: view.bounds.size.width,height: 100)
                    controller.modalPresentationStyle = UIModalPresentationStyle.popover
                    let popController = controller.popoverPresentationController
                    popController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
                    popController?.delegate = self
                    popController?.sourceRect = CGRect(x: self.view.bounds.midX , y:self.view.bounds.midY , width: 0, height: 0 )
                    popController?.sourceView =  self.view
                    self.present(controller, animated: true, completion: nil)
                }
                else
                {
                 self.showAlert(title: "Network Error!", msg: "Please Check Your Connectivity")
                 }
                tfCity.resignFirstResponder()

            }
        return true
        }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
    }

    
    
    func selectedCity(selected: String , selectedId : String) {
        tfCity.text = selected
        cityIdStr = selectedId
    }
    
    func selectedState(selectedState: String, stateID : String) {
        tfState.text = selectedState
        stateIdStr = stateID
        tfCity.becomeFirstResponder()
    }
    
    func selectedCountry(slctdCountry : String , countryId : String) {
        tfCountry.text = slctdCountry
        countryIdStr = countryId
        tfState.becomeFirstResponder()
    }
    
    func selectedMedicalType(selected: String) {
        tfMentleCondition.text = selected
    }
    
    func selectedActivityType(selected: String , activityId :String) {
        tfActivityStatus.text = selected
        activityIdStr = activityId
    }
    func selectedBodyType(selected: String, bodyTypeID : String) {
        tfBodyType.text = selected
        bodyTypeIdStr = bodyTypeID
    }
    
    func selectedIdealFatType(selected: String , bodyFatId : String) {
        tfBodyFat.text = selected
        bodyFatIdStr = bodyFatId
    }
    
    func selectedDate(selected: String) {
        tfDateOfBirth.text = selected
    }
    

    @IBAction func btnContinueClicked(_ sender: UIButton) {
        
        
        if (tfFullName.text!.isEmpty) {
            showToast(message: "Please Enter user name", font: UIFont(name: "Roboto-Medium", size: 15)!)
        }else if (genderStr == "")
        {
            showToast(message: "Please Select gender", font: UIFont(name: "Roboto-Medium", size: 15)!)
        }
        else if (tfWeightKgs.text!.isEmpty)
        {
            showToast(message: "Please Entyer your weight", font: UIFont(name: "Roboto-Medium", size: 15)!)
        }else if (tfHeightFeets.text!.isEmpty)
        {
            showToast(message: "Please Entyer your height", font: UIFont(name: "Roboto-Medium", size: 15)!)
        }else if (tfDateOfBirth.text!.isEmpty)
        {
            showToast(message: "Please Select your date of birth", font: UIFont(name: "Roboto-Medium", size: 15)!)
        }else if (tfCountry.text!.isEmpty)
        {
            showToast(message: "Please Select your country", font: UIFont(name: "Roboto-Medium", size: 15)!)
        }else if (tfState.text!.isEmpty)
        {
            showToast(message: "Please Select your sate", font: UIFont(name: "Roboto-Medium", size: 15)!)
        }else if (tfCity.text!.isEmpty)
        {
            showToast(message: "Please Select your city", font: UIFont(name: "Roboto-Medium", size: 15)!)
        }else if (tfMentleCondition.text!.isEmpty)
        {
            showToast(message: "Please Select your medical condition", font: UIFont(name: "Roboto-Medium", size: 15)!)
        }else if (fitnessStr == "")
        {
            showToast(message: "Please Select your fitness level", font: UIFont(name: "Roboto-Medium", size: 15)!)
        }else if (tfActivityStatus.text!.isEmpty)
        {
            showToast(message: "Please Select your activity", font: UIFont(name: "Roboto-Medium", size: 15)!)
        }else if (tfActivityDuration.text!.isEmpty)
        {
            showToast(message: "Please Select your activity duration", font: UIFont(name: "Roboto-Medium", size: 15)!)
        }else if (tfBodyType.text!.isEmpty)
        {
            showToast(message: "Please Select your Body Type", font: UIFont(name: "Roboto-Medium", size: 15)!)
        }else if (tfBodyFat.text!.isEmpty)
        {
            showToast(message: "Please Select your Body fat levels", font: UIFont(name: "Roboto-Medium", size: 15)!)
        }
        else
        {
            registerForUserDetails()
        }
        
       
    }
    
    
    func registerForUserDetails() -> Void {
        
        
          if Reachability.isConnectedToNetwork()
          {
                   weightStr = self.tfWeightKgs.text! + "." + self.tfWeightGrams.text!

                   
                   self.view.activityStartAnimating(activityColor: #colorLiteral(red: 0, green: 0.9914394021, blue: 1, alpha: 1), backgroundColor: UIColor.black.withAlphaComponent(0.3))

                   let headers = [
                                   "Authorization": "Bearer fitkout",
                                   "Content-Type": "application/json",
                                   "cache-control": "no-cache",
                                 ]
                   
                   if btnKgsToPounds.titleLabel!.text == "POUNDS" {
                       
                       weightStr = "\(Double(weightStr)! * 0.453592)"
                   }
            
                   print(weightStr)
                   
                   let parameters = [
                     "UserFullname": self.tfFullName.text!,
                     "UserGender": self.genderStr,
                     "UserDob": self.tfDateOfBirth.text!,
                     "UserWeight": self.tfWeightKgs.text! + "." + self.tfWeightGrams.text!,
                     "BodyTypeId":  self.bodyTypeIdStr,
                     "BodyFatPercentage": self.bodyFatIdStr,
                     "UserHeight": self.tfHeightFeets.text! + "." + self.tfHeightInches.text!,
                     "UserActivityId": self.activityIdStr,
                     "UserMedCondition": self.tfMentleCondition.text!,
                     "CityId": self.cityIdStr,
                     "Category": "1",
                     "Duration": self.tfActivityDuration.text!
                   ] as [String : Any]

                   
                   var request = URLRequest(url: URL(string: "https://fitkout.com.cs-mum-27.bluehostwebservers.com/Fitkout/index.php/api/ApiCaller/user_details/\(loginID)")!)
                   request.httpMethod = "POST"
                   request.allHTTPHeaderFields = headers
                   request.httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: [])

                   let session = URLSession.shared
                   let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
                    
                       print(response as Any)
                    
                           do {
                           let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                               print(json)
                           DispatchQueue.main.async {
                               self.view.activityStopAnimating()
                           let signUpVC = RegisterThirdVC.init(nibName: "RegisterThirdVC", bundle: nil)
                               signUpVC.loginID = self.loginID
                           signUpVC.modalPresentationStyle = .fullScreen
                            self.present(signUpVC, animated: true, completion: nil)}
                                }
                           catch {
                               self.view.activityStopAnimating()
                               print(error.localizedDescription)
                               self.showAlert(title: error.localizedDescription, msg: "")
                                 }
                   })

                   task.resume()
          }
          else
          {
           self.showAlert(title: "Network Error!", msg: "Please Check Your Connectivity")
           }
    }
    
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == tfWeightKgs {
            return range.location < 3
        } else if textField == tfWeightGrams
        {
            return range.location < 3
        }else if textField == tfHeightFeets
        {
            return range.location < 1
        }else if textField == tfHeightInches
        {
            return range.location < 2
        }
        
        return range.location < 100
    }
    
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnGenderClicked(_ sender: UIButton) {
        
        for radioButton in rGenderBtns {
                  radioButton.setImage(UIImage(named: "unchecked.png"), for: .normal)
              }
              sender.setImage(UIImage(named: "checked.png"), for: .normal)
        genderStr = sender.titleLabel!.text!
        print(genderStr)
    }
    
    @IBAction func btnBodyTypeClicked(_ sender: UIButton) {
        
        for rButton in rBodyTypeBtns {
            rButton.setImage(UIImage(named: "unchecked.png"), for: .normal)
        }
        sender.setImage(UIImage(named: "checked.png"), for: .normal)
        fitnessStr = sender.titleLabel!.text!
        print(fitnessStr)
    }
    
    
    @IBAction func btnWeightConversionClicked(_ sender: UIButton) {
        
        if(kgsClick == true) {
           
            btnKgsToPounds.setTitle("KGS", for: .normal)
        
        } else {
            btnKgsToPounds.setTitle("POUNDS", for: .normal)
        }
        
        kgsClick = !kgsClick
    }
    
}
