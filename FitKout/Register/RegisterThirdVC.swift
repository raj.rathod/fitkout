//
//  RegisterThirdVC.swift
//  FitKout
//
//  Created by HT-Admin on 21/10/19.
//  Copyright © 2019 HT-Admin. All rights reserved.
//

import UIKit

class RegisterThirdVC: UIViewController ,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableGoals: UITableView!
    
    var goalsArray = [Goals]()

    var loginID = 0

    @IBOutlet weak var navBar: UINavigationBar!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // bacground image setting
      //  setBackGroundImage()
        
        tableGoals.register(UINib(nibName: "RegThirdCell", bundle: nil), forCellReuseIdentifier: "RegThirdCell")
        tableGoals.tableFooterView = UIView()
        tableGoals.separatorStyle = .none
        
        TransparentnavigationBar(navBar:self.navBar)

        getRegisterForGoals(userId: 1517)

    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
        return goalsArray.count
           
         }
         
         func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
             let cell = tableView.dequeueReusableCell(withIdentifier: "RegThirdCell", for: indexPath) as! RegThirdCell
            
             let goalsModel: Goals
             goalsModel = goalsArray [indexPath.row]
            
            cell.btnGoal.setTitle(goalsModel.Goal, for: .normal)
            
            if goalsModel.IsEnabled == 1 {
                cell.btnGoal.isEnabled = true
                cell.btnGoal.layer.borderWidth = 1
                cell.btnGoal.layer.borderColor = #colorLiteral(red: 0.1960784314, green: 0.3960784314, blue: 0.5490196078, alpha: 1)
            }else
            {
                cell.btnGoal.isEnabled = false
                cell.btnGoal.layer.borderWidth = 1
                cell.btnGoal.layer.borderColor = #colorLiteral(red: 0.1960784314, green: 0.3960784314, blue: 0.5490196078, alpha: 1)
                cell.btnGoal.backgroundColor = #colorLiteral(red: 0.8588235294, green: 0.8901960784, blue: 0.9019607843, alpha: 1)

            }
           cell.btnGoal.tag = indexPath.row
           cell.btnGoal.addTarget(self, action:  #selector(goalsBtnPressed(sender:)), for: .touchUpInside)
             return cell
         }
         
         
         func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
             return 60
             
         }
         
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
             let goalsModel: Goals
             goalsModel = goalsArray [indexPath.row]
            print(goalsModel.Goal)
    }
    
    @objc func goalsBtnPressed(sender: UIButton) {
        print(sender.tag)
         let goalsModel: Goals
         goalsModel = goalsArray [sender.tag]
        print(goalsModel.Goal)
        let signUpVC = RegisterFourthVC.init(nibName: "RegisterFourthVC", bundle: nil)
        signUpVC.loginId = self.loginID
        signUpVC.goalId = goalsModel.FGoalId
        signUpVC.skipped = "0"
        signUpVC.modalPresentationStyle = .fullScreen
        self.present(signUpVC, animated: true, completion: nil)
    }

    @IBAction func backBtnClicked(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnSkipClicked(_ sender: UIButton) {
        let signUpVC = RegisterFourthVC.init(nibName: "RegisterFourthVC", bundle: nil)
        signUpVC.loginId = self.loginID
        signUpVC.skipped = "1"
        signUpVC.modalPresentationStyle = .fullScreen
         self.present(signUpVC, animated: true, completion: nil)
    }
    
    func getRegisterForGoals(userId : Int) -> Void {
        
          if Reachability.isConnectedToNetwork()
          {
            self.view.activityStartAnimating(activityColor: #colorLiteral(red: 0, green: 0.9914394021, blue: 1, alpha: 1), backgroundColor: UIColor.black.withAlphaComponent(0.2))
            
            let headers = [
              "Content-Type": "application/json",
              "Authorization": "Bearer fitkout",
            ]

            let request = NSMutableURLRequest(url: NSURL(string: "https://fitkout.com.cs-mum-27.bluehostwebservers.com/Fitkout/index.php/api/ExerciseApi/getFitnessGoals?LoginId=\(userId)")! as URL,
                                                    cachePolicy: .useProtocolCachePolicy,
                                                timeoutInterval: 10.0)
            request.httpMethod = "GET"
            request.allHTTPHeaderFields = headers

            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                
                let responseArray  = try! JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! NSArray
                 print(responseArray)
                
                self.view.activityStopAnimating()
                
                var gArray = [Goals]()
                for anItem in responseArray {
                    
                    if let netWorkObj = Goals(dictionary: anItem as! [String : Any]) {
                gArray.append(netWorkObj)
                        
                 }
                    self.goalsArray = gArray
                    print(self.goalsArray)
                 DispatchQueue.main.async {
                      self.tableGoals.reloadData()
                       }
                }
            })

            dataTask.resume()
          }
          else
          {
           self.showAlert(title: "Network Error!", msg: "Please Check Your Connectivity")
           }
        

        
       }
   
}

class Goals {
    var FGoalId:String = ""
    var Goal:String = ""
    var IsEnabled: Int = 0
    var IsRecommended: Int = 0
    
    
    init?(dictionary:[String:Any]) {
        guard let FGoalId = dictionary["FGoalId"],
            let Goal = dictionary["Goal"],
            let IsEnabled = dictionary["IsEnabled"],
            let IsRecommended = dictionary["IsRecommended"]
           
         else{ return}
        
        self.FGoalId = FGoalId as! String
        self.Goal = Goal as! String
        self.IsEnabled = IsEnabled as! Int
        self.IsRecommended = IsRecommended as! Int
        
    }
}

